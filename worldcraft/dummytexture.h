//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: Implementation of IEditorTexture interface for placeholder textures.
//			Placeholder textures are used for textures that are referenced in
//			the map file but not found in storage.
//
// $NoKeywords: $
//=============================================================================

#ifndef DUMMYTEXTURE_H
#define DUMMYTEXTURE_H
#pragma once


#include <afxtempl.h>
#include "IEditorTexture.h"


class CDummyTexture : public IEditorTexture
{
	public:

		CDummyTexture(const char *pszName, TEXTUREFORMAT eFormat);
		virtual ~CDummyTexture();

		inline const char *GetName() const
		{
			return(m_szName);
		}
		int GetShortName(char *pszName) const;

		int GetKeywords(char *pszKeywords) const;

		void Draw(CDC *pDC, RECT &rect, int iFontHeight, int iIconHeight, DWORD dwFlags);

		const char *GetFileName(void) const;

		void GetSize(SIZE &size) const;

		inline bool IsDummy() const
		{
			return(true);
		}

		byte *GetImageDataRGBA( int &nWidth, int &nHeight );

		inline float GetDecalScale() const
		{
			return(1.0f);
		}

		CPalette *GetPalette() const
		{
			return(NULL);
		}

		inline int GetWidth() const
		{
			return(0);
		}

		inline int GetHeight() const
		{
			return(0);
		}

		inline int GetTextureID() const
		{
			return(0);
		}

		inline TEXTUREFORMAT GetTextureFormat() const
		{
			return(m_eTextureFormat);
		}

		inline int GetSurfaceAttributes() const
		{
			return(0);
		}

		inline int GetSurfaceContents() const
		{
			return(0);
		}

		inline int GetSurfaceValue() const
		{
			return(0);
		}

		inline imageflags_t GetImageFlags() const
		{
			return 0;
		}

		inline bool IsAlphaTested() const
		{
			return(false);
		}

		inline bool IsTranslucent() const
		{
			return(false);
		}

		inline bool HasData() const
		{
			return(false);
		}

		inline bool HasPalette() const
		{
			return(false);
		}

		bool Load( void );

		inline bool IsLoaded() const
		{
			return true;
		}

		inline void SetTextureFormat(TEXTUREFORMAT eFormat)
		{
			m_eTextureFormat = eFormat;
		}

		inline void SetTextureID( int nTextureID )
		{
		}

	protected:

		char m_szName[MAX_PATH];
		char m_szFileName[MAX_PATH];

		TEXTUREFORMAT m_eTextureFormat;
};


#endif // DUMMYTEXTURE_H
