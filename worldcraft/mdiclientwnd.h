
#pragma once

#define CMDIClientWnd CWnd

#if 0

class CMDIClientWnd : public CWnd
{
public:

	CMDIClientWnd();
	virtual ~CMDIClientWnd();

protected:

	afx_msg BOOL OnEraseBkgnd(CDC *pDC);

	DECLARE_MESSAGE_MAP()
};

#endif
