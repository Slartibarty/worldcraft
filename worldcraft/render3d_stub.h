
#pragma once

#include "render3d.h"

class CRender3DStub : public CRender3D
{
public:

	~CRender3DStub(void) override {};

	void BindTexture(IEditorTexture *pTexture) override {}

	bool Initialize(HWND hwnd, CMapDoc *pDoc) override { return true; }
	void ShutDown(void) override {}

	RenderMode_t rendermode = RENDER_MODE_DEFAULT;

	RenderMode_t GetCurrentRenderMode(void) override { return rendermode; }
	RenderMode_t GetDefaultRenderMode(void) override { return RENDER_MODE_DEFAULT; }
	float GetElapsedTime(void) override { return 0.0f; }
	float GetGridDistance(void) override { return 64.0f; }
	float GetGridSize(void) override { return 64.0f; }
	void GetViewPoint(Vector& pfViewPoint) override { VectorClear(pfViewPoint); }
	void GetViewForward(Vector& pfViewForward) override { VectorClear(pfViewForward); }
	void GetViewUp(Vector& pfViewUp) override { VectorClear(pfViewUp); }
	void GetViewRight(Vector& pfViewRight) override { VectorClear(pfViewRight); }
	float GetRenderFrame(void) override { return 0.0f; }

	void UncacheAllTextures() override {}

	bool IsEnabled(RenderState_t eRenderState) override { return false; }
	bool IsPicking(void) override { return false; }

	float LightPlane(Vector& Normal) override { return 0.0f; }

	int ObjectsAt(float x, float y, float fWidth, float fHeight, RenderHitInfo_t *pObjects, int nMaxObjects) override { return 0; }

	void SetCamera(CCamera *pCamera) override {}
	void SetDefaultRenderMode(RenderMode_t eRenderMode) override {}
	void SetRenderMode(RenderMode_t eRenderMode, bool force = false) override { rendermode = eRenderMode; }
	bool SetSize(int nWidth, int nHeight) override { return true; }
	void SetProjectionMode( ProjectionMode_t eProjMode ) override {}

	void PreRender( void ) override {}
	void PostRender( void ) override {}

	void Render(void) override {}
	void RenderEnable(RenderState_t eRenderState, bool bEnable) override {}
	void RenderWireframeBox(const Vector &Mins, const Vector &Maxs, unsigned char chRed, unsigned char chGreen, unsigned char chBlue) override {}
	void RenderBox(const Vector &Mins, const Vector &Maxs, unsigned char chRed, unsigned char chGreen, unsigned char chBlue, SelectionState_t eBoxSelectionState) override {}
	void RenderArrow(Vector const &vStartPt, Vector const &vEndPt, unsigned char chRed, unsigned char chGreen, unsigned char chBlue) override {}
	void RenderCone(Vector const &vBasePt, Vector const &vTipPt, float fRadius, int nSlices,
		unsigned char chRed, unsigned char chGreen, unsigned char chBlue ) override {}
	void RenderSphere(Vector const &vCenter, float flRadius, int nTheta, int nPhi, unsigned char chRed, unsigned char chGreen, unsigned char chBlue) override {}
	void RenderWireframeSphere(Vector const &vCenter, float flRadius, int nTheta, int nPhi, unsigned char chRed, unsigned char chGreen, unsigned char chBlue) override {}
	void RenderLine(const Vector &vec1, const Vector &vec2, unsigned char r, unsigned char g, unsigned char b) override {}

	void WorldToScreen(Vector2D &Screen, const Vector &World) override {}
	void ScreenToWorld(Vector &World, const Vector2D &Screen) override {}

	void ScreenToClient(Vector2D &Client, const Vector2D &Screen) override {}
	void ClientToScreen(Vector2D &Screen, const Vector2D &Client) override {}

	void BeginRenderHitTarget(CMapAtom *pObject, unsigned int uHandle = 0) override {}
	void EndRenderHitTarget(void) override {}

	void BeginParallel(void) override {}
	void EndParallel(void) override {}

	// HACK: This is a fixup for bizarre MFC behavior with the drop-down boxes
	void ResetFocus() override {}

	// indicates we need to render an overlay pass...
	bool NeedsOverlay() const override { return false; }

	// should I sort renders?
	bool DeferRendering() const override { return false; }

	void DebugHook1(void *pData = NULL) override {}
	void DebugHook2(void *pData = NULL) override {}
};
