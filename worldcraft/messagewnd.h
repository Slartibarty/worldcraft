
#pragma once

#include <vector>
#include "globalfunctions.h"


constexpr int MAX_MESSAGE_WND_LINES = 4096;
constexpr int MESSAGE_WND_MESSAGE_LENGTH = 256;

class CMessageWnd : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CMessageWnd)
protected:
	CMessageWnd();           // protected constructor used by dynamic creation

	struct MWMSGSTRUCT
	{
		TCHAR szMsg[MESSAGE_WND_MESSAGE_LENGTH];
		MWMSGTYPE type;
		int MsgLen;	// length of message w/o 0x0
	};

// Attributes
public:
	void AddMsg(MWMSGTYPE type, TCHAR* msg);

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMessageWnd)
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CMessageWnd();

	void CalculateScrollSize();
	std::vector<MWMSGSTRUCT> MsgArray;

	CFont Font;
	int iCharWidth;	// calculated in first paint
	int iNumMsgs;

	// Generated message map functions
	//{{AFX_MSG(CMessageWnd)
	afx_msg void OnPaint();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
