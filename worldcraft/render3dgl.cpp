
#include "stdafx.h"

#include <timeapi.h>

#include "mapdoc.h"
#include "mapworld.h"
#include "mapstudiomodel.h"
#include "mapdefs.h"
#include "camera.h"
#include "globalfunctions.h"
#include "ToolManager.h"
#include "culltreenode.h"
#include "worldcraft_mathlib.h"
#include "render3d.h"
#include "render.h"

#include "render3dgl.h"

#define PICKING 1

#define NUM_MIPLEVELS			4

#define CROSSHAIR_DIST_HORIZONTAL		5
#define CROSSHAIR_DIST_VERTICAL			6

#define TEXTURE_AXIS_LENGTH				10	// Texture axis length in world units

//-----------------------------------------------------------------------------

static void MYgluPerspective( float fovy, float aspect, float zNear, float zFar )
{
	float xmin, xmax, ymin, ymax;

	ymax = ( zNear * tan( fovy * M_PI_F / 360.0f ) ) * 0.5f;
	ymin = -ymax;

	xmin = ymin * aspect;
	xmax = ymax * aspect;

	glFrustumf( xmin, xmax, ymin, ymax, zNear, zFar );
}

//-----------------------------------------------------------------------------

// Class globals, breaks convention to have them out here but whatever
static uint64	g_nRenderFrame;		// Last rendered frame. Stored as a cookie in each rendered object.

//
// Debugging / diagnostic stuff.
//
static bool g_bShowStatistics = false;
static bool g_bUseCullTree = true;
static bool g_bRenderCullBoxes = false;

CRender3DGL::CRender3DGL( void )
{
	m_hInstance = nullptr;
	m_hWnd = nullptr;
	m_hDC = nullptr;
	m_hGLRC = nullptr;

	m_pDoc = nullptr;

	m_eDefaultRenderMode = RENDER_MODE_WIREFRAME;
	m_eCurrentRenderMode = RENDER_MODE_NONE;
	m_eProjMode = PROJECTION_PERSPECTIVE;
	memset( &m_RenderState, 0, sizeof( m_RenderState ) );
	memset( &m_Pick, 0, sizeof( m_Pick ) );

	m_pBoundImage = nullptr;

	m_DeferRendering = false;

	m_pCamera = nullptr;
	memset( &m_FrustumPlanes, 0, sizeof( m_FrustumPlanes ) );

	m_nWidth = 0;
	m_nHeight = 0;
	m_fAspect = 0.0f;

	m_fFrameRate = 0.0f;
	m_nFramesThisSample = 0;
	m_dwTimeLastSample = 0;

	m_dwTimeLastFrame = 0;
	m_fTimeElapsed = 0.0f;

}

CRender3DGL::~CRender3DGL( void )
{

}


static GLuint UploadTexture32( byte *pData, int nWidth, int nHeight, imageflags_t flags )
{
	GLuint id;

	// Generate and bind the texture
	glGenTextures( 1, &id );
	glBindTexture( GL_TEXTURE_2D, id );

	// Upload it
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA8, nWidth, nHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, pData );

	// Mips
	if ( !( flags & IF_NOMIPS ) )
	{
		assert( GLEW_ARB_framebuffer_object );

		// This sucks
		glGenerateMipmap( GL_TEXTURE_2D );

		if ( flags & IF_NEAREST )
		{
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
		}
		else
		{
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
		}

		if ( GLEW_ARB_texture_filter_anisotropic && !( flags & IF_NOANISO ) )
		{
			glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY, 16.0f ); // Don't hardcode this value
		}
	}
	else
	{
		// No mips
		if ( flags & IF_NEAREST )
		{
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
		}
		else
		{
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
		}
	}

	// Clamp
	if ( GLEW_EXT_texture_edge_clamp && ( flags & IF_CLAMPS ) )
	{
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE_EXT );
	}
	if ( GLEW_EXT_texture_edge_clamp && ( flags & IF_CLAMPT ) )
	{
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE_EXT );
	}

	return id;
}

void CRender3DGL::FindOrCreateTexture( IEditorTexture *pTexture )
{
	// Look for it
	for ( auto &image : m_Images )
	{
		if ( image.pTexture == pTexture )
		{
			m_pBoundImage = &image;
			return;
		}
	}

	// We haven't cached it

	int nWidth;
	int nHeight;
	byte *pImg = pTexture->GetImageDataRGBA( nWidth, nHeight );

	GLuint id = UploadTexture32( pImg, nWidth, nHeight, pTexture->GetImageFlags() );

	// Paletted textures allocate a buffer for pImg
	if ( pTexture->HasPalette() )
	{
		free( pImg );
	}

	Image_t &pImage = m_Images.emplace_back();
	pImage.uTexnum = id;
	pImage.pTexture = pTexture;

	m_pBoundImage = &pImage;
}

void CRender3DGL::BindTexture( IEditorTexture *pTexture )
{
	if ( !m_pBoundImage || m_pBoundImage->pTexture != pTexture )
	{
		FindOrCreateTexture( pTexture );
		SetupRenderMode( m_eCurrentRenderMode, true );
	}
}


bool CRender3DGL::Initialize( HWND hWnd, CMapDoc *pDoc )
{
	ASSERT( hWnd != NULL );
	ASSERT( pDoc != NULL );
	ASSERT( pDoc->GetMapWorld() != NULL );

	m_hInstance = GetModuleHandle( nullptr );
	m_hWnd = hWnd;
	m_hDC = GetDC( hWnd );
	assert( m_hDC );

	m_pDoc = pDoc;

	// Set up GL for the window we were given
	CreateGLContextForWindow( m_hWnd, m_hDC, m_hGLRC );

	// GL setup

	RECT rect;
	GetClientRect( m_hWnd, &rect );
	SetSize( rect.right, rect.bottom );

	glClearColor( 0.5f, 0.5f, 0.5f, 1.0f );

//	glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

	glEnable( GL_DEPTH_TEST );

	glAlphaFunc( GL_GREATER, 0.666f );
	glEnable( GL_ALPHA_TEST );

	glCullFace( GL_FRONT );
	glEnable( GL_CULL_FACE );

	// Allocate space for sorting objects with masked textures.
	m_RenderLastObjects.reserve(MAX_RENDER_LAST_OBJECTS);

	return true;
}

void CRender3DGL::ShutDown( void )
{
	wglMakeCurrent( NULL, NULL );
	wglDeleteContext( m_hGLRC );
	m_hGLRC = NULL;

	ReleaseDC( m_hWnd, m_hDC );
	m_hWnd = NULL; m_hDC = NULL; // Lose the window and clear the DC
}


RenderMode_t CRender3DGL::GetCurrentRenderMode( void )
{
	return m_eCurrentRenderMode;
}

RenderMode_t CRender3DGL::GetDefaultRenderMode( void )
{
	return m_eDefaultRenderMode;
}

float CRender3DGL::GetElapsedTime( void )
{
	return m_fTimeElapsed;
}

float CRender3DGL::GetGridDistance( void )
{
	return m_RenderState.fGridDistance;
}

float CRender3DGL::GetGridSize( void )
{
	return m_RenderState.fGridSpacing;
}

void CRender3DGL::GetViewPoint( Vector &pfViewPoint )
{
	m_pCamera->GetViewPoint( pfViewPoint );
}

void CRender3DGL::GetViewForward( Vector &pfViewForward )
{
	m_pCamera->GetViewForward( pfViewForward );
}

void CRender3DGL::GetViewUp( Vector &pfViewUp )
{
	m_pCamera->GetViewUp( pfViewUp );
}

void CRender3DGL::GetViewRight( Vector &pfViewRight )
{
	m_pCamera->GetViewRight( pfViewRight );
}

float CRender3DGL::GetRenderFrame( void )
{
	return g_nRenderFrame;
}


void CRender3DGL::UncacheAllTextures()
{
	// Intentionally do nothing!
}


bool CRender3DGL::IsEnabled( RenderState_t eRenderState )
{
	switch ( eRenderState )
	{
	case RENDER_CENTER_CROSSHAIR:
		return m_RenderState.bCenterCrosshair;

	case RENDER_FRAME_RECT:
		return m_RenderState.bDrawFrameRect;

	case RENDER_GRID:
		return m_RenderState.bDrawGrid;

	case RENDER_REVERSE_SELECTION:
		return m_RenderState.bReverseSelection;
	}

	return false;
}

bool CRender3DGL::IsPicking( void )
{
	return m_Pick.bPicking;
}


float CRender3DGL::LightPlane( Vector &Normal )
{
	static Vector Light( 1.0f, 2.0f, 3.0f );
	static bool bFirst = true;

	if ( bFirst )
	{
		VectorNormalize( Light );
		bFirst = false;
	}

	float fShade = 0.8f + ( 0.5f * DotProduct( Normal, Light ) );
	if ( fShade > 1.0f )
	{
		fShade = 1.0f;
	}

	return fShade;
}


int CRender3DGL::ObjectsAt( float x, float y, float fWidth, float fHeight, RenderHitInfo_t *pObjects, int nMaxObjects )
{
	m_Pick.fX = x;
	m_Pick.fY = m_nHeight - ( y + 1 );
	m_Pick.fWidth = fWidth;
	m_Pick.fHeight = fHeight;
	m_Pick.pHitsDest = pObjects;
	m_Pick.nMaxHits = Min( nMaxObjects, MAX_PICK_HITS );
	m_Pick.nNumHits = 0;

	if ( !m_RenderState.bReverseSelection )
	{
		m_Pick.uLastZ = UINT_MAX;
	}
	else
	{
		m_Pick.uLastZ = 0;
	}

	m_Pick.bPicking = true;

	RenderMode_t eOldMode = GetDefaultRenderMode();
	SetDefaultRenderMode( RENDER_MODE_TEXTURED );

	Render();

	glRenderMode( GL_RENDER );

	SetDefaultRenderMode( eOldMode );

	m_Pick.bPicking = false;

	return m_Pick.nNumHits;
}


void CRender3DGL::SetCamera( CCamera *pCamera )
{
	m_pCamera = pCamera;
}

void CRender3DGL::SetDefaultRenderMode( RenderMode_t eRenderMode )
{
	if ( eRenderMode == RENDER_MODE_DEFAULT )
	{
		// Slart: Should be defaultrendermode?
		eRenderMode = RENDER_MODE_WIREFRAME;
	}

	m_eDefaultRenderMode = eRenderMode;
	if ( m_eCurrentRenderMode != eRenderMode )
	{
		SetupRenderMode( eRenderMode );
	}
}

void CRender3DGL::SetRenderMode( RenderMode_t eRenderMode, bool force /*= false*/ )
{
	if ( eRenderMode == RENDER_MODE_DEFAULT )
	{
		// Slart: Should be defaultrendermode?
		eRenderMode = m_eDefaultRenderMode;
	}

	if ( m_eCurrentRenderMode != eRenderMode || force )
	{
		SetupRenderMode( eRenderMode, force );
	}
}

bool CRender3DGL::SetSize( int nWidth, int nHeight )
{
	glViewport( 0, 0, nWidth, nHeight );

	m_nWidth = nWidth;
	m_nHeight = nHeight;
	m_fAspect = (float)nWidth / (float)nHeight;

	return true;
}

void CRender3DGL::SetProjectionMode( ProjectionMode_t eProjMode )
{
	m_eProjMode = eProjMode;
}


//-----------------------------------------------------------------------------
// Purpose: 
// Input  : eRenderMode - 
//-----------------------------------------------------------------------------
void CRender3DGL::SetupRenderMode( RenderMode_t eRenderMode, bool bForce )
{
	// Fix up the render mode based on whether we're picking:
	// we can't be doing wireframe if we're picking cause it won't
	// draw the whole thing.
	if ( m_Pick.bPicking )
		eRenderMode = RENDER_MODE_FLAT;

	if ( ( m_eCurrentRenderMode == eRenderMode ) && ( !bForce ) )
	{
		return;
	}

	// Bind the appropriate material based on our mode
	switch ( eRenderMode )
	{
	case RENDER_MODE_LIGHTMAP_GRID:
	//	assert( m_pLightmapGrid[m_DecalMode] );
		glEnable( GL_TEXTURE_2D );
	//	MaterialSystemInterface()->Bind( m_pLightmapGrid[m_DecalMode] );
		break;

	case RENDER_MODE_SELECTION_OVERLAY:
		// Ensures we get red even for decals
	//	assert( m_pSelectionOverlay[m_DecalMode] );
		glDisable( GL_TEXTURE_2D );
	//	MaterialSystemInterface()->Bind( m_pSelectionOverlay[m_DecalMode] );
		break;

	case RENDER_MODE_TEXTURED:
	//	assert( m_pFlat[m_DecalMode] );
		glEnable( GL_TEXTURE_2D );
		if ( m_pBoundImage )
			glBindTexture( GL_TEXTURE_2D, m_pBoundImage->uTexnum );
	//	else
	//		MaterialSystemInterface()->Bind( m_pFlat[m_DecalMode] );
		break;

	case RENDER_MODE_FLAT:
	//	assert( m_pFlat[m_DecalMode] );
		glDisable( GL_TEXTURE_2D );
	//	MaterialSystemInterface()->Bind( m_pFlat[m_DecalMode] );
		break;

	case RENDER_MODE_TRANSLUCENT_FLAT:
	//	assert( m_pTranslucentFlat[m_DecalMode] );
		glDisable( GL_TEXTURE_2D );
	//	MaterialSystemInterface()->Bind( m_pTranslucentFlat[m_DecalMode] );
		break;

	case RENDER_MODE_WIREFRAME:
	//	assert( m_pWireframe[m_DecalMode] );
		glDisable( GL_TEXTURE_2D );
	//	MaterialSystemInterface()->Bind( m_pWireframe[m_DecalMode] );
		break;

	case RENDER_MODE_SMOOTHING_GROUP:
	//	assert( m_pFlat[m_DecalMode] );
		glDisable( GL_TEXTURE_2D );
	//	MaterialSystemInterface()->Bind( m_pFlat[m_DecalMode] );
		break;
	}

	m_eCurrentRenderMode = eRenderMode;
}

void CRender3DGL::GetFrustumPlanes( Vector4D Planes[6] )
{
	//
	// Get the modelview and projection matrices from OpenGL.
	//
	matrix4_t ViewMatrix;
	matrix4_t ProjectionMatrix;
	matrix4_t CameraMatrix;
	Vector ViewPoint;

	m_pCamera->GetViewPoint( ViewPoint );

	glGetFloatv( GL_MODELVIEW_MATRIX, &ViewMatrix[0][0] );
	glGetFloatv( GL_PROJECTION_MATRIX, &ProjectionMatrix[0][0] );

	TransposeMatrix( ViewMatrix );
	TransposeMatrix( ProjectionMatrix );

	MultiplyMatrix( ProjectionMatrix, ViewMatrix, CameraMatrix );

	//
	// Now the plane coefficients can be pulled directly out of the the camera
	// matrix as follows:
	//
	// Right :  first_column  - fourth_column
	// Left  : -first_column  - fourth_column
	// Top   :  second_column - fourth_column
	// Bottom: -second_column - fourth_column
	// Front : -third_column  - fourth_column
	// Back  :  third_column  + fourth_column
	//
	// dvs: My plane constants should be coming directly from the matrices,
	//		but they aren't (for some reason). Instead I calculate the plane
	//		constants myself. Sigh.
	//
	Planes[0][0] = CameraMatrix[0][0] - CameraMatrix[3][0];
	Planes[0][1] = CameraMatrix[0][1] - CameraMatrix[3][1];
	Planes[0][2] = CameraMatrix[0][2] - CameraMatrix[3][2];
	VectorNormalize( Planes[0].AsVector3D() );
	Planes[0][3] = DotProduct( ViewPoint, Planes[0].AsVector3D() );

	Planes[1][0] = -CameraMatrix[0][0] - CameraMatrix[3][0];
	Planes[1][1] = -CameraMatrix[0][1] - CameraMatrix[3][1];
	Planes[1][2] = -CameraMatrix[0][2] - CameraMatrix[3][2];
	VectorNormalize( Planes[1].AsVector3D() );
	Planes[1][3] = DotProduct( ViewPoint, Planes[1].AsVector3D() );

	Planes[2][0] = CameraMatrix[1][0] - CameraMatrix[3][0];
	Planes[2][1] = CameraMatrix[1][1] - CameraMatrix[3][1];
	Planes[2][2] = CameraMatrix[1][2] - CameraMatrix[3][2];
	VectorNormalize( Planes[2].AsVector3D() );
	Planes[2][3] = DotProduct( ViewPoint, Planes[2].AsVector3D() );

	Planes[3][0] = -CameraMatrix[1][0] - CameraMatrix[3][0];
	Planes[3][1] = -CameraMatrix[1][1] - CameraMatrix[3][1];
	Planes[3][2] = -CameraMatrix[1][2] - CameraMatrix[3][2];
	VectorNormalize( Planes[3].AsVector3D() );
	Planes[3][3] = DotProduct( ViewPoint, Planes[3].AsVector3D() );

	Planes[4][0] = -CameraMatrix[2][0] - CameraMatrix[3][0];
	Planes[4][1] = -CameraMatrix[2][1] - CameraMatrix[3][1];
	Planes[4][2] = -CameraMatrix[2][2] - CameraMatrix[3][2];
	VectorNormalize( Planes[4].AsVector3D() );
	m_pCamera->GetMoveForward( ViewPoint, m_pCamera->GetNearClip() );
	Planes[4][3] = DotProduct( ViewPoint, Planes[4].AsVector3D() );

	Planes[5][0] = CameraMatrix[2][0] + CameraMatrix[3][0];
	Planes[5][1] = CameraMatrix[2][1] + CameraMatrix[3][1];
	Planes[5][2] = CameraMatrix[2][2] + CameraMatrix[3][2];
	VectorNormalize( Planes[5].AsVector3D() );
	m_pCamera->GetMoveForward( ViewPoint, m_pCamera->GetFarClip() );
	Planes[5][3] = DotProduct( ViewPoint, Planes[5].AsVector3D() );
}

void CRender3DGL::SetupCamera( CCamera *pCamera )
{
	assert( pCamera != NULL );
	if ( pCamera == NULL )
	{
		return;
	}

	//
	// Set up our perspective transformation.
	//
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();

#if PICKING
	if ( m_Pick.bPicking )
	{
	//	MaterialSystemInterface()->PickMatrix( m_Pick.fX, m_Pick.fY, m_Pick.fWidth, m_Pick.fHeight );
		int viewport[4]{ 0, 0, m_nWidth, m_nHeight };
		gluPickMatrix( m_Pick.fX, m_Pick.fY, m_Pick.fWidth, m_Pick.fHeight, viewport );
		glSelectBuffer( sizeof( m_Pick.uSelectionBuffer ), m_Pick.uSelectionBuffer );
		glRenderMode( GL_SELECT );
		glInitNames();
	}
#endif

	if ( m_eProjMode == PROJECTION_PERSPECTIVE )
	{
		MYgluPerspective( pCamera->GetHorizontalFOV(), m_fAspect, pCamera->GetNearClip(), pCamera->GetFarClip() );
	}
	else if ( m_eProjMode == PROJECTION_ORTHOGRAPHIC )
	{
		glScalef( 1.0f, -1.0f, 1.0f );
		glOrthof( -m_nWidth / 2.0f, -m_nHeight / 2.0f,
			m_nWidth / 2.0f, m_nHeight / 2.0f,
			pCamera->GetNearClip(), pCamera->GetFarClip() );
	}

	//
	// Set up the view matrix.
	//
	matrix4_t ViewMatrix;
	pCamera->GetViewMatrix( ViewMatrix );
	TransposeMatrix( ViewMatrix );
	glMatrixMode( GL_MODELVIEW );
	glLoadMatrixf( &ViewMatrix[0][0] );

	//
	// handle the orthographic zoom
	//
	if ( m_eProjMode == PROJECTION_ORTHOGRAPHIC )
	{
		float scale = m_pCamera->GetZoom();
		glScalef( scale, scale, scale );
	}

	//
	// Build the frustum planes for view volume culling.
	//
	GetFrustumPlanes( m_FrustumPlanes );

	// For debugging frustum planes
#ifdef _DEBUG
//	if ( m_bRecomputeFrustumRenderGeometry )
//	{
//		ComputeFrustumRenderGeometry();
//		m_bRecomputeFrustumRenderGeometry = false;
//	}
#endif

	// tell studiorender that we've updated the camera.
#if UNFINISHED
	Vector viewOrigin, viewRight, viewUp, viewPlaneNormal, viewTarget;
	m_pCamera->GetViewPoint( viewOrigin );
	m_pCamera->GetViewRight( viewRight );
	m_pCamera->GetViewUp( viewUp );
	m_pCamera->GetViewTarget( viewTarget );
	VectorSubtract( viewTarget, viewOrigin, viewPlaneNormal );
	VectorNormalize( viewPlaneNormal );
	StudioModel::UpdateViewState( viewOrigin, viewRight, viewUp, viewPlaneNormal );
#endif
}

void CRender3DGL::PreRender( void )
{
	++g_nRenderFrame;

	if ( g_CurrentHGLRC != m_hGLRC )
	{
		VERIFY( wglMakeCurrent( m_hDC, m_hGLRC ) );
		g_CurrentHGLRC = m_hGLRC;
	}

	//
	// Determine the elapsed time since the last frame was rendered.
	//
	DWORD dwTimeNow = timeGetTime();
	if ( m_dwTimeLastFrame == 0 )
	{
		m_dwTimeLastFrame = dwTimeNow;
	}
	DWORD dwTimeElapsed = dwTimeNow - m_dwTimeLastFrame;
	m_fTimeElapsed = (float)dwTimeElapsed / 1000.0f;
	m_dwTimeLastFrame = dwTimeNow;

	//
	// Animate the models based on elapsed time.
	//
	CMapStudioModel::AdvanceAnimation( GetElapsedTime() );

	//
	// Setup the camera position, orientation, and FOV.
	//
	SetupCamera( m_pCamera );

	//
	// Clear the frame buffer and Z buffer.
	//
	if (!m_Pick.bPicking)
	{
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	}

	//
	// Setup material system for wireframe, flat, or textured rendering.
	//
	SetupRenderMode( RENDER_MODE_WIREFRAME, true );

	//
	// Cache per-frame information from the doc.
	//
	m_RenderState.fGridSpacing = m_pDoc->GetGridSpacing();
	m_RenderState.fGridDistance = m_RenderState.fGridSpacing * 10;
	if (m_RenderState.fGridDistance > 2048)
	{
		m_RenderState.fGridDistance = 2048;
	}
	else if (m_RenderState.fGridDistance < 64)
	{
		m_RenderState.fGridDistance = 64;
	}

	//
	// Empty the list of objects to render last.
	//
	m_RenderLastObjects.clear();
}

static int _CompareHits( const void *pHit1, const void *pHit2 )
{
	if ( ( (RenderHitInfo_t *)pHit1 )->nDepth < ( (RenderHitInfo_t *)pHit2 )->nDepth )
	{
		return -1;
	}

	if ( ( (RenderHitInfo_t *)pHit1 )->nDepth > ( (RenderHitInfo_t *)pHit2 )->nDepth )
	{
		return 1;
	}

	return 0;
}

static int _CompareHitsReverse( const void *pHit1, const void *pHit2 )
{
	if ( ( (RenderHitInfo_t *)pHit1 )->nDepth > ( (RenderHitInfo_t *)pHit2 )->nDepth )
	{
		return -1;
	}

	if ( ( (RenderHitInfo_t *)pHit1 )->nDepth < ( (RenderHitInfo_t *)pHit2 )->nDepth )
	{
		return 1;
	}

	return 0;
}

void CRender3DGL::PostRender( void )
{
#if PICKING
	if ( m_Pick.bPicking )
	{
		glFlush();

		//
		// Some OpenGL drivers, such as the ATI Rage Fury Max, return selection buffer Z values
		// in reverse order. For these cards, we must reverse the selection order.
		//
		if ( m_Pick.nNumHits > 1 )
		{
			if ( !m_RenderState.bReverseSelection )
			{
				qsort( m_Pick.Hits, m_Pick.nNumHits, sizeof( m_Pick.Hits[0] ), _CompareHits );
			}
			else
			{
				qsort( m_Pick.Hits, m_Pick.nNumHits, sizeof( m_Pick.Hits[0] ), _CompareHitsReverse );
			}
		}

		//
		// Copy the requested number of nearest hits into the destination buffer.
		//
		int nHitsToCopy = min( m_Pick.nNumHits, m_Pick.nMaxHits );
		if ( nHitsToCopy != 0 )
		{
			memcpy( m_Pick.pHitsDest, m_Pick.Hits, sizeof( m_Pick.Hits[0] ) * nHitsToCopy );
		}
	}
#endif

	//
	// Copy the GL buffer contents to our window's device context unless we're in pick mode.
	//
	if ( !m_Pick.bPicking )
	{
		SwapBuffers( m_hDC );

		if ( g_bShowStatistics )
		{
			//
			// Calculate frame rate.
			//
			if ( m_dwTimeLastSample != 0 )
			{
				DWORD dwTimeNow = timeGetTime();
				DWORD dwTimeElapsed = dwTimeNow - m_dwTimeLastSample;
				if ( ( dwTimeElapsed > 1000 ) && ( m_nFramesThisSample > 0 ) )
				{
					float fTimeElapsed = MS2SEC(dwTimeElapsed);
					m_fFrameRate = m_nFramesThisSample / fTimeElapsed;
					m_nFramesThisSample = 0;
					m_dwTimeLastSample = dwTimeNow;
				}
			}
			else
			{
				m_dwTimeLastSample = timeGetTime();
			}

			m_nFramesThisSample++;

			//
			// Display the frame rate and camera position.
			//
			char szText[128];
			Vector ViewPoint;
			m_pCamera->GetViewPoint( ViewPoint );
			int nLen = V_sprintf( szText, "FPS=%3.2f Pos=[%.f %.f %.f]", m_fFrameRate, ViewPoint[0], ViewPoint[1], ViewPoint[2] );
			TextOut( m_hDC, 2, 18, szText, nLen );
		}
	}

	GLenum err;
	while ( ( err = glGetError() ) != GL_NO_ERROR )
	{
		Msg( mwWarning, "glGetError() = 0x%x\n", err );
	}
}


static void RenderWorldAxes()
{
	glBegin( GL_LINES );

	glColor3ub( 255, 0, 0 );
	glVertex3f( 0, 0, 0 );

	glVertex3f( 100, 0, 0 );

	glColor3ub( 0, 255, 0 );
	glVertex3f( 0, 0, 0 );

	glVertex3f( 0, 100, 0 );

	glColor3ub( 0, 0, 255 );
	glVertex3f( 0, 0, 0 );

	glVertex3f( 0, 0, 100 );

	glEnd();
}

static void RenderCrossHair( CRender3D *pRender, int windowWidth, int windowHeight )
{
	int nCenterX;
	int nCenterY;

	nCenterX = windowWidth / 2;
	nCenterY = windowHeight / 2;

	// Render the world axes
	pRender->SetRenderMode( RENDER_MODE_WIREFRAME );

	glBegin( GL_LINES );

	glColor3ub( 0, 0, 0 );
	glVertex3f( nCenterX - CROSSHAIR_DIST_HORIZONTAL, nCenterY - 1, 0 );
	glVertex3f( nCenterX + CROSSHAIR_DIST_HORIZONTAL + 1, nCenterY - 1, 0 );

	glVertex3f( nCenterX - CROSSHAIR_DIST_HORIZONTAL, nCenterY + 1, 0 );
	glVertex3f( nCenterX + CROSSHAIR_DIST_HORIZONTAL + 1, nCenterY + 1, 0 );

	glVertex3f( nCenterX - 1, nCenterY - CROSSHAIR_DIST_VERTICAL, 0 );
	glVertex3f( nCenterX - 1, nCenterY + CROSSHAIR_DIST_VERTICAL, 0 );

	glVertex3f( nCenterX + 1, nCenterY - CROSSHAIR_DIST_VERTICAL, 0 );
	glVertex3f( nCenterX + 1, nCenterY + CROSSHAIR_DIST_VERTICAL, 0 );

	glColor3ub( 255, 255, 255 );
	glVertex3f( nCenterX - CROSSHAIR_DIST_HORIZONTAL, nCenterY, 0 );
	glVertex3f( nCenterX + CROSSHAIR_DIST_HORIZONTAL + 1, nCenterY, 0 );

	glVertex3f( nCenterX, nCenterY - CROSSHAIR_DIST_VERTICAL, 0 );
	glVertex3f( nCenterX, nCenterY + CROSSHAIR_DIST_VERTICAL, 0 );

	glEnd();
}

static void RenderFrameRect( CRender3D *pRender, int windowWidth, int windowHeight )
{
	// Render the world axes.
	pRender->SetRenderMode( RENDER_MODE_WIREFRAME );

	glBegin( GL_LINE_STRIP );

	// Set color
	glColor3ub( 255, 0, 0 );

	glVertex3f( 1, 0, 0 );

	glVertex3f( windowWidth, 0, 0 );

	glVertex3f( windowWidth, windowHeight - 1, 0 );

	glVertex3f( 1, windowHeight - 1, 0 );

	glVertex3f( 1, 0, 0 );

	glEnd();
}

void CRender3DGL::RenderOverlayElements()
{
	if ( ( m_RenderState.bCenterCrosshair ) || ( m_RenderState.bDrawFrameRect ) )
		BeginParallel();

	if ( m_RenderState.bCenterCrosshair )
		RenderCrossHair( this, m_nWidth, m_nHeight );

	if ( m_RenderState.bDrawFrameRect )
		RenderFrameRect( this, m_nWidth, m_nHeight );

	if ( ( m_RenderState.bCenterCrosshair ) || ( m_RenderState.bDrawFrameRect ) )
		EndParallel();
}

void CRender3DGL::RenderTools()
{
	int nToolCount = g_pToolManager->GetToolCount();
	for ( int i = 0; i < nToolCount; i++ )
	{
		CBaseTool *pTool = g_pToolManager->GetTool( i );
		pTool->RenderTool3D( this );
	}
}

void CRender3DGL::RenderPointsFile()
{
	int &nPFPoints = m_pDoc->m_nPFPoints;
	if ( nPFPoints > 0 )
	{
		Vector *&pPFPoints = m_pDoc->m_pPFPoints;

		SetRenderMode( RENDER_MODE_WIREFRAME );

		glLineWidth(2.0f);

		glBegin( GL_LINE_STRIP );

		glColor3ub( 255, 0, 0 );

		for ( int i = 0; i < nPFPoints; i++ )
		{
			glVertex3fv( pPFPoints[i].Base() );
		}

		glEnd();

		glLineWidth(1.0f);

		SetRenderMode( RENDER_MODE_DEFAULT );
	}
}

Visibility_t CRender3DGL::IsBoxVisible( Vector const &BoxMins, Vector const &BoxMaxs )
{
	Vector NearVertex;
	Vector FarVertex;

	//
	// Build the near and far vertices based on the octant of the plane normal.
	//
	int i, nInPlanes;
	for ( i = 0, nInPlanes = 0; i < 6; i++ )
	{
		if ( m_FrustumPlanes[i][0] > 0 )
		{
			NearVertex[0] = BoxMins[0];
			FarVertex[0] = BoxMaxs[0];
		}
		else
		{
			NearVertex[0] = BoxMaxs[0];
			FarVertex[0] = BoxMins[0];
		}

		if ( m_FrustumPlanes[i][1] > 0 )
		{
			NearVertex[1] = BoxMins[1];
			FarVertex[1] = BoxMaxs[1];
		}
		else
		{
			NearVertex[1] = BoxMaxs[1];
			FarVertex[1] = BoxMins[1];
		}

		if ( m_FrustumPlanes[i][2] > 0 )
		{
			NearVertex[2] = BoxMins[2];
			FarVertex[2] = BoxMaxs[2];
		}
		else
		{
			NearVertex[2] = BoxMaxs[2];
			FarVertex[2] = BoxMins[2];
		}

		if ( DotProduct( m_FrustumPlanes[i].AsVector3D(), NearVertex ) >= m_FrustumPlanes[i][3] )
		{
			return( VIS_NONE );
		}

		if ( DotProduct( m_FrustumPlanes[i].AsVector3D(), FarVertex ) < m_FrustumPlanes[i][3] )
		{
			nInPlanes++;
		}
	}

	if ( nInPlanes == 6 )
	{
		return( VIS_TOTAL );
	}

	return( VIS_PARTIAL );
}

void CRender3DGL::RenderMapClass( CMapClass *pMapClass )
{
	assert( pMapClass != NULL );

	if ( ( pMapClass != NULL ) && ( pMapClass->GetRenderFrame() != g_nRenderFrame ) )
	{
		if ( pMapClass->IsVisible() )
		{
			//
			// Render this object's culling box if it is enabled.
			//
			if ( g_bRenderCullBoxes )
			{
				Vector mins;
				Vector maxs;
				pMapClass->GetCullBox( mins, maxs );
				RenderWireframeBox( mins, maxs, 255, 0, 0 );
			}

			//
			// If we should render this object after all the other objects,
			// just add it to a list of objects to render last. Otherwise, render it now.
			//
			if ( !pMapClass->ShouldRenderLast() )
			{
				pMapClass->Render3D( this );
			}
			else
			{
				m_RenderLastObjects.push_back( pMapClass );
			}

			//
			// Render this object's children.
			//
			POSITION pos;
			CMapClass *pChild = pMapClass->GetFirstChild( pos );
			while ( pChild != NULL )
			{
				Vector vecMins;
				Vector vecMaxs;
				pChild->GetCullBox( vecMins, vecMaxs );

				if ( IsBoxVisible( vecMins, vecMaxs ) != VIS_NONE )
				{
					RenderMapClass( pChild );
				}

				pChild = pMapClass->GetNextChild( pos );
			}
		}

		//
		// Consider this object as handled for this frame.
		//
		pMapClass->SetRenderFrame( g_nRenderFrame );
	}
}

void CRender3DGL::RenderNode( CCullTreeNode *pNode, bool bForce )
{
	//
	// Render all child nodes first.
	//
	CCullTreeNode *pChild;
	int nChildren = pNode->GetChildCount();
	if ( nChildren != 0 )
	{
		for ( int nChild = 0; nChild < nChildren; nChild++ )
		{
			pChild = pNode->GetCullTreeChild( nChild );
			ASSERT( pChild != NULL );

			if ( pChild != NULL )
			{
				//
				// Only bother checking nodes with children or objects.
				//
				if ( ( pChild->GetChildCount() != 0 ) || ( pChild->GetObjectCount() != 0 ) )
				{
					bool bForceThisChild = bForce;
					Visibility_t eVis = VIS_NONE;

					if ( !bForceThisChild )
					{
						Vector vecMins;
						Vector vecMaxs;
						pChild->GetBounds( vecMins, vecMaxs );
						eVis = IsBoxVisible( vecMins, vecMaxs );
						if ( eVis == VIS_TOTAL )
						{
							bForceThisChild = true;
						}
					}

					if ( ( bForceThisChild ) || ( eVis != VIS_NONE ) )
					{
						RenderNode( pChild, bForceThisChild );
					}
				}
			}
		}
	}
	else
	{
		//
		// Now render the contents of this node.
		//
		CMapClass *pObject;
		int nObjects = pNode->GetObjectCount();
		for ( int nObject = 0; nObject < nObjects; nObject++ )
		{
			pObject = pNode->GetCullTreeObject( nObject );
			ASSERT( pObject != NULL );

			Vector vecMins;
			Vector vecMaxs;
			pObject->GetCullBox( vecMins, vecMaxs );
			if ( IsBoxVisible( vecMins, vecMaxs ) != VIS_NONE )
			{
				RenderMapClass( pObject );
			}
		}
	}
}

void CRender3DGL::RenderTree()
{
	assert( m_pDoc != NULL );
	if ( m_pDoc == NULL )
	{
		return;
	}

	CMapWorld *pWorld = m_pDoc->GetMapWorld();
	if ( pWorld == NULL )
	{
		return;
	}

	//
	// Recursively traverse the culling tree, rendering visible nodes.
	//
	CCullTreeNode *pTree = pWorld->CullTree_GetCullTree();
	if ( pTree != NULL )
	{
		Vector vecMins;
		Vector vecMaxs;
		pTree->GetBounds( vecMins, vecMaxs );
		Visibility_t eVis = IsBoxVisible( vecMins, vecMaxs );

		if ( eVis != VIS_NONE )
		{
			RenderNode( pTree, eVis == VIS_TOTAL );
		}
	}
}

void CRender3DGL::Render( void )
{
	if ( m_nWidth == 0 || m_nHeight == 0 )
	{
		// Not initialized yet.
		return;
	}

	if ( !m_pCamera )
	{
		// Camera not set up yet.
		return;
	}

	PreRender();

	RenderWorldAxes();

	//
	// Deferred rendering lets us sort everything here by material.
	//
	if ( !IsPicking() )
	{
		m_DeferRendering = true;
	}

	//
	// Render the world using octree culling.
	//
	if (g_bUseCullTree)
	{
		RenderTree();
	}
	//
	// Render the world without octree culling.
	//
	else
	{
		RenderMapClass( m_pDoc->GetMapWorld() );
	}

	if ( !IsPicking() )
	{
		m_DeferRendering = false;

		// An optimization... render tree doesn't actually render anythung
		// This here will do the rendering, sorted by material by pass
		CMapFace::RenderOpaqueFaces( this );
	}

	RenderTools();
	RenderPointsFile();

	if ( !m_RenderLastObjects.empty() )
	{
		// Need to sort	our last objects by their center...
		// We want to render these back to front
		float *pDepth = (float *)_alloca( m_RenderLastObjects.size() * sizeof( float ) );

		Vector direction, center;
		m_pCamera->GetViewForward( direction );
		for ( int i = 0; i < (int)m_RenderLastObjects.size(); ++i )
		{
			CMapAtom *pMapAtom = m_RenderLastObjects[i];
			( (CMapClass *)pMapAtom )->GetOrigin( center );

			float depth = DotProduct( center, direction );

			// Yeah, this isn't incredibly fast. But it was easy to write!
			int j = i;
			while ( --j >= 0 )
			{
				if ( pDepth[j] > depth )
					break;

				// move up...
				pDepth[j + 1] = pDepth[j];
				m_RenderLastObjects[j + 1] = m_RenderLastObjects[j];
			}

			// insert after j
			++j;
			pDepth[j] = depth;
			m_RenderLastObjects[j] = pMapAtom;
		}

		glEnable( GL_BLEND );

		for ( int nObject = 0; nObject < (int)m_RenderLastObjects.size(); nObject++ )
		{
			m_RenderLastObjects[nObject]->Render3D( this );
		}

		glDisable( GL_BLEND );
	}

#ifdef _DEBUG
//	if ( m_bRenderFrustum )
//	{
//		RenderFrustum();
//	}
#endif

	//
	// Render any 2D elements that overlay the 3D view, like a center crosshair.
	//
	RenderOverlayElements();

	PostRender();
}

void CRender3DGL::RenderEnable( RenderState_t eRenderState, bool bEnable )
{
	switch ( eRenderState )
	{
	case RENDER_POLYGON_OFFSET_FILL:
		if ( bEnable )
		{
			glEnable( GL_POLYGON_OFFSET_FILL );
			glPolygonOffset( -1.0f, -1.0f );
		}
		else
		{
			glDisable( GL_POLYGON_OFFSET_FILL );
		}
		break;

	case RENDER_POLYGON_OFFSET_LINE:	// Slart: This is never used
		if ( bEnable )
		{
			glEnable( GL_POLYGON_OFFSET_LINE );
			glPolygonOffset( -1.0f, -1.0f );
		}
		else
		{
			glDisable( GL_POLYGON_OFFSET_LINE );
		}
		break;

	case RENDER_CENTER_CROSSHAIR:
		m_RenderState.bCenterCrosshair = bEnable;
		break;

	case RENDER_FRAME_RECT:
		m_RenderState.bDrawFrameRect = bEnable;
		break;

	case RENDER_GRID:
		m_RenderState.bDrawGrid = bEnable;
		break;

	case RENDER_FILTER_TEXTURES:
		m_RenderState.bFilterTextures = bEnable;
		break;

	case RENDER_REVERSE_SELECTION:
		m_RenderState.bReverseSelection = bEnable;
		break;
	}
}

void CRender3DGL::RenderWireframeBox( const Vector &Mins, const Vector &Maxs, unsigned char chRed, unsigned char chGreen, unsigned char chBlue )
{
	//
	// Draw the box bottom, top, and one corner edge.
	//

	SetRenderMode( RENDER_MODE_WIREFRAME );

	glBegin( GL_LINE_STRIP );

	// Set color
	glColor3ub( chRed, chGreen, chBlue );
	
	glVertex3f( Mins[0], Mins[1], Mins[2] );

	glVertex3f( Maxs[0], Mins[1], Mins[2] );

	glVertex3f( Maxs[0], Maxs[1], Mins[2] );

	glVertex3f( Mins[0], Maxs[1], Mins[2] );
	
	glVertex3f( Mins[0], Mins[1], Mins[2] );

	glVertex3f( Mins[0], Mins[1], Maxs[2] );

	glVertex3f( Maxs[0], Mins[1], Maxs[2] );

	glVertex3f( Maxs[0], Maxs[1], Maxs[2] );

	glVertex3f( Mins[0], Maxs[1], Maxs[2] );

	glVertex3f( Mins[0], Mins[1], Maxs[2] );

	glEnd();

	//
	// Draw the three missing edges.
	//
	glBegin( GL_LINES );

	glVertex3f( Maxs[0], Mins[1], Mins[2] );
	glVertex3f( Maxs[0], Mins[1], Maxs[2] );

	glVertex3f( Maxs[0], Maxs[1], Mins[2] );
	glVertex3f( Maxs[0], Maxs[1], Maxs[2] );

	glVertex3f( Mins[0], Maxs[1], Mins[2] );
	glVertex3f( Mins[0], Maxs[1], Maxs[2] );

	glEnd();

	SetRenderMode( RENDER_MODE_DEFAULT );
}

void CRender3DGL::RenderBox( const Vector &Mins, const Vector &Maxs, unsigned char chRed, unsigned char chGreen, unsigned char chBlue, SelectionState_t eBoxSelectionState )
{
	Vector FacePoints[8];

	FacePoints[0][0] = Mins[0];
	FacePoints[0][1] = Mins[1];
	FacePoints[0][2] = Mins[2];

	FacePoints[1][0] = Mins[0];
	FacePoints[1][1] = Mins[1];
	FacePoints[1][2] = Maxs[2];

	FacePoints[2][0] = Mins[0];
	FacePoints[2][1] = Maxs[1];
	FacePoints[2][2] = Mins[2];

	FacePoints[3][0] = Mins[0];
	FacePoints[3][1] = Maxs[1];
	FacePoints[3][2] = Maxs[2];

	FacePoints[4][0] = Maxs[0];
	FacePoints[4][1] = Mins[1];
	FacePoints[4][2] = Mins[2];

	FacePoints[5][0] = Maxs[0];
	FacePoints[5][1] = Mins[1];
	FacePoints[5][2] = Maxs[2];

	FacePoints[6][0] = Maxs[0];
	FacePoints[6][1] = Maxs[1];
	FacePoints[6][2] = Mins[2];

	FacePoints[7][0] = Maxs[0];
	FacePoints[7][1] = Maxs[1];
	FacePoints[7][2] = Maxs[2];

	int nFaces[6][4] =
	{
		{ 0, 2, 3, 1 },
		{ 0, 1, 5, 4 },
		{ 4, 5, 7, 6 },
		{ 2, 6, 7, 3 },
		{ 1, 3, 7, 5 },
		{ 0, 4, 6, 2 }
	};

	RenderMode_t eRenderModeThisPass;
	int nPasses;

	if ( ( eBoxSelectionState != SELECT_NONE ) && ( GetDefaultRenderMode() != RENDER_MODE_WIREFRAME ) )
	{
		nPasses = 2;
	}
	else
	{
		nPasses = 1;
	}

	for ( int nPass = 1; nPass <= nPasses; nPass++ )
	{
		if ( nPass == 1 )
		{
			eRenderModeThisPass = GetDefaultRenderMode();

			// There's no texture for a bounding box.
			if ( ( eRenderModeThisPass == RENDER_MODE_TEXTURED ) ||
				( eRenderModeThisPass == RENDER_MODE_LIGHTMAP_GRID ) )
			{
				eRenderModeThisPass = RENDER_MODE_FLAT;
			}

			SetRenderMode( eRenderModeThisPass );
		}
		else
		{
			eRenderModeThisPass = RENDER_MODE_WIREFRAME;
			SetRenderMode( eRenderModeThisPass );
		}

		for ( int nFace = 0; nFace < 6; nFace++ )
		{
			Vector Edge1, Edge2, Normal;
			int nP1, nP2, nP3, nP4;

			nP1 = nFaces[nFace][0];
			nP2 = nFaces[nFace][1];
			nP3 = nFaces[nFace][2];
			nP4 = nFaces[nFace][3];

			VectorSubtract( FacePoints[nP4], FacePoints[nP1], Edge1 );
			VectorSubtract( FacePoints[nP2], FacePoints[nP1], Edge2 );
			CrossProduct( Edge1, Edge2, Normal );
			VectorNormalize( Normal );

			//
			// If we are rendering using one of the lit modes, calculate lighting.
			// 
			unsigned char color[3];

			assert( ( eRenderModeThisPass != RENDER_MODE_TEXTURED ) &&
				( eRenderModeThisPass != RENDER_MODE_LIGHTMAP_GRID ) );
			if ( ( eRenderModeThisPass == RENDER_MODE_FLAT ) )
			{
				float fShade = LightPlane( Normal );

				//
				// For flat and textured mode use the face color with lighting.
				//
				if ( eBoxSelectionState != SELECT_NONE )
				{
					color[0] = SELECT_FACE_RED * fShade;
					color[1] = SELECT_FACE_GREEN * fShade;
					color[2] = SELECT_FACE_BLUE * fShade;
				}
				else
				{
					color[0] = chRed * fShade;
					color[1] = chGreen * fShade;
					color[2] = chBlue * fShade;
				}
			}
			//
			// For wireframe mode use the face color without lighting.
			//
			else
			{
				if ( eBoxSelectionState != SELECT_NONE )
				{
					color[0] = SELECT_FACE_RED;
					color[1] = SELECT_FACE_GREEN;
					color[2] = SELECT_FACE_BLUE;
				}
				else
				{
					color[0] = chRed;
					color[1] = chGreen;
					color[2] = chBlue;
				}
			}

			//
			// Draw the face.
			//
			bool wireframe = ( eRenderModeThisPass == RENDER_MODE_WIREFRAME );

			glBegin( wireframe ? GL_LINE_LOOP : GL_POLYGON );

			glColor3ub( color[0], color[1], color[2] );
			glVertex3fv( FacePoints[nP1].Base() );
			glVertex3fv( FacePoints[nP2].Base() );
			glVertex3fv( FacePoints[nP3].Base() );
			glVertex3fv( FacePoints[nP4].Base() );

			glEnd();
		}
	}

	SetRenderMode( RENDER_MODE_DEFAULT );
}

void CRender3DGL::RenderArrow( Vector const &vStartPt, Vector const &vEndPt, unsigned char chRed, unsigned char chGreen, unsigned char chBlue )
{
	// Never called!
}

void CRender3DGL::RenderCone( Vector const &vBasePt, Vector const &vTipPt, float fRadius, int nSlices,
	unsigned char chRed, unsigned char chGreen, unsigned char chBlue )
{
	// Never called!
}

// This barely functions
void CRender3DGL::RenderSphere( Vector const &vCenter, float flRadius, int nTheta, int nPhi, unsigned char chRed, unsigned char chGreen, unsigned char chBlue )
{
	int nTriangles = 2 * nTheta * ( nPhi - 1 ); // Two extra degenerate triangles per row (except the last one)
	int nIndices = 2 * ( nTheta + 1 ) * ( nPhi - 1 );

//	MaterialSystemInterface()->Bind( m_pVertexColor[0] );

	glBegin( GL_TRIANGLE_STRIP );

	//
	// Build the index buffer.
	//
	int i, j;
	for ( i = 0; i < nPhi; ++i )
	{
		for ( j = 0; j < nTheta; ++j )
		{
			float u = j / (float)( nTheta - 1 );
			float v = i / (float)( nPhi - 1 );
			float theta = 2.0f * M_PI_F * u;
			float phi = M_PI_F * v;

			Vector vecPos;
			vecPos.x = flRadius * sin( phi ) * cos( theta );
			vecPos.y = flRadius * sin( phi ) * sin( theta );
			vecPos.z = flRadius * cos( phi );

			Vector vecNormal = vecPos;
			VectorNormalize( vecNormal );

			float flScale = LightPlane( vecNormal );

			unsigned char red = chRed * flScale;
			unsigned char green = chGreen * flScale;
			unsigned char blue = chBlue * flScale;

			vecPos += vCenter;

			glColor3ub( red, green, blue );
			glVertex3fv( vecPos.Base() );
		}
	}

	//
	// Emit the triangle strips.
	//
#if 0
	int idx = 0;
	for ( i = 0; i < nPhi - 1; ++i )
	{
		for ( j = 0; j < nTheta; ++j )
		{
			idx = nTheta * i + j;

			meshBuilder.Index( idx + nTheta );
			meshBuilder.AdvanceIndex();

			meshBuilder.Index( idx );
			meshBuilder.AdvanceIndex();
		}

		//
		// Emit a degenerate triangle to skip to the next row without
		// a connecting triangle.
		//
		if ( i < nPhi - 2 )
		{
			meshBuilder.Index( idx );
			meshBuilder.AdvanceIndex();

			meshBuilder.Index( idx + nTheta + 1 );
			meshBuilder.AdvanceIndex();
		}
	}
#endif

	glEnd();
}

void CRender3DGL::RenderWireframeSphere( Vector const &vCenter, float flRadius, int nTheta, int nPhi, unsigned char chRed, unsigned char chGreen, unsigned char chBlue )
{

}

void CRender3DGL::RenderLine( const Vector &vec1, const Vector &vec2, unsigned char r, unsigned char g, unsigned char b )
{
	glBegin( GL_LINES );

	glColor3ub( r, g, b );

	glVertex3fv( vec1.Base() );

	glVertex3fv( vec2.Base() );

	glEnd();
}


void CRender3DGL::WorldToScreen( Vector2D &Screen, const Vector &World )
{
	// FIXME: Should this computation move into the material system?
	matrix4_t ViewMatrix;
	matrix4_t ProjectionMatrix;
	matrix4_t ViewProjMatrix;
//	int viewX, viewY, viewWidth, viewHeight;

	glGetFloatv( GL_MODELVIEW_MATRIX, &ViewMatrix[0][0] );
	glGetFloatv( GL_PROJECTION_MATRIX, &ProjectionMatrix[0][0] );

	TransposeMatrix( ViewMatrix );
	TransposeMatrix( ProjectionMatrix );

	MultiplyMatrix( ProjectionMatrix, ViewMatrix, ViewProjMatrix );

	Vector ProjPoint;
	ProjectPoint( ProjPoint, World, ViewProjMatrix );

	// NOTE: The negative sign on y is because wc wants to think about screen
	// coordinates in a different way than the material system
	Screen[0] = 0.5f * ( ProjPoint[0] + 1.0f ) * m_nWidth /*+ viewX*/;
	Screen[1] = 0.5f * ( -ProjPoint[1] + 1.0f ) * m_nHeight /*+ viewY*/;
}

void CRender3DGL::ScreenToWorld( Vector &World, const Vector2D &Screen )
{
	// FIXME: Should this computation move into the material system?
	// FIXME: Haven't debugged this yet

	matrix4_t ViewMatrix;
	matrix4_t ProjectionMatrix;
	matrix4_t ViewProjInvMatrix;
//	int viewX, viewY, viewWidth, viewHeight;
	Vector temp;

	glGetFloatv( GL_MODELVIEW_MATRIX, &ViewMatrix[0][0] );
	glGetFloatv( GL_PROJECTION_MATRIX, &ProjectionMatrix[0][0] );

	TransposeMatrix( ViewMatrix );
	TransposeMatrix( ProjectionMatrix );

	MultiplyMatrix( ProjectionMatrix, ViewMatrix, ViewProjInvMatrix );
	MatrixInvert( ViewProjInvMatrix );

	temp[0] = 2.0f * Screen[0] / m_nWidth - 1;
	temp[1] = -2.0f * Screen[1] / m_nHeight + 1;
	temp[2] = 0.0f;

	ProjectPoint( World, temp, ViewProjInvMatrix );
}


void CRender3DGL::ScreenToClient( Vector2D &Client, const Vector2D &Screen )
{
	Client[0] = Screen[0];
	Client[1] = Screen[1];
}

void CRender3DGL::ClientToScreen( Vector2D &Screen, const Vector2D &Client )
{
	Screen[0] = Client[0];
	Screen[1] = Client[1];
}



#ifndef _WIN64
#error RenderHitTarget code is not 32-bit ready!
#endif

void CRender3DGL::BeginRenderHitTarget( CMapAtom *pObject, unsigned int uHandle /*= 0*/ )
{
	if ( m_Pick.bPicking )
	{
		GLuint ptra = (intptr_t)pObject & 0xFFFFFFFF;
		GLuint ptrb = (intptr_t)pObject >> 32;

		glPushName( ptra );
		glPushName( ptrb );
		glPushName( uHandle );
	}
}

void CRender3DGL::EndRenderHitTarget( void )
{
	if ( m_Pick.bPicking )
	{
		//
		// Pop the name and the handle from the stack.
		//

		glPopName();
		glPopName();
		glPopName();

		if ( m_Pick.nNumHits < MAX_PICK_HITS )
		{
			int numObjects = glRenderMode( GL_SELECT );
			if ( ( numObjects != 0 ) && m_Pick.uSelectionBuffer[0] == 3 )
			{
				intptr_t ptra = (intptr_t)m_Pick.uSelectionBuffer[3];
				intptr_t ptrb = (intptr_t)m_Pick.uSelectionBuffer[4] << 32;
				m_Pick.Hits[m_Pick.nNumHits].pObject = (CMapAtom *)( ptra + ptrb );
				m_Pick.Hits[m_Pick.nNumHits].uData = m_Pick.uSelectionBuffer[5];
				m_Pick.Hits[m_Pick.nNumHits].nDepth = m_Pick.uSelectionBuffer[1];
				m_Pick.nNumHits++;
			}
		}
	}
}


void CRender3DGL::BeginParallel( void )
{
	glDisable( GL_DEPTH_TEST );

	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();

	glOrthof( 0, (float)m_nWidth, (float)m_nHeight, 0, -COORD_NOTINIT, COORD_NOTINIT );

	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	glLoadIdentity();
}

void CRender3DGL::EndParallel( void )
{
	glMatrixMode( GL_MODELVIEW );
	glPopMatrix();

	glMatrixMode( GL_PROJECTION );
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}


// HACK: This is a fixup for bizarre MFC behavior with the drop-down boxes
void CRender3DGL::ResetFocus()
{
	// A bizarre workaround; the drop-down menu somehow
	// sets some wierd state that causes the whole screen to not be updated
	// SlartTodo: Is this valid still?
	InvalidateRect( m_hWnd, nullptr, false );
}


// indicates we need to render an overlay pass...
bool CRender3DGL::NeedsOverlay() const
{
	return ( m_eCurrentRenderMode == RENDER_MODE_LIGHTMAP_GRID ) ||
		( m_eCurrentRenderMode == RENDER_MODE_TEXTURED );
}


// should I sort renders?
bool CRender3DGL::DeferRendering() const
{
	return m_DeferRendering;
}


void CRender3DGL::DebugHook1( void *pData /*= NULL*/ )
{
	g_bShowStatistics = !g_bShowStatistics;

#ifdef _DEBUG
//	m_bRecomputeFrustumRenderGeometry = true;
//	m_bRenderFrustum = true;
#endif
}

void CRender3DGL::DebugHook2( void *pData /*= NULL*/ )
{
	g_bRenderCullBoxes = !g_bRenderCullBoxes;
}
