
#include "stdafx.h"

#if 0

#include "Worldcraft.h"
#include "MDIClientWnd.h"

#include "wc_postinclude.h"


BEGIN_MESSAGE_MAP(CMDIClientWnd, CWnd)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_CREATE()
END_MESSAGE_MAP()


CMDIClientWnd::CMDIClientWnd()
{
}


CMDIClientWnd::~CMDIClientWnd()
{
}


// Makes our background color mesh with the splash screen for maximum effect.
BOOL CMDIClientWnd::OnEraseBkgnd(CDC *pDC)
{
	// Set brush to desired background color
	CBrush backBrush(RGB(141, 136, 130)); // This color blends with the splash image!

	// Save old brush
	CBrush *pOldBrush = pDC->SelectObject(&backBrush);

	CRect rect;
	pDC->GetClipBox(&rect);     // Erase the area needed

	pDC->PatBlt(rect.left, rect.top, rect.Width(), rect.Height(), PATCOPY);

	pDC->SelectObject(pOldBrush);
	return TRUE;
} 

#endif
