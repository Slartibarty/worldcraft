//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#include "stdafx.h"
#include "render3d.h"
#include "render3d_stub.h"
#include "render3dgl.h"

#include "wc_postinclude.h"


CRender3D *CreateRender3D(Render3DType_t eRender3DType)
{
	switch (eRender3DType)
	{
		case Render3DTypeOpenGL:
		{
			return(new CRender3DGL());
		}
	}

	return(new CRender3DStub());
}
