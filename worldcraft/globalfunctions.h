//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#ifndef GLOBALFUNCTIONS_H
#define GLOBALFUNCTIONS_H
#ifdef _WIN32
#pragma once
#endif

#include "MapClass.h"	// For CMapObjectList

#include "random.h"
#include "stringtools.h"


class CMapSolid;
class CMainFrame;
class CMapWorld;
class CMapDoc;
class CSubdivMesh;


CMapWorld *GetActiveWorld(void);

//
// misc.cpp:
//

namespace wc
{
	// Get the default font
	CFont &GetDefaultFont();

}

void NotifyDuplicates(CMapSolid *pSolid);
void NotifyDuplicates(CMapObjectList *pList);

LPCTSTR GetDefaultTextureName();
void SetDefaultTextureName( const char *szTexName );


//
// Message window interface.
//
enum MWMSGTYPE
{	mwStatus,
	mwError,
	mwWarning
};

void Msg(MWMSGTYPE type, LPCTSTR fmt, ...);

// noise function
float PerlinNoise2D( float x, float y, float rockiness );
float PerlinNoise2DScaled( float x, float y, float rockiness );

#endif // GLOBALFUNCTIONS_H
