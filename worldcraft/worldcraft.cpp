//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: The application object.
//
// $NoKeywords: $
//=============================================================================

#include "stdafx.h"
#include <io.h>
#include <stdlib.h>
#include <direct.h>
#include <timeapi.h>
#include "BuildNum.h"
#include "Splash.h"
#include "Options.h"
#include "MainFrm.h"
#include "ChildFrm.h"
#include "MapDoc.h"
#include "MapView3D.h"
#include "MapView2D.h"
#include "GlobalFunctions.h"
#include "Shell.h"
#include "ShellMessageWnd.h"
#include "Options.h"
#include "TextureSystem.h"
#include "ToolManager.h"
#include "Worldcraft.h"
#ifdef SOURCE_MATERIALSYSTEM
#include "StudioModel.h"
#endif
#include "statusbarids.h"
#include "stringtools.h"

#include "wc_postinclude.h"


static BOOL bMakeLib = FALSE;

static float fSequenceVersion = 0.2f;
static char *pszSequenceHdr = "Worldcraft Command Sequences\r\n\x1a";


CWorldcraft theApp;
COptions Options;

CShell g_Shell;
CShellMessageWnd g_ShellMessageWnd;


class CWorldcraftCmdLine : public CCommandLineInfo
{
	public:

		CWorldcraftCmdLine(void)
		{
			m_bShowLogo = TRUE;
		}

		void ParseParam(LPCTSTR lpszParam, BOOL bFlag, BOOL bLast)
		{
			void MakePrefabLibrary(LPCTSTR pszName);

			if (bFlag && !strcmp(lpszParam, "nologo"))
			{
				m_bShowLogo = FALSE;
			}
			else if (bFlag && !strcmpi(lpszParam, "makelib"))
			{
				bMakeLib = TRUE;
			}
			else if (!bFlag && bMakeLib)
			{
				MakePrefabLibrary(lpszParam);
			}
			else
			{
				CCommandLineInfo::ParseParam(lpszParam, bFlag, bLast);
			}
		}

		BOOL m_bShowLogo;
};


BEGIN_MESSAGE_MAP(CWorldcraft, CWinApp)
	//{{AFX_MSG_MAP(CWorldcraft)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, OnFileNew)
END_MESSAGE_MAP()


//-----------------------------------------------------------------------------
// Purpose: Constructor. Initializes member variables and creates a scratch
//			buffer for use when loading WAD files.
//-----------------------------------------------------------------------------
CWorldcraft::CWorldcraft(void)
{
	SetAppID( APP_ID );

	m_bActiveApp = true;
	m_bEnable3DRender = true;
	m_SuppressVideoAllocation = false;
	m_bForceRenderNextFrame = false;
}


//-----------------------------------------------------------------------------
// Purpose: Destructor. Frees scratch buffer used when loading WAD files.
//			Deletes all command sequences used when compiling maps.
//-----------------------------------------------------------------------------
CWorldcraft::~CWorldcraft(void)
{
}


//-----------------------------------------------------------------------------
// Purpose: Adds a backslash to the end of a string if there isn't one already.
// Input  : psz - String to add the backslash to.
//-----------------------------------------------------------------------------
static void EnsureTrailingBackslash(char *psz)
{
	if ((psz[0] != '\0') && (psz[strlen(psz) - 1] != '\\'))
	{
		strcat(psz, "\\");
	}
}


//-----------------------------------------------------------------------------
// Purpose: 
// Input  : eDir - 
//			szDir - 
//-----------------------------------------------------------------------------
void CWorldcraft::AppendSearchSubDir(SearchDir_t eDir, char *szDir)
{
	EnsureTrailingBackslash(szDir);

	switch (eDir)
	{
		//
		// Get the wads directory with a trailing backslash. This will be under
		// the mod directory (or the game directory if there is no mod directory).
		//
		case SEARCHDIR_WADS:
		{
			strcat(szDir, "wads\\");
			break;
		}

		//
		// Get the sprites directory with a trailing backslash. This will be under
		// the mod directory (or the game directory if there is no mod directory).
		//
		case SEARCHDIR_SPRITES:
		{
			strcat(szDir, "materials\\sprites\\");
			break;
		}

		//
		// Get the sounds directory with a trailing backslash. This will be under
		// the mod directory (or the game directory if there is no mod directory).
		//
		case SEARCHDIR_SOUNDS:
		{
			strcat(szDir, "sound\\");
			break;
		}

		//
		// Get the models directory with a trailing backslash. This will be under
		// the mod directory (or the game directory if there is no mod directory).
		//
		case SEARCHDIR_MODELS:
		{
			strcat(szDir, "models\\");
			break;
		}

		//
		// Get the scenes directory with a trailing backslash. This will be under
		// the mod directory (or the game directory if there is no mod directory).
		//
		case SEARCHDIR_SCENES:
		{
			strcat(szDir, "scenes\\");
			break;
		}
	}
}


//-----------------------------------------------------------------------------
// Purpose: Retrieves various important directories.
// Input  : dir - Enumerated directory to retrieve.
//			p - Pointer to buffer that receives the full path to the directory.
//-----------------------------------------------------------------------------
void CWorldcraft::GetDirectory(DirIndex_t dir, char *p)
{
	switch (dir)
	{
		case DIR_PROGRAM:
		{
			strcpy(p, m_szAppDir);
			EnsureTrailingBackslash(p);
			break;
		}

		case DIR_PREFABS:
		{
			strcpy(p, m_szAppDir);
			EnsureTrailingBackslash(p);
			strcat(p, "prefabs");

			//
			// Make sure the prefabs directory exists.
			//
			if ((_access( p, 0 )) == -1)
			{
				CreateDirectory(p, NULL);
			}
			break;
		}

		//
		// Get the game directory with a trailing backslash. This is
		// where the base game's resources are, such as "C:\Half-Life\valve\".
		//
		case DIR_GAME_EXE:
		{
			strcpy(p, g_pGameConfig->m_szGameExeDir);
			EnsureTrailingBackslash(p);
			break;
		}

		//
		// Get the mod directory with a trailing backslash. This is where
		// the mod's resources are, such as "C:\Half-Life\tfc\".
		//
		case DIR_MOD:
		{
			strcpy(p, g_pGameConfig->m_szModDir);
			EnsureTrailingBackslash(p);
			break;
		}

		//
		// Get the game directory with a trailing backslash. This is where
		// the game is installed, such as "C:\Half-Life\".
		//
		case DIR_GAME:
		{
			strcpy(p, g_pGameConfig->m_szGameDir);
			EnsureTrailingBackslash(p);
			break;
		}

	}
}


//-----------------------------------------------------------------------------
// Purpose: Always returns the game directory.
// Input  : szDir - Buffer to receive first search directory.
//			pos - Iterator.
//-----------------------------------------------------------------------------
void CWorldcraft::GetFirstSearchDir(SearchDir_t eDir, char *szDir, POSITION &pos)
{
	//
	// First, return the mod directory. Eventually this may be a list of
	// search directories configurable by the user.
	//
	GetDirectory(DIR_MOD, szDir);
	pos = (POSITION)1;

	//
	// If there's no mod directory, skip to the next search directory.
	//
	if (szDir[0] == '\0')
	{
		GetNextSearchDir(eDir, szDir, pos);
	}
	else
	{
		AppendSearchSubDir(eDir, szDir);
	}
}


//-----------------------------------------------------------------------------
// Purpose: Returns the next directory in the search path.
// Input  : szDir - Buffer to receive next search directory.
//			pos - Iterator filled out by a previous call to GetFirst/NextSearchDir.
//-----------------------------------------------------------------------------
void CWorldcraft::GetNextSearchDir(SearchDir_t eDir, char *szDir, POSITION &pos)
{
	// Slart: This sucks

	//
	// Second, return the base game directory.
	//
	if ((intptr_t)pos == 1)
	{
		GetDirectory(DIR_GAME, szDir);
		pos = (POSITION)((intptr_t)pos + 1);
	}
	//
	// Last, return the Worldcraft directory.
	//
	else if ((intptr_t)pos == 2)
	{
		GetDirectory(DIR_PROGRAM, szDir);
		pos = (POSITION)((intptr_t)pos + 1);
	}
	else
	{
		pos = NULL;
	}

	if (pos != NULL)
	{
		AppendSearchSubDir(eDir, szDir);
	}
}


//-----------------------------------------------------------------------------
// Purpose: Returns a color from the application configuration storage.
//-----------------------------------------------------------------------------
COLORREF CWorldcraft::GetProfileColor(const char *pszSection, const char *pszKey, int r, int g, int b)
{
	int newR, newG, newB;
	
	CString strDefault;
	CString strReturn;
	char szBuff[128];
	V_sprintf(szBuff, "%i %i %i", r, g, b);

	strDefault = szBuff;

	strReturn = GetProfileString(pszSection, pszKey, strDefault);

	if (strReturn.IsEmpty())
		return 0;

	// Parse out colors.
	char *pStart;
	char *pCurrent;
	pStart = szBuff;
	pCurrent = pStart;
	
	strcpy( szBuff, strReturn.GetString() );

	while (*pCurrent && *pCurrent != ' ')
		pCurrent++;

	*pCurrent++ = 0;
	newR = atoi(pStart);

	pStart = pCurrent;
	while (*pCurrent && *pCurrent != ' ')
		pCurrent++;

	*pCurrent++ = 0;
	newG = atoi(pStart);

	pStart = pCurrent;
	while (*pCurrent)
		pCurrent++;

	*pCurrent++ = 0;
	newB = atoi(pStart);

	return COLORREF(RGB(newR, newG, newB));
}


UINT LazyLoadProc( LPVOID pParam )
{
	Msg( mwStatus, "Starting lazy loading textures...\n" );
	DWORD start = timeGetTime();

	g_Textures.LazyLoadTextures();

	DWORD end = timeGetTime();
	Msg( mwStatus, "Finished lazy loading textures: %g s...\n", MS2SEC( end - start ) );

	return 0;
}


//-----------------------------------------------------------------------------
// Purpose: Intializes this instance of Worldcraft.
// Output : Returns TRUE on success, FALSE on failure.
//-----------------------------------------------------------------------------
BOOL CWorldcraft::InitInstance(void)
{
	CWinApp::InitInstance();

	// Init mathlib
	MathLib_Init( 2.2f, 2.2f, 0.0f, 2.0f );

	EnableTaskbarInteraction(FALSE);

	// ensure we're in the same directory as the .EXE
	// debug builds use the working directory
#ifndef _DEBUG
	GetModuleFileName( NULL, m_szAppDir, MAX_PATH );
	char *p = strrchr( m_szAppDir, '\\' );
	if ( p )
	{
		// chop off \wc.exe
		p[0] = '\0';
	}
#else
	GetCurrentDirectory( MAX_PATH, m_szAppDir );
#endif

#ifndef _DEBUG
	SetCurrentDirectory( m_szAppDir );
#endif

	SetRegistryKey(REGISTRY_COMPANY_NAME);

	//
	// Create a window to receive shell commands from the engine, and attach it
	// to our shell processor.
	//
	g_ShellMessageWnd.Create();
	g_ShellMessageWnd.SetShell(&g_Shell);

	//
	// Create the tool manager singleton.
	//
	CToolManager::Init();

	// Parse command line for standard shell commands, DDE, file open
	CWorldcraftCmdLine cmdInfo;
	ParseCommandLine(cmdInfo);

	if (bMakeLib)
	{
		return(FALSE);	// made library .. don't want to enter program
	}

	//
	// Create and optionally display the splash screen.
	//
#ifndef DISABLE_SPLASH
	CSplashWnd::EnableSplashScreen(cmdInfo.m_bShowLogo);
#endif

	LoadSequences();	// load cmd sequences - different from options because
						//  users might want to share (darn registry)

	// other init:
	RandomSeed( (int)time( NULL ) );	// SlartTodo: This'll break soon...

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.
	pMapDocTemplate = new CMultiDocTemplate(
		IDR_FORGEMAPTYPE,
		RUNTIME_CLASS(CMapDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CMapView2D));
	if (!pMapDocTemplate)
		return FALSE;
	AddDocTemplate(pMapDocTemplate);

	// Enable DDE Execute open
	// EnableShellOpen();
	// RegisterShellFileTypes(TRUE);

	//
	// Initialize the rich edit control so we can use it in the entity help dialog.
	//
	AfxInitRichEdit();

	//
	// Create main MDI Frame window. Must be done AFTER registering the multidoc template!
	//
	CMainFrame *pMainFrame = new CMainFrame;
	if (!pMainFrame->LoadFrame(IDR_MAINFRAME))
	{
		delete pMainFrame;
		return(FALSE);
	}
	m_pMainWnd = pMainFrame;

	// call DragAcceptFiles only if there's a suffix
	//  In an MDI app, this should occur immediately after setting m_pMainWnd
	// Enable drag/drop open
	m_pMainWnd->DragAcceptFiles();

	if(!cmdInfo.m_strFileName.IsEmpty())
	{
		// we don't want to make a new file (default behavior if no file
		//  is specified on the commandline.)

		// Dispatch commands specified on the command line
		if (!ProcessShellCommand(cmdInfo))
			return FALSE;
	}

	Msg( mwStatus, "Worldcraft build %d (%s %s)", build_number(), __DATE__, __TIME__ );

	// Init OpenGL function pointers
	extern bool InitialiseOpenGL();
	if ( !InitialiseOpenGL() )
	{
		//return FALSE;
	}

#ifndef DISABLE_SPLASH
	CSplashWnd::ShowSplashScreen();
#endif

	//
	// Load the options.
	//
	Options.Init();

	//
	// Initialize the texture manager and load all textures.
	//
	if (g_Textures.Initialize())
	{
#ifdef SOURCE_MATERIALSYSTEM
		//
		// Initialize studio model rendering (must happen after g_Textures.Initialize since
		// g_Textures.Initialize kickstarts the material system and sets up g_MaterialSystemClientFactory)
		//
		StudioModel::Initialize();
#endif
	}

	g_Textures.LoadAllGraphicsFiles();

	// Start lazy loading textures
	AfxBeginThread( LazyLoadProc, nullptr, THREAD_PRIORITY_IDLE );

	//
	// The main window has been initialized, so show and update it.
	//
	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->UpdateWindow();

	return TRUE;
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
int CWorldcraft::ExitInstance()
{
	m_bClosing = true;

	//
	// Delete the tool manager singleton.
	//
	CToolManager::Shutdown();

	g_Textures.ShutDown();

	//
	// Delete the command sequences.
	//
	int nSequenceCount = m_CmdSequences.GetSize();
	for (int i = 0; i < nSequenceCount; i++)
	{
		CCommandSequence *pSeq = m_CmdSequences[i];
		if (pSeq != NULL)
		{
			delete pSeq;
			m_CmdSequences[i] = NULL;
		}
	}

	return CWinApp::ExitInstance();
}


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
};


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}


//-----------------------------------------------------------------------------
// Purpose: 
// Input  : pDX - 
//-----------------------------------------------------------------------------
void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


//-----------------------------------------------------------------------------
// Purpose: 
// Output : Returns TRUE on success, FALSE on failure.
//-----------------------------------------------------------------------------
BOOL CAboutDlg::OnInitDialog(void)
{
	CDialog::OnInitDialog();

	//
	// Display the build number.
	//
	CWnd *pWnd = GetDlgItem(IDC_BUILD_NUMBER);
	if (pWnd != NULL)
	{
		char szTemp1[MAX_PATH];
		char szTemp2[MAX_PATH];
		int nBuild = build_number();
		pWnd->GetWindowText(szTemp1, sizeof(szTemp1));
		V_sprintf(szTemp2, szTemp1, nBuild);
		pWnd->SetWindowText(szTemp2);
	}

	return TRUE;
}


BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWorldcraft::OnAppAbout(void)
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}



//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWorldcraft::OnFileNew(void)
{
	pMapDocTemplate->OpenDocumentFile(NULL);
	if(Options.general.bLoadwinpos && Options.general.bIndependentwin)
	{
		::GetMainWnd()->LoadWindowStates();
	}
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWorldcraft::OnFileOpen(void)
{
	static char szInitialDir[MAX_PATH] = "";
	if (szInitialDir[0] == '\0')
	{
		strcpy(szInitialDir, g_pGameConfig->szMapDir);
	}

	// TODO: need to prevent (or handle) opening VMF files when using old map file formats
	CFileDialog dlg(TRUE, NULL, NULL, OFN_LONGNAMES | OFN_HIDEREADONLY | OFN_NOCHANGEDIR,
		"Map Files (*.vmf;*.rmf;*.map)|*.vmf; *.rmf; *.map|"
		"Valve Map Files (*.vmf)|*.vmf|Rich Map Files (*.rmf)|*.rmf|" "Game Maps (*.map)|*.map||");
	dlg.m_ofn.lpstrInitialDir = szInitialDir;
	int iRvl = dlg.DoModal();

	if (iRvl == IDCANCEL)
	{
		return;
	}

	//
	// Get the directory they browsed to for next time.
	//
	CString str = dlg.GetPathName();
	int nSlash = str.ReverseFind('\\');
	if (nSlash != -1)
	{
		strcpy(szInitialDir, str.Left(nSlash));
	}

	if (str.Find('.') == -1)
	{
		switch (dlg.m_ofn.nFilterIndex)
		{
			case 1:
			{
				str += ".vmf";
				break;
			}

			case 2:
			{
				str += ".rmf";
				break;
			}

			case 3:
			{
				str += ".map";
				break;
			}
		}
	}

	OpenDocumentFile(str);
}


//-----------------------------------------------------------------------------
// Purpose: 
// Input  : lpszFileName - 
// Output : CDocument*
//-----------------------------------------------------------------------------
CDocument* CWorldcraft::OpenDocumentFile(LPCTSTR lpszFileName) 
{
	if(GetFileAttributes(lpszFileName) == INVALID_FILE_ATTRIBUTES)
	{
		AfxMessageBox("That file does not exist.");
		return NULL;
	}

	CDocument *pDoc = pMapDocTemplate->OpenDocumentFile(lpszFileName);

	if(Options.general.bLoadwinpos && Options.general.bIndependentwin)
	{
		::GetMainWnd()->LoadWindowStates();
	}

	return pDoc;
}


//-----------------------------------------------------------------------------
// Purpose: 
// Input  : pMsg - 
// Output : Returns TRUE on success, FALSE on failure.
//-----------------------------------------------------------------------------
BOOL CWorldcraft::PreTranslateMessage(MSG* pMsg)
{
#ifndef DISABLE_SPLASH
	// CG: The following lines were added by the Splash Screen component.
	if (CSplashWnd::PreTranslateAppMessage(pMsg))
		return TRUE;
#endif

	return CWinApp::PreTranslateMessage(pMsg);
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWorldcraft::LoadSequences(void)
{
	std::ifstream file("CmdSeq.wc", std::ios::in | std::ios::binary);
	
	if(!file.is_open())
		return;	// none to load

	// skip past header & version
	float fThisVersion;

	file.seekg(strlen(pszSequenceHdr));
	file.read((char*)&fThisVersion, sizeof fThisVersion);

	// read number of sequences
	DWORD dwSize;
	int nSeq;

	file.read((char*)&dwSize, sizeof dwSize);
	nSeq = dwSize;

	for(int i = 0; i < nSeq; i++)
	{
		CCommandSequence *pSeq = new CCommandSequence;
		file.read(pSeq->m_szName, 128);

		// read commands in sequence
		file.read((char*)&dwSize, sizeof dwSize);
		int nCmd = dwSize;
		CCOMMAND cmd;
		for(int iCmd = 0; iCmd < nCmd; iCmd++)
		{
			if(fThisVersion < 0.2f)
			{
				file.read((char*)&cmd, sizeof(CCOMMAND)-1);
				cmd.bNoWait = FALSE;
			}
			else
			{
				file.read((char*)&cmd, sizeof(CCOMMAND));
			}
			pSeq->m_Commands.Add(cmd);
		}

		m_CmdSequences.Add(pSeq);
	}
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWorldcraft::SaveSequences(void)
{
	std::ofstream file("CmdSeq.wc", std::ios::out | std::ios::binary);

	// write header
	file.write(pszSequenceHdr, strlen(pszSequenceHdr));
	// write out version
	file.write((char*)&fSequenceVersion, sizeof(float));

	// write out each sequence..
	int i, nSeq = m_CmdSequences.GetSize();
	DWORD dwSize = nSeq;
	file.write((char*)&dwSize, sizeof dwSize);
	for(i = 0; i < nSeq; i++)
	{
		CCommandSequence *pSeq = m_CmdSequences[i];

		// write name of sequence
		file.write(pSeq->m_szName, 128);
		// write number of commands
		int nCmd = pSeq->m_Commands.GetSize();
		dwSize = nCmd;
		file.write((char*)&dwSize, sizeof dwSize);
		// write commands .. 
		for(int iCmd = 0; iCmd < nCmd; iCmd++)
		{
			CCOMMAND &cmd = pSeq->m_Commands[iCmd];
			file.write((char*)&cmd, sizeof cmd);
		}
	}
}


void CWorldcraft::SetForceRenderNextFrame()
{
	m_bForceRenderNextFrame = true;
}


bool CWorldcraft::GetForceRenderNextFrame()
{
	return m_bForceRenderNextFrame;
}


//-----------------------------------------------------------------------------
// Purpose: Performs idle processing. Runs the frame and does MFC idle processing.
// Input  : lCount - The number of times OnIdle has been called in succession,
//				indicating the relative length of time the app has been idle without
//				user input.
// Output : Returns TRUE if there is more idle processing to do, FALSE if not.
//-----------------------------------------------------------------------------
BOOL CWorldcraft::OnIdle(LONG lCount)
{
	static int lastPercent = -20000;
	int curPercent = -10000;

	// Update the status text.
	if( curPercent == -10000 )
	{
		SetStatusText( SBI_LIGHTPROGRESS, "<->" );
	}
	else if( curPercent != lastPercent )
	{
		char str[256];
		V_sprintf( str, "%.2f%%", curPercent / 100.0f );
		SetStatusText( SBI_LIGHTPROGRESS, str );
	}

	lastPercent = curPercent;	
	return(CWinApp::OnIdle(lCount));
}


//-----------------------------------------------------------------------------
// Purpose: Renders the realtime views.
//-----------------------------------------------------------------------------
void CWorldcraft::RunFrame(void)
{
	if (!IsActiveApp())
	{
		Sleep(50);
	}

	//
	// Only do realtime stuff when we are the active application.
	//
	if (CMapDoc::GetActiveMapDoc() && 
		m_bActiveApp && 
		m_bEnable3DRender )
	{
		// get the time
		CMapDoc::GetActiveMapDoc()->UpdateCurrentTime();

		// run any animation
		CMapDoc::GetActiveMapDoc()->UpdateAnimation();

		// redraw the 3d views
		CMapDoc::GetActiveMapDoc()->Update3DViews();
	}

	m_bForceRenderNextFrame = false;
}


//-----------------------------------------------------------------------------
// Purpose: Overloaded Run so that we can control the frameratefor realtime
//			rendering in the 3D view.
// Output : As MFC CWinApp::Run.
//-----------------------------------------------------------------------------
int CWorldcraft::Run(void)
{
	ASSERT_VALID(this);

	MSG msg;

	// For tracking the idle time state
	bool bIdle = true;
	long lIdleCount = 0;

	//
	// Acquire and dispatch messages until a WM_QUIT message is received.
	//
	while (true)
	{
		RunFrame();

		if (bIdle && !OnIdle(lIdleCount++))
		{
			bIdle = false;
		}

		//
		// Pump messages until the message queue is empty.
		//
		while (::PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				return(ExitInstance());
			}

			if (!PreTranslateMessage(&msg))
			{
				::TranslateMessage(&msg);
				::DispatchMessage(&msg);
			}

			// Reset idle state after pumping idle message.
			if (IsIdleMessage(&msg))
			{
				bIdle = true;
				lIdleCount = 0;
			}
		}
	}

	// not reachable
}


//-----------------------------------------------------------------------------
// Purpose: Returns true if Worldcraft is the active app, false if not.
//-----------------------------------------------------------------------------
bool CWorldcraft::IsActiveApp(void)
{
	return(m_bActiveApp);
}


//-----------------------------------------------------------------------------
// Purpose: Called from CMainFrame::OnSysCommand, this informs the app when it
//			is minimized and unminimized. This allows us to stop rendering the
//			3D view when we are minimized.
// Input  : bMinimized - TRUE when minmized, FALSE otherwise.
//-----------------------------------------------------------------------------
void CWorldcraft::OnActivateApp(bool bActive)
{
	m_bActiveApp = bActive;
}


//-----------------------------------------------------------------------------
// Purpose: 
// Input  : bEnable - 
//-----------------------------------------------------------------------------
bool CWorldcraft::Is3DRenderEnabled(void)
{
	return(m_bEnable3DRender);
}


//-----------------------------------------------------------------------------
// Purpose: 
// Input  : bEnable - 
//-----------------------------------------------------------------------------
void CWorldcraft::Enable3DRender(bool bEnable)
{
	m_bEnable3DRender = bEnable;
}


//-----------------------------------------------------------------------------
// Purpose: Called from the shell to relinquish our video memory in favor of the
//			engine. This is called by the engine when it starts up.
//-----------------------------------------------------------------------------
void CWorldcraft::ReleaseVideoMemory()
{
   POSITION pos = GetFirstDocTemplatePosition();

   while (pos)
   {
      CDocTemplate* pTemplate = (CDocTemplate*)GetNextDocTemplate(pos);
      POSITION pos2 = pTemplate->GetFirstDocPosition();
      while (pos2)
      {
         CDocument * pDocument;
         if ((pDocument=pTemplate->GetNextDoc(pos2)) != NULL)
		 {
			 static_cast<CMapDoc*>(pDocument)->ReleaseVideoMemory();
		 }
      }
   }
} 

void CWorldcraft::SuppressVideoAllocation( bool bSuppress )
{
	m_SuppressVideoAllocation = bSuppress;
} 

bool CWorldcraft::CanAllocateVideo() const
{
	return !m_SuppressVideoAllocation;
}

