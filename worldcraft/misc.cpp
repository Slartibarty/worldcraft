//========= Copyright � 1996-2002, Valve LLC, All rights reserved. ============
//
// Purpose: Miscellaneous utility functions.
//
// $NoKeywords: $
//=============================================================================

#include "stdafx.h"
#include <direct.h>
#include <time.h>
#include "MapSolid.h"

#include "wc_postinclude.h"


namespace wc
{
	static void CreateDevFont( CFont &font )
	{
#if 0
		NONCLIENTMETRICS ncm;
		ncm.cbSize = sizeof( ncm );

		SystemParametersInfo( SPI_GETNONCLIENTMETRICS, sizeof( ncm ), &ncm, 0 );

		font.CreateFontIndirect( &ncm.lfMessageFont );
#else
		font.CreatePointFont( 9 * 10, "Consolas" );
#endif
	}

	//-----------------------------------------------------------------------------
	// Purpose: Returns the best fitting OS font
	//-----------------------------------------------------------------------------
	CFont &GetDefaultFont()
	{
		static CFont font;

		if ( !font.m_hObject )
		{
			CreateDevFont( font );
		}

		return font;
	}

}


// MapCheckDlg.cpp:
BOOL DoesContainDuplicates(CMapSolid *pSolid);
static BOOL bCheckDupes = FALSE;


void NotifyDuplicates(CMapSolid *pSolid)
{
	if(!bCheckDupes)
		return;	// stop that

	if(DoesContainDuplicates(pSolid))
	{
		if(IDNO == AfxMessageBox("Duplicate Plane! Do you want more messages?", 
			MB_YESNO))
		{
			bCheckDupes = FALSE;
		}
	}
}


void NotifyDuplicates(CMapObjectList *pList)
{
	if(!bCheckDupes)
		return;	// stop that

	POSITION p = pList->GetHeadPosition();
	while(p)
	{
		CMapClass *pobj = pList->GetNext(p);
		if(!pobj->IsMapClass(MAPCLASS_TYPE(CMapSolid)))
			continue;	// not a solid
		NotifyDuplicates((CMapSolid*) pobj);
	}
}
