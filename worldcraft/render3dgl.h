
#pragma once

#include "render3d.h"
#include "camera.h"
#include "vector4d.h"

#include "GL/glew.h"
#include "GL/wglew.h"

#include <vector>

class CMapClass;
class CCullTreeNode;

//
// Size of the buffer used for picking. See glSelectBuffer for documention on
// the contents of the selection buffer.
//
#define SELECTION_BUFFER_SIZE	50

//
// Size of the texture cache. THis is the maximum number of unique textures that
// a map can refer to and still render properly in Worldcraft.
//
#define TEXTURE_CACHE_SIZE		2048

//
// Maximum number of objects that can be kept in the list of objects to render last.
//
#define MAX_RENDER_LAST_OBJECTS	256

//
// Maximum number of hits that can be returned by ObjectsAt.
//
#define MAX_PICK_HITS			512

//
// Render state information set via RenderEnable:
//
struct RenderStateInfo_t
{
	bool bCenterCrosshair;	// Whether to render the center crosshair.
	bool bDrawFrameRect;	// Whether to render a rectangle around the view.
	bool bDrawGrid;			// Whether to render the grid.
	float fGridSpacing;		// Grid spacing in world units.
	float fGridDistance;	// Maximum distance from camera to draw grid.
	bool bFilterTextures;	// Whether to filter textures.
	bool bReverseSelection;	// Driver issue fix - whether to return the largest (rather than smallest) Z value when picking
};

//
// Picking state information used when called from ObjectsAt.
//
struct PickInfo_t
{
	bool bPicking;							// Whether we are rendering in pick mode or not.

	float fX;								// Leftmost coordinate of pick rectangle, passed in by caller.
	float fY;								// Topmost coordinate of pick rectangle, passed in by caller.
	float fWidth;							// Width of pick rectangle, passed in by caller.
	float fHeight;							// Height of pick rectangle, passed in by caller.

	RenderHitInfo_t *pHitsDest;				// Final array in which to place pick hits, passed in by caller.
	int nMaxHits;							// Maximum number of hits to place in the 'pHits' array, passed in by caller, must be <= MAX_PICK_HITS.

	RenderHitInfo_t Hits[MAX_PICK_HITS];	// Temporary array in which to place unsorted pick hits.
	int nNumHits;							// Number of hits so far in this pick (number of hits in 'Hits' array).

	unsigned int uSelectionBuffer[SELECTION_BUFFER_SIZE];
	unsigned int uLastZ;
};


class CRender3DGL : public CRender3D
{
public:
	CRender3DGL(void);
	~CRender3DGL(void) override;

	void BindTexture(IEditorTexture *pTexture) override;

	bool Initialize(HWND hWnd, CMapDoc *pDoc) override;
	void ShutDown(void) override;

	RenderMode_t GetCurrentRenderMode(void) override;
	RenderMode_t GetDefaultRenderMode(void) override;
	float GetElapsedTime(void) override;
	float GetGridDistance(void) override;
	float GetGridSize(void) override;
	void GetViewPoint(Vector& pfViewPoint) override;
	void GetViewForward(Vector& pfViewForward) override;
	void GetViewUp(Vector& pfViewUp) override;
	void GetViewRight(Vector& pfViewRight) override;
	float GetRenderFrame(void) override;

	void UncacheAllTextures() override;

	bool IsEnabled(RenderState_t eRenderState) override;
	bool IsPicking(void) override;

	float LightPlane(Vector& Normal) override;

	int ObjectsAt(float x, float y, float fWidth, float fHeight, RenderHitInfo_t *pObjects, int nMaxObjects) override;

	void SetCamera(CCamera *pCamera) override;
	void SetDefaultRenderMode(RenderMode_t eRenderMode) override;
	void SetRenderMode(RenderMode_t eRenderMode, bool force = false) override;
	bool SetSize(int nWidth, int nHeight) override;
	void SetProjectionMode( ProjectionMode_t eProjMode ) override;

	void PreRender( void ) override;
	void PostRender( void ) override;

	void Render(void) override;
	void RenderEnable(RenderState_t eRenderState, bool bEnable) override;
	void RenderWireframeBox(const Vector &Mins, const Vector &Maxs, unsigned char chRed, unsigned char chGreen, unsigned char chBlue) override;
	void RenderBox(const Vector &Mins, const Vector &Maxs, unsigned char chRed, unsigned char chGreen, unsigned char chBlue, SelectionState_t eBoxSelectionState) override;
	void RenderArrow(Vector const &vStartPt, Vector const &vEndPt, unsigned char chRed, unsigned char chGreen, unsigned char chBlue) override;
	void RenderCone(Vector const &vBasePt, Vector const &vTipPt, float fRadius, int nSlices,
		unsigned char chRed, unsigned char chGreen, unsigned char chBlue ) override;
	void RenderSphere(Vector const &vCenter, float flRadius, int nTheta, int nPhi, unsigned char chRed, unsigned char chGreen, unsigned char chBlue) override;
	void RenderWireframeSphere(Vector const &vCenter, float flRadius, int nTheta, int nPhi, unsigned char chRed, unsigned char chGreen, unsigned char chBlue) override;
	void RenderLine(const Vector &vec1, const Vector &vec2, unsigned char r, unsigned char g, unsigned char b) override;

	void WorldToScreen(Vector2D &Screen, const Vector &World) override;
	void ScreenToWorld(Vector &World, const Vector2D &Screen) override;

	void ScreenToClient(Vector2D &Client, const Vector2D &Screen) override;
	void ClientToScreen(Vector2D &Screen, const Vector2D &Client) override;

	void BeginRenderHitTarget(CMapAtom *pObject, unsigned int uHandle = 0) override;
	void EndRenderHitTarget(void) override;

	void BeginParallel(void) override;
	void EndParallel(void) override;

	// HACK: This is a fixup for bizarre MFC behavior with the drop-down boxes
	void ResetFocus() override;

	// indicates we need to render an overlay pass...
	bool NeedsOverlay() const override;

	// should I sort renders?
	bool DeferRendering() const override;

	void DebugHook1(void *pData = NULL) override;
	void DebugHook2(void *pData = NULL) override;

private:
	void SetupRenderMode( RenderMode_t eRenderMode, bool bForce = false );
	void GetFrustumPlanes( Vector4D Planes[6] );
	void SetupCamera( CCamera *pCamera );
	void FindOrCreateTexture( IEditorTexture *pTexture );

	void RenderOverlayElements();
	void RenderTools();
	void RenderPointsFile();
	void RenderMapClass( CMapClass *pMapClass );
	void RenderNode( CCullTreeNode *pNode, bool bForce );
	Visibility_t IsBoxVisible( Vector const &BoxMins, Vector const &BoxMaxs );
	void RenderTree();


	// Represents an uploaded image
	struct Image_t
	{
		unsigned		uTexnum;
		IEditorTexture *pTexture;
	};

	// Window
	HINSTANCE	m_hInstance;
	HWND		m_hWnd;
	HDC			m_hDC;
	HGLRC		m_hGLRC;

	// Doc
	CMapDoc				*m_pDoc;				// Our document

	// Render options
	RenderMode_t		m_eCurrentRenderMode;	// Current render mode setting - Wireframe, flat, or textured.
	RenderMode_t		m_eDefaultRenderMode;	// Default render mode - Wireframe, flat, or textured.
	ProjectionMode_t    m_eProjMode;			// projection mode - PERSPECTIVE or ORTHOGRAPHIC
	RenderStateInfo_t	m_RenderState;			// Render state set via RenderEnable.
	PickInfo_t			m_Pick;					// Contains information used when rendering in pick mode.

	Image_t				*m_pBoundImage;

	bool				m_DeferRendering;		// Used when we want to sort lovely opaque objects

	// Camera
	CCamera				*m_pCamera;				// Camera to use for perspective transformations.
	Vector4D			m_FrustumPlanes[6];		// Plane normals and constants for the current view frustum.

	// W/H
	int					m_nWidth;				// Width of client area
	int					m_nHeight;				// Height of client area
	float				m_fAspect;

	// Misc
	float				m_fFrameRate;			// Framerate in frames per second, calculated once per second.
	int					m_nFramesThisSample;	// Number of frames rendered in the current sample period.
	DWORD				m_dwTimeLastSample;		// Time when the framerate was last calculated.

	DWORD				m_dwTimeLastFrame;		// The time when the previous frame was rendered.
	float				m_fTimeElapsed;			// Milliseconds elapsed since the last frame was rendered.

	// OpenGL internals

	std::vector<CMapAtom *> m_RenderLastObjects;		// List of objects to render after all the other objects.

	std::vector<Image_t> m_Images;

};
