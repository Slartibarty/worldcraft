//========= Copyright � 1996-2002, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#ifndef OBJECTBAR_H
#define OBJECTBAR_H
#ifdef _WIN32
#pragma once
#endif

#include "FilteredComboBox.h"
#include "WorldcraftBar.h"


class Axes2;
class BoundBox;
class CMapClass;
class Vector;
class CPrefab;

#define MAX_PREV_SEL 12


class CObjectBar : public CWorldcraftBar, public CFilteredComboBox::ICallbacks
{
public:
	
	CObjectBar();
	BOOL Create(CWnd *pParentWnd);
	
	static LPCTSTR GetDefaultEntityClass(void);

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	
	void UpdateListForTool(int iTool);
	void SetupForBlockTool();
	void DoHideControls();
	CMapClass *CreateInBox(BoundBox *pBox, Axes2 *pAxes = NULL);
	BOOL GetPrefabBounds(BoundBox *pBox);

	void DoDataExchange(CDataExchange *pDX);

	bool UseRandomYawOnEntityPlacement();

	bool IsEntityToolCreatingPrefab( void );
	bool IsEntityToolCreatingEntity( void );
	CMapClass *BuildPrefabObjectAtPoint( Vector const &HitPos );

	// CFilteredComboBox::ICallbacks implementation.

	void OnTextChanged( const char *pText ) override;

private:
	
	enum 
	{
		listPrimitives,
		listPrefabs,
		listEntities
	} ListType;
	
	//{{AFX_DATA(CMapViewBar)
	enum { IDD = IDD_OBJECTBAR };
	//}}AFX_DATA
	
	CFilteredComboBox	m_CreateList;				// this should really be m_ItemList
	CComboBox			m_CategoryList;
	CEdit				m_Faces;
	CSpinButtonCtrl		m_FacesSpin;

	CPrefab *FindPrefabByName( const char *pName );

	void LoadBlockCategories( void );
	void LoadEntityCategories( void );
	void LoadPrefabCategories( void );	

	void LoadBlockItems( void );
	void LoadEntityItems( void );
	void LoadPrefabItems( void );

	int GetPrevSelIndex(DWORD dwGameID, int *piNewIndex = NULL);
	BOOL EnableFaceControl(CWnd *pWnd, BOOL bModifyWnd);
	
	int iEntitySel;
	int iBlockSel;
	
	// previous selections:
	DWORD m_dwPrevGameID;
	struct tagprevsel
	{
		DWORD dwGameID;
		struct tagblock
		{
			CString strItem;
			CString strCategory;
		} block;
		struct tagentity
		{
			CString strItem;
			CString strCategory;
		} entity;
	} m_PrevSel[MAX_PREV_SEL];
	int m_iLastTool;
	
protected:
	
	afx_msg void UpdateControl(CCmdUI*);
	afx_msg void UpdateFaceControl(CCmdUI*);
	afx_msg void OnChangeCategory();
	
	DECLARE_MESSAGE_MAP()
};

#endif // OBJECTBAR_H
