// Implementation of the IEditorTexture interface for JaffaQuake materials

#pragma once


#include "IEditorTexture.h"


class IMaterialEnumerator;


#define INCLUDE_MODEL_MATERIALS		1
#define INCLUDE_WORLD_MATERIALS		2
#define INCLUDE_ALL_MATERIALS		UINT_MAX


class CJaffaMaterial : public IEditorTexture
{
public:

	static bool Initialize();
	static void Shutdown();
	static void EnumerateMaterials( IMaterialEnumerator *pEnum, intptr_t nContext, uint32 nFlags = INCLUDE_ALL_MATERIALS );
	static CJaffaMaterial *CJaffaMaterial::CreateMaterial( const char *pszMaterialName, bool bLoadImmediately, bool *pFound = nullptr );

	// Will actually load the material bits
	// We don't want to load them all at once because it takes way too long
	bool LoadMaterial();

	CJaffaMaterial();
	~CJaffaMaterial() override;

	int GetWidth() const override { return m_nWidth; }
	int GetHeight() const override { return m_nHeight; }

	float GetDecalScale() const override { return 1.0f; }

	const char *GetName() const override { return m_szName; };
	int GetShortName( char *szShortName ) const override;
	int GetKeywords( char *szKeywords ) const override;
	void Draw( CDC *pDC, RECT &rect, int iFontHeight, int iIconHeight, DWORD dwFlags = ( drawCaption | drawIcons ) ) override;
	TEXTUREFORMAT GetTextureFormat( void ) const override { return tfMAT; }
	int GetSurfaceAttributes( void ) const override { return 0; }
	int GetSurfaceContents( void ) const override { return 0; }
	int GetSurfaceValue( void ) const override { return 0; }
	imageflags_t GetImageFlags( void ) const { return m_flags; }
	bool ShouldRenderLast( void ) const { return m_flags; }
	CPalette *GetPalette( void ) const override { return nullptr; }
	bool HasData( void ) const override { return m_nTextureIndex != -1; }
	bool HasPalette( void ) const override { return false; }
	bool Load( void ) override;
	bool IsLoaded( void ) const override { return m_bLoaded; }
	const char *GetFileName( void ) const override { return m_szName; }

	byte *GetImageDataRGBA( int &nWidth, int &nHeight ) override;

	bool IsAlphaTested() const override { return m_bAlphaTest; }

	bool IsTranslucent() const override { return false; }

	bool IsDummy() const override { return false; }

	int GetTextureID() const override { return m_nTextureID; }

	void SetTextureID( int nTextureID ) override { m_nTextureID = nTextureID; }

private:

	// Used to draw the bitmap for the texture browser
	void DrawBitmap( CDC *pDC, RECT &srcRect, RECT &dstRect );

	bool ParseMaterial( char *data );

	char	m_szName[MAX_PATH]{};						// The name seen by users
	char	m_szFileName[MAX_PATH]{};					// Internal name, full path
	char	m_szKeywords[MAX_PATH]{ "SlartTodo!" };

	char	m_szBaseTexture[MAX_PATH]{};

	int		m_nWidth{};
	int		m_nHeight{};

	imageflags_t	m_flags{};

	int		m_nTextureIndex{};
	bool	m_bLoaded{};
	bool	m_bAlphaTest{};

	int		m_nTextureID{};			// Uniquely identifies this texture in all 3D renderers.

};
