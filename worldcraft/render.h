// A central location between the 2D and 3D GL renderers

#pragma once

//-----------------------------------------------------------------------------

extern HGLRC	g_CurrentHGLRC;		// A pointer to the HGLRC that is current right now, avoids api calls

void CreateGLContextForWindow( HWND &hWnd, HDC &hDC, HGLRC &hGLRC );

bool InitialiseOpenGL();
