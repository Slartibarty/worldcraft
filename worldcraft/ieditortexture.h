//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: Defines the interface a given texture for the 3D renderer. Current
//			implementations are for world textures (WADTexture.cpp) and sprite
//			textures (Texture.cpp).
//
// $NoKeywords: $
//=============================================================================

#ifndef IEDITORTEXTURE_H
#define IEDITORTEXTURE_H
#pragma once


#include "basetypes.h"


class CDC;
class CPalette;

//
// Set your texture ID to this in your implementation's constructor.
//
#define TEXTURE_ID_NONE	-1


//
// Texture formats. hack: MUST correlate with radio buttons in IDD_OPTIONS_CONFIGS.
//
enum TEXTUREFORMAT
{
	tfNone = -1,
	tfWAD,			// Quake 1
	tfWAL,			// Quake 2
	tfWAD3,			// Half-Life
	tfVMT,			// Source
	tfMAT			// JaffaQuake
};


//
// Draw flags.
//
#define drawCaption			0x01
#define	drawResizeAlways	0x02
#define	drawIcons			0x04
#define	drawErrors			0x08


// Image flags
// Mipmaps are opt-out
// Anisotropic filtering is opt-out
// Clamping is opt-in
//
// NOTABLE OVERSIGHT:
// The first material to use an image decides whether an image uses mipmaps or not
// This may confuse artists who expect a mipmapped texture, when the image was first
// referenced without them
// A solution to this problem is to generate mipmaps when required, but only once
//
using imageflags_t = uint8;

#define IF_NONE			0	// Gaben sound pack for FLStudio 6.1
#define IF_NOMIPS		1	// Do not use or generate mipmaps (infers no anisotropy)
#define IF_NOANISO		2	// Do not use anisotropic filtering (only applies to mipmapped images)
#define IF_NEAREST		4	// Use nearest filtering (as opposed to linear filtering)
#define IF_CLAMPS		8	// Clamp to edge (S)
#define IF_CLAMPT		16	// Clamp to edge (T)


abstract_class IEditorTexture
{
	public:

		virtual ~IEditorTexture(void)
		{
		}

		virtual int GetWidth( void ) const = 0;
		virtual int GetHeight( void ) const = 0;

		virtual float GetDecalScale( void ) const = 0;

		//
		// dvs: Try to remove as many of these as possible:
		//
		virtual const char *GetName( void ) const = 0;
		virtual int GetShortName( char *szShortName ) const = 0;
		virtual int GetKeywords( char *szKeywords ) const = 0;
		virtual void Draw(CDC *pDC, RECT &rect, int iFontHeight, int iIconHeight, DWORD dwFlags = (drawCaption|drawIcons)) = 0;
		virtual TEXTUREFORMAT GetTextureFormat( void ) const = 0;
		virtual int GetSurfaceAttributes( void ) const = 0;
		virtual int GetSurfaceContents(void ) const = 0;
		virtual int GetSurfaceValue( void ) const = 0;
		virtual imageflags_t GetImageFlags( void ) const = 0;
		virtual CPalette *GetPalette( void ) const = 0;
		virtual bool HasData( void ) const = 0;
		virtual bool HasPalette( void ) const = 0;
		virtual bool Load( void ) = 0; // ensure that texture is loaded. could this be done internally?
		virtual bool IsLoaded( void ) const = 0;
		virtual const char *GetFileName( void ) const = 0;

		//-----------------------------------------------------------------------------
		// Purpose: 
		// Input  : pData - 
		// Output : 
		//-----------------------------------------------------------------------------
		virtual byte *GetImageDataRGBA( int &nWidth, int &nHeight ) = 0;

		//-----------------------------------------------------------------------------
		// Purpose: Returns true if this texture wants 1-bit alpha testing
		//-----------------------------------------------------------------------------
		virtual bool IsAlphaTested( void ) const = 0;

		//-----------------------------------------------------------------------------
		// Purpose: Returns true if this texture wants 8-bit translucency
		//-----------------------------------------------------------------------------
		virtual bool IsTranslucent( void ) const = 0;

		//-----------------------------------------------------------------------------
		// Purpose: Returns whether this texture is a dummy texture or not. Dummy textures
		//			serve as placeholders for textures that were found in the map, but
		//			not in the WAD (or the materials tree). The dummy texture enables us
		//			to bind the texture, find it by name, etc.
		//-----------------------------------------------------------------------------
		virtual bool IsDummy( void ) const = 0; // dvs: perhaps not the best name?

		//-----------------------------------------------------------------------------
		// Purpose: Returns the unique texture ID for this texture object. The texture ID
		//			identifies the texture object across all renderers, and is assigned
		//			by the first renderer that actually binds the texture thru BindTexture.
		//
		//			Only the renderer ever needs to call SetTextureID.
		//-----------------------------------------------------------------------------
		virtual int GetTextureID( void ) const = 0;

		//-----------------------------------------------------------------------------
		// Purpose: Sets the unique texture ID for this texture object. The texture ID
		//			identifies the texture object across all renderers, and is assigned
		//			by the first renderer that actually binds the texture thru BindTexture.
		//
		//			Only the renderer should ever call SetTextureID!
		//-----------------------------------------------------------------------------
		virtual void SetTextureID( int nTextureID ) = 0;

		inline bool ShouldRenderLast( void ) const
		{
			return IsTranslucent();
		}
};


typedef CTypedPtrList<CPtrList, IEditorTexture *> ITextureList;


#endif // IEDITORTEXTURE_H
