// Implementation of the IEditorTexture interface for JaffaQuake materials

#include "stdafx.h"

#include <vector>
#include "stringtools.h"
#include "globalfunctions.h"
#include "gamedata.h"
#include "texturesystem.h"

#define STB_IMAGE_IMPLEMENTATION
#define STBI_NO_HDR
#define STBI_NO_LINEAR
#define STBI_ONLY_PNG
#include "stb_image.h"

#include "jaffamaterial.h"

#include "wc_postinclude.h"

#define MATERIAL_DIR "materials"


struct Texture_t
{
	char	szName[MAX_PATH];
	byte	*pImg;				// The RGBA data, this sucks
};

static std::vector<Texture_t> s_textures;

bool CJaffaMaterial::Initialize()
{
	assert( s_textures.size() == 0 );
	return true;
}

void CJaffaMaterial::Shutdown()
{
	for ( const auto &texture : s_textures )
	{
		free( texture.pImg );
	}

	s_textures.clear();
}

static bool LoadMaterialsInDirectory( const char *pDirectoryName, strlen_t nDirectoryNameLen,
	IMaterialEnumerator *pEnum, intptr_t nContext, uint32 nFlags )
{
	char szWildCard[MAX_PATH];
	V_sprintf_s( szWildCard, "%s/%s/*.mat", g_pGameConfig->m_szModDir, pDirectoryName );

	WIN32_FIND_DATA findData;
	HANDLE hFind;

	hFind = FindFirstFile( szWildCard, &findData );
	if ( hFind == INVALID_HANDLE_VALUE )
		return true;

	while ( true )
	{
		if ( !( findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) )
		{
			char szFullName[MAX_PATH];
			// Weird logic by me, skip past materials, and then + 1 to skip directory separator
			V_sprintf_s( szFullName, "%s/%s", pDirectoryName + sizeof( MATERIAL_DIR ) - 1, findData.cFileName );
			if ( !pEnum->EnumMaterial( szFullName, nContext ) )
			{
				FindClose( hFind );
				return false;
			}
		}

		if ( !FindNextFile( hFind, &findData ) )
			break;
	}

	FindClose( hFind );
	return true;
}

//-----------------------------------------------------------------------------
// Discovers all MAT files lying under a particular directory
// It only finds their names so we can generate shell materials for them
// that we can load up at a later time
// Slart: Can we run out of stack in here?
//-----------------------------------------------------------------------------
static bool InitDirectoryRecursive( const char *pDirectoryName, strlen_t nDirectoryNameLen,
	IMaterialEnumerator *pEnum, intptr_t nContext, uint32 nFlags )
{
	assert( g_pGameConfig->m_szModDir && g_pGameConfig->m_szModDir[0] );

	if ( !LoadMaterialsInDirectory( pDirectoryName, nDirectoryNameLen, pEnum, nContext, nFlags ) )
		return false;

	char szWildCard[MAX_PATH];
	V_sprintf_s( szWildCard, "%s/%s/*.*", g_pGameConfig->m_szModDir, pDirectoryName );

	WIN32_FIND_DATA findData;
	HANDLE hFind;

	hFind = FindFirstFile( szWildCard, &findData );
	if ( hFind == INVALID_HANDLE_VALUE )
		return false;

	while ( true )
	{
		if ( ( findData.cFileName[0] != '.' ) || ( findData.cFileName[1] != '.' && findData.cFileName[1] != '\0' ) )
		{
			if ( findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
			{
				char szFullPath[260];
				V_sprintf_s( szFullPath, "%s/%s", pDirectoryName, findData.cFileName );
				if ( !InitDirectoryRecursive( szFullPath, V_strlen( szFullPath ), pEnum, nContext, nFlags ) )
					return false;
			}
		}

		if ( !FindNextFile( hFind, &findData ) )
			break;
	}

	FindClose( hFind );
	return true;
}

void CJaffaMaterial::EnumerateMaterials( IMaterialEnumerator *pEnum, intptr_t nContext, uint32 nFlags /*= INCLUDE_ALL_MATERIALS*/ )
{
	InitDirectoryRecursive( MATERIAL_DIR, sizeof( MATERIAL_DIR ) - 1, pEnum, nContext, nFlags );
}

//-----------------------------------------------------------------------------
// Purpose: Factory. Creates a material by name.
// Input  : pszMaterialName - Name of material, ie "brick/brickfloor01".
// Output : Returns a pointer to the new material object, NULL if the given
//			material did not exist.
//-----------------------------------------------------------------------------
CJaffaMaterial *CJaffaMaterial::CreateMaterial( const char *pszMaterialName, bool bLoadImmediately, bool *pFound /*= nullptr*/ )
{
	Assert( pszMaterialName && pszMaterialName[0] );

	CJaffaMaterial *pMaterial = new CJaffaMaterial;
	Assert( pMaterial );

	if ( pszMaterialName[0] == '/' )
		++pszMaterialName;	// If we start with '/' skip past it

	// Store off the material name so we can load it later if we need to
	V_sprintf_s( pMaterial->m_szFileName, "%s/" MATERIAL_DIR "/%s", g_pGameConfig->m_szModDir, pszMaterialName );
	// Strip off materials and .mat
	// Filename will always start with "materials" and end with ".mat" so
	size_t end = ( strrchr( pszMaterialName, '.' ) - pszMaterialName );
	memcpy( pMaterial->m_szName, pszMaterialName, end );

	//
	// Find the material by name and load it.
	//
	if ( bLoadImmediately )
	{
		bool bFound = pMaterial->LoadMaterial();

		// Returns if the material was found or not
		if ( pFound )
			*pFound = bFound;
	}

	return pMaterial;
}

// JQ2 CODE

#define	MAX_TOKEN_CHARS		128		// max length of an individual token

//-----------------------------------------------------------------------------
// Parse a token out of a string (version 2, no global variables)
//-----------------------------------------------------------------------------
static void COM_Parse2( char **data_p, char **token_p, int tokenlen )
{
	int		c;
	int		len;
	char	*data;
	char	*token;

	data = *data_p;
	token = *token_p;
	len = 0;

	if ( !data )
	{
		*data_p = NULL;
		token[0] = '\0';
		return;
	}

// skip whitespace
skipwhite:
	while ( ( c = *data ) <= ' ' )
	{
		if ( c == 0 )
		{
			*data_p = NULL;
			token[0] = '\0';
			return;
		}
		data++;
	}

// skip // comments
	if ( c == '/' && data[1] == '/' )
	{
		while ( *data && *data != '\n' )
			data++;
		goto skipwhite;
	}

// handle quoted strings specially
	if ( c == '\"' )
	{
		data++;
		while ( 1 )
		{
			c = *data++;
			if ( c == '\"' || !c )
			{
				token[len] = 0;
				*data_p = data;
				return;
			}
			if ( len < tokenlen )
			{
				token[len] = c;
				len++;
			}
		}
	}

// parse a regular word
	do
	{
		if ( len < tokenlen )
		{
			token[len] = c;
			len++;
		}
		data++;
		c = *data;
	} while ( c > 32 );

	if ( len == tokenlen )
	{
	//	Com_Printf ("Token exceeded %i chars, discarded.\n", MAX_TOKEN_CHARS);
		len = 0;
	}
	token[len] = 0;

	*data_p = data;
}

// END JQ2 CODE

bool CJaffaMaterial::ParseMaterial( char *data )
{
	char tokenhack[MAX_TOKEN_CHARS];
	char *token = tokenhack;

	COM_Parse2( &data, &token, sizeof( tokenhack ) );
	if ( token[0] != '{' )
	{
		// Malformed
		Msg( mwError, "Malformed material %s\n", m_szFileName );
		return false;
	}

	COM_Parse2( &data, &token, sizeof( tokenhack ) );

	for ( ; token[0] != '}'; COM_Parse2( &data, &token, sizeof( tokenhack ) ) )
	{
		if ( V_strcmp( token, "$basetexture" ) == 0 )
		{
			COM_Parse2( &data, &token, sizeof( tokenhack ) );
			V_strcpy_s( m_szBaseTexture, token );
			continue;
		}
		if ( V_strcmp( token, "$alphatest" ) == 0 )
		{
			COM_Parse2( &data, &token, sizeof( tokenhack ) );
			m_bAlphaTest = (bool)atoi( token );
			continue;
		}
		if ( V_strcmp( token, "$nextframe" ) == 0 )
		{
#if 0
			COM_Parse2( &data, &token, sizeof( tokenhack ) );
			material->nextframe = GL_FindMaterial( token );
			if ( material->nextframe == mat_notexture ) // SLARTHACK: THIS IS REALLY BAD!!! REFCOUNT!!!
			{
				material->nextframe = nullptr;
			}
#endif
			continue;
		}
		if ( V_strcmp( token, "$nomips" ) == 0 )
		{
			COM_Parse2( &data, &token, sizeof( tokenhack ) );
			if ( atoi( token ) )
			{
				m_flags |= IF_NOMIPS;
			}
			continue;
		}
		if ( V_strcmp( token, "$noaniso" ) == 0 )
		{
			COM_Parse2( &data, &token, sizeof( tokenhack ) );
			if ( atoi( token ) )
			{
				m_flags |= IF_NOANISO;
			}
			continue;
		}
		if ( V_strcmp( token, "$nearestfilter" ) == 0 )
		{
			COM_Parse2( &data, &token, sizeof( tokenhack ) );
			if ( atoi( token ) )
			{
				m_flags |= IF_NEAREST;
			}
			continue;
		}
		if ( V_strcmp( token, "$clamps" ) == 0 )
		{
			COM_Parse2( &data, &token, sizeof( tokenhack ) );
			if ( atoi( token ) )
			{
				m_flags |= IF_CLAMPS;
			}
			continue;
		}
		if ( V_strcmp( token, "$clampt" ) == 0 )
		{
			COM_Parse2( &data, &token, sizeof( tokenhack ) );
			if ( atoi( token ) )
			{
				m_flags |= IF_CLAMPT;
			}
			continue;
		}
	}

	return true;
}

bool CJaffaMaterial::LoadMaterial()
{
	if ( m_bLoaded )
		return true;

	m_bLoaded = true;

	FILE *pFile = fopen( m_szFileName, "rb" );
	if ( !pFile )
	{
		Msg( mwError, "Load material failed: %s", m_szName );
		return false;
	}

	fseek( pFile, 0, SEEK_END );
	int nLength = ftell( pFile );
	assert( nLength > 2 );
	fseek( pFile, 0, SEEK_SET );
	char *pData = (char *)malloc( nLength );
	assert( pData );

	if ( fread( pData, 1, nLength, pFile ) != nLength )
	{
		free( pData );
		fclose( pFile );
		Msg( mwError, "Load material failed: %s", m_szName );
		return false;
	}

	fclose( pFile );

	if ( !ParseMaterial( pData ) )
	{
		free( pData );
		return false;
	}

	free( pData );

	// Look for it
	for ( size_t i = 0; i < s_textures.size(); ++i )
	{
		Texture_t &texture = s_textures[i];

		if ( V_strcmp( texture.szName, m_szBaseTexture ) == 0 )
		{
			m_nTextureIndex = (int)i;
			return true;
		}
	}

	char szFullPath[MAX_PATH];
	V_sprintf_s( szFullPath, "%s/%s", g_pGameConfig->m_szModDir, m_szBaseTexture );

	byte *pImg = stbi_load( szFullPath, &m_nWidth, &m_nHeight, nullptr, 4 );
	if ( !pImg )
	{
		Msg( mwError, "Load texture failed: %s", m_szBaseTexture );
		return false;
	}

	Texture_t &texture = s_textures.emplace_back();

	V_strcpy_s( texture.szName, m_szBaseTexture );
	texture.pImg = pImg;

	m_nTextureIndex = (int)(s_textures.size() - 1);

	return true;
}

CJaffaMaterial::CJaffaMaterial()
{
}

CJaffaMaterial::~CJaffaMaterial()
{
}

int CJaffaMaterial::GetShortName( char *szShortName ) const
{
	if ( szShortName )
	{
		strcpy( szShortName, m_szName );
	}

	return (int)strlen( m_szName );
}

int CJaffaMaterial::GetKeywords( char *szKeywords ) const
{
	if ( szKeywords )
	{
		strcpy( szKeywords, m_szKeywords );
	}

	return (int)strlen( m_szKeywords );
}

void CJaffaMaterial::DrawBitmap( CDC *pDC, RECT &srcRect, RECT &dstRect )
{
	struct
	{
		BITMAPINFOHEADER bmih;
		DWORD bitFields[3];
	} bmi;

	bmi.bitFields[0] = 0x0000FF;
	bmi.bitFields[1] = 0x00FF00;
	bmi.bitFields[2] = 0xFF0000;

	int srcWidth = srcRect.right - srcRect.left;
	int srcHeight = srcRect.bottom - srcRect.top;

	BITMAPINFOHEADER &bmih = bmi.bmih;
	memset( &bmih, 0, sizeof( bmih ) );
	bmih.biSize = sizeof( bmih );
	bmih.biWidth = srcWidth;
	bmih.biHeight = -srcHeight;
	bmih.biCompression = BI_BITFIELDS;

	bmih.biBitCount = 32;
	bmih.biPlanes = 1;

	int dest_width = dstRect.right - dstRect.left;
	int dest_height = dstRect.bottom - dstRect.top;

	// ** bits **
	SetStretchBltMode( pDC->m_hDC, HALFTONE );
	if ( StretchDIBits( pDC->m_hDC, dstRect.left, dstRect.top, dest_width, dest_height,
		srcRect.left, -srcRect.top, srcWidth, srcHeight, s_textures[m_nTextureIndex].pImg, (BITMAPINFO *)&bmi, DIB_RGB_COLORS, SRCCOPY ) == GDI_ERROR )
	{
		Msg( mwError, "CJaffaMaterial::Draw(): StretchDIBits failed." );
	}
}

void CJaffaMaterial::Draw( CDC *pDC, RECT &rect, int iFontHeight, int iIconHeight, DWORD dwFlags /*= ( drawCaption | drawIcons )*/ )
{
	if ( !HasData() || m_nWidth <= 0 || m_nHeight <= 0 )
	{
		// draw "no data"
		CFont *pOldFont = (CFont *)pDC->SelectStockObject( ANSI_VAR_FONT );
		COLORREF cr = pDC->SetTextColor( RGB( 0xff, 0xff, 0xff ) );
		COLORREF cr2 = pDC->SetBkColor( RGB( 0, 0, 0 ) );

		// draw black rect first
		pDC->FillRect( &rect, CBrush::FromHandle( HBRUSH( GetStockObject( BLACK_BRUSH ) ) ) );

		// then text
		pDC->TextOut( rect.left + 2, rect.top + 2, "No Image", 8 );
		pDC->SelectObject( pOldFont );
		pDC->SetTextColor( cr );
		pDC->SetBkColor( cr2 );
		return;
	}

	// Draw the material image
	RECT srcRect, dstRect;
	srcRect.left = 0;
	srcRect.top = 0;
	srcRect.right = m_nWidth;
	srcRect.bottom = m_nHeight;
	dstRect = rect;

	if ( dwFlags & drawCaption )
	{
		dstRect.bottom -= iFontHeight + 4;
	}
	if ( dwFlags & drawIcons )
	{
		dstRect.bottom -= iIconHeight;
	}

	if ( !( dwFlags & drawResizeAlways ) )
	{
		if ( m_nWidth < dstRect.right - dstRect.left )
		{
			dstRect.right = dstRect.left + m_nWidth;
		}

		if ( m_nHeight < dstRect.bottom - dstRect.top )
		{
			dstRect.bottom = dstRect.top + m_nHeight;
		}
	}
	DrawBitmap( pDC, srcRect, dstRect );

#if 0
	// Draw the icons
	if ( dwFlags & drawIcons )
	{
		dstRect = rect;
		if ( dwFlags & drawCaption )
		{
			dstRect.bottom -= iFontHeight + 5;
		}
		dstRect.top = dstRect.bottom - iIconHeight;
		DrawBrowserIcons( pDC, dstRect, ( dwFlags & drawErrors ) != 0 );
	}
#endif

	// ** caption **
	if ( dwFlags & drawCaption )
	{
		// draw background for name
		CBrush brCaption( RGB( 0, 0, 255 ) );
		CRect rcCaption( rect );

		rcCaption.top = rcCaption.bottom - ( iFontHeight + 5 );
		pDC->FillRect( rcCaption, &brCaption );

		// draw name
		char szShortName[MAX_PATH];
		int iLen = GetShortName( szShortName );
		pDC->TextOut( rect.left, rect.bottom - ( iFontHeight + 4 ), szShortName, iLen );

#if 0
		// draw usage count
		if ( dwFlags & drawUsageCount )
		{
			CString str;
			str.Format( "%d", DrawTexData.nUsageCount );
			CSize size = pDC->GetTextExtent( str );
			pDC->TextOut( rect.right - size.cx, rect.bottom - ( iFontHeight + 4 ), str );
		}
#endif
	}
}

bool CJaffaMaterial::Load()
{
	return LoadMaterial();		// I don't think this result is ever checked
}

byte *CJaffaMaterial::GetImageDataRGBA( int &nWidth, int &nHeight )
{
	assert( m_bLoaded );
	assert( m_nWidth > 0 && m_nHeight > 0 );

	nWidth = m_nWidth;
	nHeight = m_nHeight;

	return s_textures[m_nTextureIndex].pImg;
}
