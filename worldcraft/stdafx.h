
#pragma once

// Include standard headers first
// We do this because we want to encourage inline functions
// in the MFC headers to use C++ overloads

#include <cstdlib>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <cassert>

// C++ standard headers
// These can be after MFC, it doesn't really matter

#include <fstream>
#include <vector>
#include <algorithm>
#include <utility>

// Now Windows / MFC

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <sdkddkver.h>

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>			// MFC core and standard components
#include <afxext.h>			// MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif

// Fixup, use MFC assert

#if 0

#ifdef assert
#error assert should not be defined by now!
#endif

#define assert ASSERT

#else

#undef ASSERT
#define ASSERT assert

#endif

// OpenGL

#ifndef SOURCE_MATERIALSYSTEM
#include "GL/glew.h"
#include <GL/GLU.h>
#endif

// Everything else

#include "branding.h"
