// A central location between the 2D and 3D GL renderers

#include "stdafx.h"

#include "mathlib.h"
#include "globalfunctions.h"

#include "GL/wglew.h"

#include "render.h"

//-----------------------------------------------------------------------------

HGLRC	g_CurrentHGLRC;		// A pointer to the HGLRC that is current right now, avoids api calls

// Set up GL for a given window, dc and glrc
void CreateGLContextForWindow( HWND &hWnd, HDC &hDC, HGLRC &hGLRC )
{
	BOOL	result;
	PIXELFORMATDESCRIPTOR pfd;
	int		pixelformat;

	assert( WGLEW_ARB_pixel_format );

	const int attriblist[]
	{
		WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
		WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
		WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
		WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
		WGL_COLOR_BITS_ARB, 32,
		WGL_DEPTH_BITS_ARB, 24,
		WGL_STENCIL_BITS_ARB, 8,
		0
	};

	UINT numformats;

	result = wglChoosePixelFormatARB( hDC, attriblist, NULL, 1, &pixelformat, &numformats );
	assert( result );

	result = DescribePixelFormat( hDC, pixelformat, sizeof( pfd ), &pfd );
	assert( result != 0 );

	result = SetPixelFormat( hDC, pixelformat, &pfd );
	assert( result );

	// Intel HD Graphics 3000 chips (my laptop) don't support this function
	if ( WGLEW_ARB_create_context )
	{
		const int contextAttribs[]
		{
			WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
			WGL_CONTEXT_MINOR_VERSION_ARB, 3,
			WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,
			0, // End
		};

		hGLRC = wglCreateContextAttribsARB( hDC, NULL, contextAttribs );
		assert( hGLRC );
	}
	else
	{
		hGLRC = wglCreateContext( hDC );
		assert( hGLRC );
	}

	result = wglMakeCurrent( hDC, hGLRC );
	assert( result );

	g_CurrentHGLRC = hGLRC;

	if ( WGL_EXT_swap_control )
	{
		wglSwapIntervalEXT( 1 );
	}
}

//-----------------------------------------------------------------------------
// Dummy window
//-----------------------------------------------------------------------------

struct DummyVars
{
	HWND	hWnd;
	HGLRC	hGLRC;
};

static constexpr auto DummyClassname = _T( "GRUG" );

// Create a dummy window
//
static void GLimp_CreateDummyWindow( DummyVars &dvars, HINSTANCE hInstance )
{
	BOOL result;

	// Create our dummy window class
	const WNDCLASSEX wcex
	{
		sizeof( wcex ),				// Size
		CS_OWNDC,					// Style
		DefWindowProc,				// Wndproc
		0,							// ClsExtra
		0,							// WndExtra
		hInstance,					// hInstance
		NULL,						// Icon
		NULL,						// Cursor
		(HBRUSH)( COLOR_WINDOW + 1 ),	// Background colour
		NULL,						// Menu
		DummyClassname,				// Class name
		NULL						// Small icon
	};
	RegisterClassEx( &wcex );

	dvars.hWnd = CreateWindowEx(
		0,
		DummyClassname,
		nullptr,
		0,
		0, 0, 800, 600,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	assert( dvars.hWnd );

	// Create our dummy PFD
	const PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof( pfd ),
		1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,    // Flags
		PFD_TYPE_RGBA,		// The kind of framebuffer. RGBA or palette
		32,					// Colordepth of the framebuffer
		0, 0, 0, 0, 0, 0,
		0,
		0,
		0,
		0, 0, 0, 0,
		24,					// Number of bits for the depthbuffer
		8,					// Number of bits for the stencilbuffer
		0,					// Number of Aux buffers in the framebuffer
		PFD_MAIN_PLANE,
		0,
		0, 0, 0
	};

	// Get this window's DC, we don't need to release it afterwards due to
	// CS_OWNDC
	HDC hDC = GetDC( dvars.hWnd );
	assert( hDC );

	int pixelformat = ChoosePixelFormat( hDC, &pfd );
	assert( pixelformat != 0 );

	result = SetPixelFormat( hDC, pixelformat, &pfd );
	assert( result );

	dvars.hGLRC = wglCreateContext( hDC );
	assert( dvars.hGLRC );

	result = wglMakeCurrent( hDC, dvars.hGLRC );
	assert( result );
}

// Destroy a dummy window
//
static void GLimp_DestroyDummyWindow( DummyVars &dvars, HINSTANCE hInstance )
{
	BOOL result;

	if ( dvars.hGLRC )
	{
		result = wglDeleteContext( dvars.hGLRC );
		assert( result );
	}
	if ( dvars.hWnd )
	{
		result = DestroyWindow( dvars.hWnd );
		assert( result );
	}
	result = UnregisterClass( DummyClassname, hInstance );
	assert( result );
}

//-----------------------------------------------------------------------------

bool InitialiseOpenGL()
{
	HINSTANCE hInstance = GetModuleHandle( nullptr );

	// Init the dummy window
	DummyVars dvars{};
	GLimp_CreateDummyWindow( dvars, hInstance );

	// initialize our OpenGL dynamic bindings
	glewExperimental = GL_TRUE;
	if ( glewInit() != GLEW_OK && wglewInit() != GLEW_OK )
	{
		GLimp_DestroyDummyWindow( dvars, hInstance );
		Msg( mwError, __FUNCTION__ " - Could not load OpenGL bindings\n" );
		return false;
	}

	GLimp_DestroyDummyWindow( dvars, hInstance );

	return true;
}
