//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: Manages the set of application configuration options.
//
// $NoKeywords: $
//=============================================================================

#include "stdafx.h"
#include "Options.h"
#include "Worldcraft.h"
#include "MainFrm.h"
#include "mapdoc.h"
#include "GlobalFunctions.h"
#include "CustomMessages.h"
#include "OptionProperties.h"

#include "wc_postinclude.h"


static const char *pszGeneral = "General";
static const char *pszView2D = "2D Views";
static const char *pszView3D = "3D Views";
static const char *g_szColors = "Custom2DColors";

const int iThisVersion = 2;


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
COptionsConfigs::COptionsConfigs(void)
{
	nConfigs = 0;
}


//-----------------------------------------------------------------------------
// Purpose: 
// Output : 
//-----------------------------------------------------------------------------
COptionsConfigs::~COptionsConfigs(void)
{
	for (int i = 0; i < Options.configs.nConfigs; i++)
	{
		CGameConfig *pConfig = Options.configs.Configs[i];
		if (!pConfig)
			continue;

		delete pConfig;
	}
}


//-----------------------------------------------------------------------------
// Purpose: 
// Output : 
//-----------------------------------------------------------------------------
CGameConfig *COptionsConfigs::AddConfig(void)
{
	CGameConfig *pConfig = new CGameConfig;
	Configs.SetAtGrow(nConfigs++, pConfig);

	return pConfig;
}


//-----------------------------------------------------------------------------
// Purpose: 
// Input  : dwID - 
//			piIndex - 
// Output : 
//-----------------------------------------------------------------------------
CGameConfig *COptionsConfigs::FindConfig(DWORD dwID, int *piIndex)
{
	for(int i = 0; i < nConfigs; i++)
	{
		if(Configs[i]->dwID == dwID)
		{
			if(piIndex)
				piIndex[0] = i;
			return Configs[i];
		}
	}

	return NULL;
}


//-----------------------------------------------------------------------------
// Purpose: Loads all the game configs from disk.
// Output : Returns the number of game configurations successfully loaded.
//-----------------------------------------------------------------------------
int COptionsConfigs::LoadGameConfigs(const char *pszFileName)
{
	//
	// Read game configs - this is from an external file so we can distribute it
	// with Worldcraft as a set of defaults.
	//
	// Newer versions of Worldcraft load configs from the "GameCfg.ini" file.
	//
	int nConfigsRead = 0;
	int nNumConfigs = GetPrivateProfileInt("Configs", "NumConfigs", 0, pszFileName);
	for (int nConfig = 0; nConfig < nNumConfigs; nConfig++)
	{
		//
		// Each came configuration is stored in a different section, named "GameConfig0..GameConfigN".
		// If the "Name" key exists in this section, try to load the configuration from this section.
		//
		char szSectionName[MAX_PATH];
		char szName[MAX_PATH];
		V_sprintf(szSectionName, "GameConfig%d", nConfig);

		int nCount = GetPrivateProfileString(szSectionName, "Name", "", szName, sizeof(szName), pszFileName);
		if (nCount > 0)
		{
			CGameConfig *pConfig = AddConfig();
			if (pConfig != NULL)
			{	
				if (pConfig->Load(pszFileName, szSectionName))
				{
					nConfigsRead++;
				}
			}
		}
	}

	return(nConfigsRead);
}


//-----------------------------------------------------------------------------
// Purpose: Saves all the cgame configurations to disk.
// Input  : *pszFileName - 
//-----------------------------------------------------------------------------
void COptionsConfigs::SaveGameConfigs(const char *pszFileName)
{
	char szKey[MAX_PATH];
	WritePrivateProfileString("Configs", "NumConfigs", itoa(nConfigs, szKey, 10), pszFileName);

	for (int i = 0; i < nConfigs; i++)
	{
		CGameConfig *pConfig = Configs.GetAt(i);
		if (pConfig != NULL)
		{
			char szSectionName[MAX_PATH];
			sprintf(szSectionName, "GameConfig%d", i);
			pConfig->Save(pszFileName, szSectionName);
		}
	}
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
COptions::COptions(void)
{
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void COptions::Init(void)
{
	SetDefaults();
	Read();

	//
	// Notify appropriate windows of new settings.
	// dvs: is all this necessary?
	//
	CMainFrame *pMainWnd = GetMainWnd();
	if (pMainWnd != NULL)
	{
		pMainWnd->SetBrightness(textures.fBrightness);

		pMainWnd->UpdateAllDocViews(MAPVIEW_UPDATE_2D | MAPVIEW_OPTIONS_CHANGED);
		pMainWnd->UpdateAllDocViews(MAPVIEW_UPDATE_3D | MAPVIEW_OPTIONS_CHANGED);

		pMainWnd->GlobalNotify(WM_GAME_CHANGED);
	}
}


//-----------------------------------------------------------------------------
// Purpose: Enables or disables texture locking.
// Input  : b - TRUE to enable texture locking, FALSE to disable.
// Output : Returns the previous value of the texture locking flag.
//-----------------------------------------------------------------------------
BOOL COptions::SetLockingTextures(BOOL b)
{
	BOOL bOld = general.bLockingTextures;
	general.bLockingTextures = b;
	return(bOld);
}


//-----------------------------------------------------------------------------
// Purpose: Returns TRUE if texture locking is enabled, FALSE if not.
//-----------------------------------------------------------------------------
BOOL COptions::IsLockingTextures(void)
{
	return(general.bLockingTextures);
}


//-----------------------------------------------------------------------------
// Purpose: Returns whether new faces should be world aligned or face aligned.
//-----------------------------------------------------------------------------
TextureAlignment_t COptions::GetTextureAlignment(void)
{
	return(general.eTextureAlignment);
}


//-----------------------------------------------------------------------------
// Purpose: Sets whether new faces should be world aligned or face aligned.
// Input  : eTextureAlignment - TEXTURE_ALIGN_WORLD or TEXTURE_ALIGN_FACE.
// Output : Returns the old setting for texture alignment.
//-----------------------------------------------------------------------------
TextureAlignment_t COptions::SetTextureAlignment(TextureAlignment_t eTextureAlignment)
{
	TextureAlignment_t eOld = general.eTextureAlignment;
	general.eTextureAlignment = eTextureAlignment;
	return(eOld);
}


//-----------------------------------------------------------------------------
// Purpose: Returns whether helpers should be hidden or shown.
//-----------------------------------------------------------------------------
bool COptions::GetShowHelpers(void)
{
	return (general.bShowHelpers == TRUE);
}


//-----------------------------------------------------------------------------
// Purpose: Sets whether helpers should be hidden or shown.
//-----------------------------------------------------------------------------
void COptions::SetShowHelpers(bool bShow)
{
	general.bShowHelpers = bShow ? TRUE : FALSE;
}


//-----------------------------------------------------------------------------
// Purpose: Loads the application configuration settings.
// Output : Returns TRUE on success, FALSE on failure.
//-----------------------------------------------------------------------------
BOOL COptions::Read(void)
{
	CWorldcraft *pApp = APP();

	if (!pApp->GetProfileInt("Configured", "Configured", 0))
	{
		return FALSE;
	}

	DWORD dwTime = pApp->GetProfileInt("Configured", "Installed", time(NULL));
	CTimeSpan ts(time(NULL) - dwTime);
	uDaysSinceInstalled = ts.GetDays();

	int i, iSize;
	CString str;

	// read texture info - it's stored in the general section from
	//  an old version, but this doesn't matter much.
	iSize = pApp->GetProfileInt(pszGeneral, "TextureFileCount", 0);
	if(iSize)
	{
		// make sure default is removed (Slart: We don't do this anymore but it can't hurt)
		textures.nTextureFiles = 0;
		textures.TextureFiles.RemoveAll();
		// read texture file names
		for(i = 0; i < iSize; i++)
		{
			str.Format("TextureFile%d", i);
			str = pApp->GetProfileString(pszGeneral, str);
			if(GetFileAttributes(str) == INVALID_FILE_ATTRIBUTES)
			{
				// can't find
				continue;
			}
			textures.TextureFiles.Add(str);
			textures.nTextureFiles++;
		}
	}

	textures.fBrightness = float(pApp->GetProfileInt(pszGeneral, "Brightness", 10)) / 10.0;

	// load general info
	general.iUndoLevels = pApp->GetProfileInt(pszGeneral, "Undo Levels", 50);
	general.bLockingTextures = pApp->GetProfileInt(pszGeneral, "Locking Textures", TRUE);
	general.eTextureAlignment = (TextureAlignment_t)pApp->GetProfileInt(pszGeneral, "Texture Alignment", TEXTURE_ALIGN_WORLD);
	general.bLoadwinpos = pApp->GetProfileInt(pszGeneral, "Load Default Positions", TRUE);
	general.bIndependentwin = pApp->GetProfileInt(pszGeneral, "Independent Windows", FALSE);
	general.bGroupWhileIgnore = pApp->GetProfileInt(pszGeneral, "GroupWhileIgnore", FALSE);
	general.bStretchArches = pApp->GetProfileInt(pszGeneral, "StretchArches", TRUE);
	general.bShowHelpers = pApp->GetProfileInt(pszGeneral, "Show Helpers", TRUE);

	// read view2d
	view2d.bCrosshairs = pApp->GetProfileInt(pszView2D, "Crosshairs", FALSE);
	view2d.bGroupCarve = pApp->GetProfileInt(pszView2D, "GroupCarve", TRUE);
	view2d.bScrollbars = pApp->GetProfileInt(pszView2D, "Scrollbars", TRUE);
	view2d.bRotateConstrain = pApp->GetProfileInt(pszView2D, "RotateConstrain", FALSE);
	view2d.bDrawVertices = pApp->GetProfileInt(pszView2D, "Draw Vertices", TRUE);
	view2d.bWhiteOnBlack = pApp->GetProfileInt(pszView2D, "WhiteOnBlack", TRUE);
	view2d.bGridHigh1024 = pApp->GetProfileInt(pszView2D, "GridHigh1024", TRUE);
	view2d.bGridHigh10 = pApp->GetProfileInt(pszView2D, "GridHigh10", TRUE);
	view2d.bHideSmallGrid = pApp->GetProfileInt(pszView2D, "HideSmallGrid", TRUE);
	view2d.bNudge = pApp->GetProfileInt(pszView2D, "Nudge", FALSE);
	view2d.bOrientPrimitives = pApp->GetProfileInt(pszView2D, "OrientPrimitives", FALSE);
	view2d.bAutoSelect = pApp->GetProfileInt(pszView2D, "AutoSelect", FALSE);
	view2d.bSelectbyhandles = pApp->GetProfileInt(pszView2D, "SelectByHandles", FALSE);
	view2d.iGridIntensity = pApp->GetProfileInt(pszView2D, "GridIntensity", 30);
	view2d.iDefaultGrid = pApp->GetProfileInt(pszView2D, "Default Grid", 64);
	view2d.iGridHighSpec = pApp->GetProfileInt(pszView2D, "GridHighSpec", 8);
	view2d.bKeepclonegroup = pApp->GetProfileInt(pszView2D, "Keepclonegroup", TRUE);
	view2d.bGridHigh64 = pApp->GetProfileInt(pszView2D, "Gridhigh64", TRUE);
	view2d.bGridDots = pApp->GetProfileInt(pszView2D, "GridDots", FALSE);
	view2d.bCenteroncamera = pApp->GetProfileInt(pszView2D, "Centeroncamera", FALSE);
	view2d.bUsegroupcolors = pApp->GetProfileInt(pszView2D, "Usegroupcolors", TRUE);

	// read view3d
	view3d.bHardware = pApp->GetProfileInt(pszView3D, "Hardware", FALSE);
	view3d.bReverseY = pApp->GetProfileInt(pszView3D, "Reverse Y", TRUE);
	view3d.iBackPlane = pApp->GetProfileInt(pszView3D, "BackPlane", 5000);
	view3d.bUseMouseLook = pApp->GetProfileInt(pszView3D, "UseMouseLook", TRUE);
	view3d.nModelDistance = pApp->GetProfileInt(pszView3D, "ModelDistance", 400);
	view3d.bAnimateModels = pApp->GetProfileInt(pszView3D, "AnimateModels", TRUE);
	view3d.nForwardSpeedMax = pApp->GetProfileInt(pszView3D, "ForwardSpeedMax", 1000);
	view3d.nTimeToMaxSpeed = pApp->GetProfileInt(pszView3D, "TimeToMaxSpeed", 500);
	view3d.bFilterTextures = pApp->GetProfileInt(pszView3D, "FilterTextures", TRUE);
	view3d.bReverseSelection = pApp->GetProfileInt(pszView3D, "ReverseSelection", FALSE);

	ReadColorSettings();

	//
	// If we can't load any game configurations, pop up the options screen.
	//
	char szIniPath[MAX_PATH];
	pApp->GetDirectory(DIR_PROGRAM, szIniPath);
	strcat(szIniPath, "GameCfg.ini");
	
	configs.LoadGameConfigs(szIniPath);

	//
	// By default use the first config.
	//
	if (configs.nConfigs > 0)
	{
		g_pGameConfig = configs.Configs.GetAt(0);
	}

	return(TRUE);
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void COptions::ReadColorSettings(void)
{
	CWorldcraft *pApp = APP();

	colors.bUseCustom = (pApp->GetProfileInt(g_szColors, "UseCustom", 0) != 0);
	if (colors.bUseCustom)
	{
		colors.clrAxis = pApp->GetProfileColor(g_szColors, "Grid0", 0 , 100, 100);
		colors.bScaleAxisColor = (pApp->GetProfileInt(g_szColors, "ScaleGrid0", 0) != 0);
		colors.clrGrid = pApp->GetProfileColor(g_szColors, "Grid", 50 , 50, 50);
		colors.bScaleGridColor = (pApp->GetProfileInt(g_szColors, "ScaleGrid", 1) != 0);
		colors.clrGrid10 = pApp->GetProfileColor(g_szColors, "Grid10", 40 , 40, 40);
		colors.bScaleGrid10Color = (pApp->GetProfileInt(g_szColors, "ScaleGrid10", 1) != 0);
		colors.clrGrid1024 = pApp->GetProfileColor(g_szColors, "Grid1024", 40 , 40, 40);
		colors.bScaleGrid1024Color = (pApp->GetProfileInt(g_szColors, "ScaleGrid1024", 1) != 0);
		colors.clrGridDot = pApp->GetProfileColor(g_szColors, "GridDot", 128, 128, 128);
		colors.bScaleGridDotColor = (pApp->GetProfileInt(g_szColors, "ScaleGridDot", 1) != 0);

		colors.clrBrush = pApp->GetProfileColor(g_szColors, "LineColor", 0, 0, 0);
		colors.clrEntity = pApp->GetProfileColor(g_szColors, "Entity", 220, 30, 220);
		colors.clrVertex = pApp->GetProfileColor(g_szColors, "Vertex", 0, 0, 0);
		colors.clrBackground = pApp->GetProfileColor(g_szColors, "Background", 0, 0, 0);
		colors.clrToolHandle = pApp->GetProfileColor(g_szColors, "HandleColor", 0, 0, 0);
		colors.clrToolBlock = pApp->GetProfileColor(g_szColors, "BoxColor", 0, 0, 0);
		colors.clrToolSelection = pApp->GetProfileColor(g_szColors, "ToolSelect", 0, 0, 0);
		colors.clrToolMorph = pApp->GetProfileColor(g_szColors, "Morph", 255, 0, 0);
		colors.clrToolPath = pApp->GetProfileColor(g_szColors, "Path", 255, 0, 0);
		colors.clrSelection = pApp->GetProfileColor(g_szColors, "Selection", 220, 0, 0);
		colors.clrToolDrag = pApp->GetProfileColor(g_szColors, "ToolDrag", 255, 255, 0);
	}
	else
	{
		if (Options.view2d.bWhiteOnBlack)
		{
			// BLACK BACKGROUND
			colors.clrBackground = RGB(0, 0, 0);
			colors.clrGrid = RGB(255, 255, 255);
			colors.clrGridDot = RGB(255, 255, 255);
			colors.clrGrid1024 = RGB(100, 50, 5);
			colors.clrGrid10 = RGB(255, 255, 255);
			colors.clrAxis = RGB(0, 100, 100);
			colors.clrBrush = RGB(255, 255, 255);
			colors.clrVertex = RGB(255, 255, 255);

			colors.clrToolHandle = RGB(255, 255, 255);
			colors.clrToolBlock = RGB(255, 255, 255);
			colors.clrToolDrag = RGB(255, 255, 0);
		}
		else
		{
			// WHITE BACKGROUND
			colors.clrBackground = RGB(255, 255, 255);
			colors.clrGrid = RGB(50, 50, 50);
			colors.clrGridDot = RGB(40, 40, 40);
			colors.clrGrid1024 = RGB(200, 100, 10);
			colors.clrGrid10 = RGB(40, 40, 40);
			colors.clrAxis = RGB(0, 100, 100);
			colors.clrBrush = RGB(0, 0, 0);
			colors.clrVertex = RGB(0, 0, 0);

			colors.clrToolHandle = RGB(0, 0, 0);
			colors.clrToolBlock = RGB(0, 0, 0);
			colors.clrToolDrag = RGB(0, 0, 255);
		}

		colors.bScaleAxisColor = false;
		colors.bScaleGridColor = true;
		colors.bScaleGrid10Color = true;
		colors.bScaleGrid1024Color = false;
		colors.bScaleGridDotColor = true;

		colors.clrToolSelection = colors.clrSelection = RGB(255, 0, 0);
		colors.clrToolMorph = RGB(255, 0, 0);
		colors.clrToolPath = RGB(255, 0, 0);
		colors.clrEntity = RGB(220, 30, 220);
	}
}


//-----------------------------------------------------------------------------
// Purpose: 
// Input  : fOverwrite - 
//-----------------------------------------------------------------------------
void COptions::Write(BOOL fOverwrite)
{
	CWorldcraft *pApp = APP();

	pApp->WriteProfileInt("Configured", "Configured", iThisVersion);

	int i, iSize;
	CString str;

	// write texture info - remember, it's stored in general
	iSize = textures.nTextureFiles;
	pApp->WriteProfileInt(pszGeneral, "TextureFileCount", iSize);
	for(i = 0; i < iSize; i++)
	{
		str.Format("TextureFile%d", i);
		pApp->WriteProfileString(pszGeneral, str, textures.TextureFiles[i]);
	}
	pApp->WriteProfileInt(pszGeneral, "Brightness", int(textures.fBrightness * 10));

	// write general
	pApp->WriteProfileInt(pszGeneral, "Undo Levels", general.iUndoLevels);
	pApp->WriteProfileInt(pszGeneral, "Locking Textures", general.bLockingTextures);
	pApp->WriteProfileInt(pszGeneral, "Texture Alignment", general.eTextureAlignment);
	pApp->WriteProfileInt(pszGeneral, "Independent Windows", general.bIndependentwin);
	pApp->WriteProfileInt(pszGeneral, "Load Default Positions", general.bLoadwinpos);
	pApp->WriteProfileInt(pszGeneral, "GroupWhileIgnore", general.bGroupWhileIgnore);
	pApp->WriteProfileInt(pszGeneral, "StretchArches", general.bStretchArches);
	pApp->WriteProfileInt(pszGeneral, "Show Helpers", general.bShowHelpers);

	// write view2d
	pApp->WriteProfileInt(pszView2D, "Crosshairs", view2d.bCrosshairs);
	pApp->WriteProfileInt(pszView2D, "GroupCarve", view2d.bGroupCarve);
	pApp->WriteProfileInt(pszView2D, "Scrollbars", view2d.bScrollbars);
	pApp->WriteProfileInt(pszView2D, "RotateConstrain", view2d.bRotateConstrain);
	pApp->WriteProfileInt(pszView2D, "Draw Vertices", view2d.bDrawVertices);
	pApp->WriteProfileInt(pszView2D, "Default Grid", view2d.iDefaultGrid);
	pApp->WriteProfileInt(pszView2D, "WhiteOnBlack", view2d.bWhiteOnBlack);
	pApp->WriteProfileInt(pszView2D, "GridHigh1024", view2d.bGridHigh1024);
	pApp->WriteProfileInt(pszView2D, "GridHigh10", view2d.bGridHigh10);
	pApp->WriteProfileInt(pszView2D, "GridIntensity", view2d.iGridIntensity);
	pApp->WriteProfileInt(pszView2D, "HideSmallGrid", view2d.bHideSmallGrid);
	pApp->WriteProfileInt(pszView2D, "Nudge", view2d.bNudge);
	pApp->WriteProfileInt(pszView2D, "OrientPrimitives", view2d.bOrientPrimitives);
	pApp->WriteProfileInt(pszView2D, "AutoSelect", view2d.bAutoSelect);
	pApp->WriteProfileInt(pszView2D, "SelectByHandles", view2d.bSelectbyhandles);
	pApp->WriteProfileInt(pszView2D, "GridHighSpec", view2d.iGridHighSpec);
	pApp->WriteProfileInt(pszView2D, "KeepCloneGroup", view2d.bKeepclonegroup);
	pApp->WriteProfileInt(pszView2D, "Gridhigh64", view2d.bGridHigh64);
	pApp->WriteProfileInt(pszView2D, "GridDots", view2d.bGridDots);
	pApp->WriteProfileInt(pszView2D, "Centeroncamera", view2d.bCenteroncamera);
	pApp->WriteProfileInt(pszView2D, "Usegroupcolors", view2d.bUsegroupcolors);

	// write view3d
	pApp->WriteProfileInt(pszView3D, "Hardware", view3d.bHardware);
	pApp->WriteProfileInt(pszView3D, "Reverse Y", view3d.bReverseY);
	pApp->WriteProfileInt(pszView3D, "BackPlane", view3d.iBackPlane);
	pApp->WriteProfileInt(pszView3D, "UseMouseLook", view3d.bUseMouseLook);
	pApp->WriteProfileInt(pszView3D, "ModelDistance", view3d.nModelDistance);
	pApp->WriteProfileInt(pszView3D, "AnimateModels", view3d.bAnimateModels);
	pApp->WriteProfileInt(pszView3D, "ForwardSpeedMax", view3d.nForwardSpeedMax);
	pApp->WriteProfileInt(pszView3D, "TimeToMaxSpeed", view3d.nTimeToMaxSpeed);
	pApp->WriteProfileInt(pszView3D, "FilterTextures", view3d.bFilterTextures);
	pApp->WriteProfileInt(pszView3D, "ReverseSelection", view3d.bReverseSelection);

	//
	// We don't write custom color settings because there is no GUI for them yet.
	//

	//
	// Write game configs to the INI file. This is in an external file so we can distribute it
	// with worldcraft as a set of defaults.
	//
	char szIniPath[MAX_PATH];
	pApp->GetDirectory(DIR_PROGRAM, szIniPath);
	strcat(szIniPath, "GameCfg.ini");
	configs.SaveGameConfigs(szIniPath);
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void COptions::SetDefaults(void)
{
	CWorldcraft *pApp = APP();

	BOOL bWrite = FALSE;

	if (pApp->GetProfileInt("Configured", "Configured", 0) != iThisVersion)
	{
		bWrite = TRUE;
	}

	if (pApp->GetProfileInt("Configured", "Installed", 42151) == 42151)
	{
		pApp->WriteProfileInt("Configured", "Installed", time(NULL));
	}

	uDaysSinceInstalled = 0;

	// textures
	textures.nTextureFiles = 0;
	textures.fBrightness = 1.0;

	// general
	general.bIndependentwin = FALSE;
	general.bLoadwinpos = TRUE;
	general.iUndoLevels = 50;
	general.bGroupWhileIgnore = FALSE;
	general.bStretchArches = TRUE;
	general.bLockingTextures = TRUE;
	general.bShowHelpers = TRUE;

	// view2d
	view2d.bCrosshairs = FALSE;
	view2d.bGroupCarve = TRUE;
	view2d.bScrollbars = TRUE;
	view2d.bRotateConstrain = FALSE;
	view2d.bDrawVertices = TRUE;
	view2d.iDefaultGrid = 64;
	view2d.bWhiteOnBlack = TRUE;
	view2d.bGridHigh1024 = TRUE;
	view2d.bGridHigh10 = TRUE;
	view2d.iGridIntensity = 30;
	view2d.bHideSmallGrid = TRUE;
	view2d.bNudge = FALSE;
	view2d.bOrientPrimitives = FALSE;
	view2d.bAutoSelect = FALSE;
	view2d.bSelectbyhandles = FALSE;
	view2d.iGridHighSpec = 8;
	view2d.bKeepclonegroup = TRUE;
	view2d.bGridHigh64 = FALSE;
	view2d.bGridDots = FALSE;
	view2d.bCenteroncamera = FALSE;
	view2d.bUsegroupcolors = TRUE;

	// view3d
	view3d.bUseMouseLook = TRUE;
	view3d.bHardware = FALSE;
	view3d.bReverseY = FALSE;
	view3d.iBackPlane = 5000;
	view3d.nModelDistance = 400;
	view3d.bAnimateModels = TRUE;
	view3d.nForwardSpeedMax = 1000;
	view3d.nTimeToMaxSpeed = 500;
	view3d.bFilterTextures = TRUE;
	view3d.bReverseSelection = FALSE;

	if (bWrite)
	{
		Write(FALSE);
	}
}


//-----------------------------------------------------------------------------
// Purpose: This is called by the user interface itself when changes are made. 
//			tells the COptions object to notify the parts of the interface.
// Input  : dwOptionsChanged - Flags indicating which options changed.
//-----------------------------------------------------------------------------
void COptions::PerformChanges(DWORD dwOptionsChanged)
{
	CMainFrame *pMainWnd = GetMainWnd();

	if (dwOptionsChanged & secTextures)
	{
		if (pMainWnd != NULL)
		{
			pMainWnd->SetBrightness(textures.fBrightness);
		}
	}

	if (dwOptionsChanged & secView2D)
	{
		ReadColorSettings();

		if (pMainWnd != NULL)
		{
			pMainWnd->UpdateAllDocViews(MAPVIEW_UPDATE_2D | MAPVIEW_OPTIONS_CHANGED);
			pMainWnd->UpdateAllDocViews(MAPVIEW_UPDATE_3D | MAPVIEW_OPTIONS_CHANGED);
		}
	}

	if (dwOptionsChanged & secView3D)
	{
		if (pMainWnd != NULL)
		{
			pMainWnd->UpdateAllDocViews(MAPVIEW_UPDATE_3D | MAPVIEW_OPTIONS_CHANGED);
		}
	}

	if (dwOptionsChanged & secConfigs)
	{
		if (pMainWnd != NULL)
		{
			pMainWnd->GlobalNotify(WM_GAME_CHANGED);
		}
	}
}

