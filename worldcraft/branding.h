
#ifndef APSTUDIO_INVOKED
#pragma once
#endif

#define PROGRAM_NAME "Worldcraft"
#define PROGRAM_NAME_AND_VERSION "Worldcraft 3.40"
#define INTERNAL_NAME "Worldcraft"
#define PRODUCT_NAME "Worldcraft (tm)"

#define TRADEMARK_TEXT "Worldcraft is a trademark of Valve Corporation"

// Internal, should stay consistent

// The folder name created under HKEY_LOCAL_USER/SOFTWARE
#define REGISTRY_COMPANY_NAME "Slartibarty"

// Must always be Worldcraft, this is used for the mainframe name, used for registry keys, etc
#define STABLE_PROGRAM_NAME "Worldcraft"

// We don't use the appid at all, should probably remove
#define APP_ID "Slartibarty.Worldcraft"
