-------------------------------------------------------------------------------
-- Premake5 build script for Quake 2
-------------------------------------------------------------------------------

-- Convenience locals
local conf_dbg = "debug"
local conf_rel = "release"
local conf_rtl = "retail"

local plat_32bit = "x86"
local plat_64bit = "x64"

local build_dir = "build"

local filter_dbg = "configurations:" .. conf_dbg
local filter_rel_or_rtl = "configurations:release or retail" -- TODO: This shouldn't be necessary
local filter_rtl = "configurations:" .. conf_rtl

local filter_32bit = "platforms:" .. plat_32bit
local filter_64bit = "platforms:" .. plat_64bit

-- Workspace definition -------------------------------------------------------

workspace "worldcraft"
	configurations { conf_dbg, conf_rel, conf_rtl }
	platforms { plat_32bit, plat_64bit }
	location( build_dir )
	preferredtoolarchitecture "x86_64"
	startproject "engine"

-- Configuration --------------------------------------------------------------

-- Misc flags for all projects

includedirs { "public", "thirdparty/stb" }

flags { "MultiProcessorCompile", "NoBufferSecurityCheck" }
staticruntime "On"
cppdialect "C++20"
warnings "Default"
floatingpoint "Default"
characterset "ASCII"
--exceptionhandling "Off"

-- Config for all 32-bit projects
filter( filter_32bit )
	vectorextensions "SSE2"
	architecture "x86"
filter {}
	
-- Config for all 64-bit projects
filter( filter_64bit )
	architecture "x86_64"
filter {}

-- Config for Windows
filter "system:windows"
	buildoptions { "/permissive", "/Zc:__cplusplus" }
	defines { "WIN32", "_WINDOWS", "_CRT_SECURE_NO_WARNINGS", "_CRT_NONSTDC_NO_WARNINGS", "MICROSOFT_WINDOWS_WINBASE_H_DEFINE_INTERLOCKED_CPLUSPLUS_OVERLOADS=0" }
filter {}
	
-- Config for Windows, release, clean this up!
filter { "system:windows", filter_rel_or_rtl }
	buildoptions { "/Gw" }
filter {}

-- Config for all projects in debug, _DEBUG is defined for library-compatibility
filter( filter_dbg )
	defines { "_DEBUG" }
	symbols "FastLink"
filter {}

-- Config for all projects in release or retail, NDEBUG is defined for cstd compatibility (assert)
filter( filter_rel_or_rtl )
	defines { "NDEBUG" }
	symbols "Full"
	optimize "Speed"
filter {}

-- Config for all projects in retail
filter( filter_rtl )
	flags { "LinkTimeOptimization" }
	symbols "Off"
filter {}

-- Config for shared library projects
filter "kind:SharedLib"
	flags { "NoManifest" } -- We don't want manifests for DLLs
filter {}

-- Project definitions --------------------------------------------------------

project "worldcraft"
	kind "WindowedApp"
	language "C++"
	targetname "Worldcraft"
	targetdir "../game/worldcraft"
	debugdir "../game/worldcraft"
	defines { "GLEW_STATIC", "GLEW_NO_GLU" }
	includedirs { "thirdparty/glew/include" }
	flags { "MFC" }
	links { "winmm", "opengl32", "glu32" }
	linkoptions { "/FORCE:MULTIPLE" }
	
	-- Warnings to ignore if doing a w4 run
	disablewarnings { "4244", "4456", "4100", "4324" }
	
	pchsource( "worldcraft/stdafx.cpp" )
	pchheader( "stdafx.h" )
	filter( "files:not worldcraft/**" )
		flags( { "NoPCH" } )
	filter( {} )
	
	files {
		"worldcraft/**",
		
		"common/*.manifest",
		
		"public/**",
		
		"thirdparty/glew/src/glew.c"
	}
	
	removefiles {
		"worldcraft/updatehint.*",
		"worldcraft/materialproxyfactory_wc.*",
		"worldcraft/render3dms.*",
		"worldcraft/faceedit_disppage.*",
		"worldcraft/material.*",
		"worldcraft/studiomodel.*",
		"worldcraft/tooloverlay.*",
		"worldcraft/gizmo.*",
		"worldcraft/render3d_stub.*",
		
		"**.aps"
	}
