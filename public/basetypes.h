
#pragma once

// Begin platform.h

// Extremely low-level platform-specific stuff

#ifndef _WIN32
#error
#endif

#ifdef _CHAR_UNSIGNED
#error
#endif

#define Assert assert			// Slart

using int8		= char;
using int16		= short;
using int32		= int;
using int64		= long long;

using uint8		= unsigned char;
using uint16	= unsigned short;
using uint32	= unsigned int;
using uint64	= unsigned long long;

using byte		= uint8;
using uint		= unsigned int;

using vec_t		= float;		// Obsolete

// Used for standard calling conventions
#ifdef _WIN32
#define FORCEINLINE		__forceinline
#else
#define FORCEINLINE		inline
#endif

// Pass hints to the compiler to prevent it from generating unnessecary / stupid code
// in certain situations.  Several compilers other than MSVC also have an equivilent 
// construct.
//
// Essentially the 'Hint' is that the condition specified is assumed to be true at 
// that point in the compilation.  If '0' is passed, then the compiler assumes that
// any subsequent code in the same 'basic block' is unreachable, and thus usually 
// removed.
#ifdef _WIN32
#define HINT(THE_HINT)	__assume(THE_HINT)
#else
#define HINT(THE_HINT)	0
#endif

// Declspecs
#ifdef _WIN32
#define NOVTABLE		__declspec(novtable)
#else
#define NOVTABLE
#endif

#define abstract_class	class NOVTABLE

//-----------------------------------------------------------------------------
// Methods to invoke the constructor, copy constructor, and destructor
//-----------------------------------------------------------------------------

template <class T>
inline void Construct( T *pMemory )
{
	new( pMemory ) T;
}

template <class T>
inline void CopyConstruct( T *pMemory, T const &src )
{
	new( pMemory ) T( src );
}

template <class T>
inline void Destruct( T *pMemory )
{
	pMemory->~T();

#ifdef _DEBUG
	memset( pMemory, 0xDD, sizeof( T ) );
#endif
}

// end platform.h

template< typename T >
inline constexpr T Min( const T &valMin, const T &valMax )
{
	return valMin < valMax ? valMin : valMax;
}

template< typename T >
inline constexpr T Max( const T &valMin, const T &valMax )
{
	return valMin > valMax ? valMin : valMax;
}

template< typename T >
inline constexpr T Clamp( const T &val, const T &valMin, const T &valMax )
{
	return Min( Max( val, valMin ), valMax );
}

template< typename T >
inline void Swap( T &x, T &y )
{
	T tmp = x;
	x = y;
	y = tmp;
}

//-----------------------------------------------------------------------------
// look for NANs, infinities, and underflows. 
// This assumes the ANSI/IEEE 754-1985 standard
//-----------------------------------------------------------------------------

inline unsigned long& FloatBits( vec_t& f )
{
	return *reinterpret_cast<unsigned long*>(&f);
}

inline unsigned long const& FloatBits( vec_t const& f )
{
	return *reinterpret_cast<unsigned long const*>(&f);
}

inline vec_t BitsToFloat( unsigned long i )
{
	return *reinterpret_cast<vec_t*>(&i);
}

inline bool IsFinite( vec_t f )
{
	return ((FloatBits(f) & 0x7F800000) != 0x7F800000);
}

inline unsigned long FloatAbsBits( vec_t f )
{
	return FloatBits(f) & 0x7FFFFFFF;
}

inline float FloatMakeNegative( vec_t f )
{
	return BitsToFloat( FloatBits(f) | 0x80000000 );
}

#define FLOAT32_NAN_BITS	(unsigned long)0x7FC00000	// not a number!
#define FLOAT32_NAN			BitsToFloat( FLOAT32_NAN_BITS )

#define VEC_T_NAN FLOAT32_NAN

struct color32
{
	uint8 r, g, b, a;

	inline bool operator!=( const struct color32 &other ) const
	{
		return r != other.r || g != other.g || b != other.b || a != other.a;
	}
};

struct colorVec
{
	unsigned r, g, b, a;
};
