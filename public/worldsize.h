// worldsize.h  -- extent of world and resolution/size of coordinate messages used in engine

#pragma once

// These definitions must match the coordinate message sizes in coordsize.h

// Following values should be +16384, -16384, +15/16, -15/16
// NOTE THAT IF THIS GOES ANY BIGGER THEN DISK NODES/LEAVES CANNOT USE SHORTS TO STORE THE BOUNDS
constexpr int MAX_COORD_INTEGER		= 16384;
constexpr int MIN_COORD_INTEGER		= -MAX_COORD_INTEGER;

// Width of the coord system, which is TOO BIG to send as a client/server coordinate value
constexpr int COORD_EXTENT			= 2 * MAX_COORD_INTEGER;

// Maximum traceable distance ( assumes cubic world and trace from one corner to opposite )
// COORD_EXTENT * sqrt(3)
constexpr float MAX_TRACE_LENGTH	= 1.732050807569f * COORD_EXTENT;
