
#include "basetypes.h"

#include <cassert>

#define STB_SPRINTF_IMPLEMENTATION
#include "stb_sprintf.h"

#include "stringtools.h"

#define AssertString(str) assert( str && str[0] )

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void StringSubstitute( char *pStr, char a, char b )
{
	AssertString( pStr );

	for ( ; *pStr; ++pStr )
	{
		if ( *pStr == a )
		{
			*pStr = b;
		}
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void V_strcpy_s( char *pDest, strlen_t nDestSize, const char *pSrc )
{
	Assert( pDest && pSrc );

	char *pLast = pDest + nDestSize - 1;
	while ( ( pDest < pLast ) && ( *pSrc != 0 ) )
	{
		*pDest = *pSrc;
		++pDest; ++pSrc;
	}
	*pDest = 0;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
int	V_stricmp( const char *s1, const char *s2 )
{
	// It is not uncommon to compare a string to itself. Since stricmp
	// is expensive and pointer comparison is cheap, this simple test
	// can save a lot of cycles, and cache pollution.
	// This also implicitly does the s1 and s2 both equal to NULL check
	// that the POSIX code used to have.
	if ( s1 == s2 )
		return 0;

	const uint8 *pS1 = (const uint8 *)s1;
	const uint8 *pS2 = (const uint8 *)s2;
	for ( ;;)
	{
		int c1 = *( pS1++ );
		int c2 = *( pS2++ );
		if ( c1 == c2 )
		{
			if ( !c1 ) return 0;
		}
		else
		{
			if ( !c2 )
			{
				return c1 - c2;
			}
			c1 = FastASCIIToLower( c1 );
			c2 = FastASCIIToLower( c2 );
			if ( c1 != c2 )
			{
				return c1 - c2;
			}
		}
		c1 = *( pS1++ );
		c2 = *( pS2++ );
		if ( c1 == c2 )
		{
			if ( !c1 ) return 0;
		}
		else
		{
			if ( !c2 )
			{
				return c1 - c2;
			}
			c1 = FastASCIIToLower( c1 );
			c2 = FastASCIIToLower( c2 );
			if ( c1 != c2 )
			{
				return c1 - c2;
			}
		}
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
int V_strnicmp( const char *s1, const char *s2, strlen_t n )
{
	Assert( n > 0 );

	if ( s1 == s2 )
		return 0;

	const uint8 *pS1 = (const uint8 *)s1;
	const uint8 *pS2 = (const uint8 *)s2;
	while ( n-- > 0 )
	{
		int c1 = *( pS1++ );
		int c2 = *( pS2++ );
		if ( c1 == c2 )
		{
			if ( !c1 ) return 0;
		}
		else
		{
			if ( !c2 )
			{
				return c1 - c2;
			}
			c1 = FastASCIIToLower( c1 );
			c2 = FastASCIIToLower( c2 );
			if ( c1 != c2 )
			{
				return c1 - c2;
			}
		}
	}

	return 0;
}

//-----------------------------------------------------------------------------
// Finds a string in another string with a case insensitive test
//-----------------------------------------------------------------------------
const char *V_stristr( const char *pStr, const char *pSearch )
{
	Assert( pStr );
	Assert( pSearch );
	if ( !pStr || !pSearch )
		return 0;

	const char *pLetter = pStr;

	// Check the entire string
	while ( *pLetter != 0 )
	{
		// Skip over non-matches
		if ( FastASCIIToLower( *pLetter ) == FastASCIIToLower( *pSearch ) )
		{
			// Check for match
			const char *pMatch = pLetter + 1;
			const char *pTest = pSearch + 1;
			while ( *pTest != 0 )
			{
				// We've run off the end; don't bother.
				if ( *pMatch == 0 )
					return 0;

				if ( FastASCIIToLower( *pMatch ) != FastASCIIToLower( *pTest ) )
					break;

				++pMatch;
				++pTest;
			}

			// Found a match!
			if ( *pTest == 0 )
				return pLetter;
		}

		++pLetter;
	}

	return 0;
}

//-----------------------------------------------------------------------------
// Finds a string in another string with a case insensitive test w/ length validation
//-----------------------------------------------------------------------------
const char *V_strnistr( const char *pStr, const char *pSearch, strlen_t n )
{
	Assert( pStr );
	Assert( pSearch );
	if ( !pStr || !pSearch )
		return 0;

	const char *pLetter = pStr;

	// Check the entire string
	while ( *pLetter != 0 )
	{
		if ( n <= 0 )
			return 0;

		// Skip over non-matches
		if ( FastASCIIToLower( *pLetter ) == FastASCIIToLower( *pSearch ) )
		{
			strlen_t n1 = n - 1;

			// Check for match
			const char *pMatch = pLetter + 1;
			const char *pTest = pSearch + 1;
			while ( *pTest != 0 )
			{
				if ( n1 <= 0 )
					return 0;

				// We've run off the end; don't bother.
				if ( *pMatch == 0 )
					return 0;

				if ( FastASCIIToLower( *pMatch ) != FastASCIIToLower( *pTest ) )
					break;

				++pMatch;
				++pTest;
				--n1;
			}

			// Found a match!
			if ( *pTest == 0 )
				return pLetter;
		}

		++pLetter;
		--n;
	}

	return 0;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
int V_vsprintf_s( char *pDest, strlen_t nDestSize, _Printf_format_string_ const char *pFmt, va_list args )
{
	return stbsp_vsnprintf( pDest, nDestSize, pFmt, args );
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
int V_vsprintf( char *pDest, _Printf_format_string_ const char *pFmt, va_list args )
{
	return stbsp_vsprintf( pDest, pFmt, args );
}

//-----------------------------------------------------------------------------


