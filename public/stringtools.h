
#pragma once

#include "basetypes.h"

#include <cstring>
#include <cstdarg>

using strlen_t = int;

#define USE_FAST_CASE_CONVERSION 1
#if USE_FAST_CASE_CONVERSION
/// Faster conversion of an ascii char to upper case. This function does not obey locale or any language
/// setting. It should not be used to convert characters for printing, but it is a better choice
/// for internal strings such as used for hash table keys, etc. It's meant to be inlined and used
/// in places like the various dictionary classes. Not obeying locale also protects you from things
/// like your hash values being different depending on the locale setting.
#define FastASCIIToUpper( c ) ( ( ( (c) >= 'a' ) && ( (c) <= 'z' ) ) ? ( (c) - 32 ) : (c) )
/// similar to FastASCIIToLower
#define FastASCIIToLower( c ) ( ( ( (c) >= 'A' ) && ( (c) <= 'Z' ) ) ? ( (c) + 32 ) : (c) )
#else
#define FastASCIIToLower tolower
#define FastASCIIToUpper toupper
#endif

[[nodiscard]]
inline strlen_t V_strlen( const char *str )
{
	return static_cast<strlen_t>( strlen( str ) );
}

// SlartTodo: Operate with int like std::tolower?
inline char V_tolower( char ch )
{
	return ( ch <= 'Z' && ch >= 'A' ) ? ( ch + ( 'a' - 'A' ) ) : ch;
}

inline char V_toupper( char ch )
{
	return ( ch >= 'a' && ch <= 'z' ) ? ( ch - ( 'a' - 'A' ) ) : ch;
}

inline void V_strlwr( char *dest )
{
	while ( *dest )
	{
		*dest = V_tolower( *dest );
		++dest;
	}
}

inline void V_strupr( char *dest )
{
	while ( *dest )
	{
		*dest = V_toupper( *dest );
		++dest;
	}
}

// String content subsitution routines

// Subsitute A for B
void StringSubstitute( char *pStr, char a, char b );

// Replaces all instances of '\' with '/'
inline void PathFixSlashes( char *pStr )
{
	StringSubstitute( pStr, '\\', '/' );
}

// BLAH

void V_strcpy_s( char *pDest, strlen_t nDestSize, const char *pSrc );

template< strlen_t nDestSize >
inline void V_strcpy_s( char( &pDest )[nDestSize], const char *pSrc )
{
	V_strcpy_s( pDest, nDestSize, pSrc );
}

inline int V_strcmp( const char *s1, const char *s2 )
{
	return strcmp( s1, s2 );
}

inline int V_strncmp( const char *s1, const char *s2, strlen_t n )
{
	return strncmp( s1, s2, n );
}

int	V_stricmp( const char *s1, const char *s2 );
int V_strnicmp( const char *s1, const char *s2, strlen_t n );

const char *V_stristr( const char *pStr, const char *pSearch );
const char *V_strnistr( const char *pStr, const char *pSearch, strlen_t n );

// Formatting
// These functions return int representing the length of a string
// because whoever penned the standard forgot to make it size_t (no really)

// Safe

int V_vsprintf_s( char *pDest, strlen_t nDestSize, _Printf_format_string_ const char *pFmt, va_list args );

template< strlen_t nDestSize >
inline int V_vsprintf_s( char( &pDest )[nDestSize], _Printf_format_string_ const char *pFmt, va_list args )
{
	return V_vsprintf_s( pDest, nDestSize, pFmt, args );
}

inline int V_sprintf_s( char *pDest, strlen_t nDestSize, _Printf_format_string_ const char *pFmt, ... )
{
	int result;

	va_list args;
	va_start( args, pFmt );
	result = V_vsprintf_s( pDest, nDestSize, pFmt, args );
	va_end( args );

	return result;
}

template< strlen_t nDestSize >
inline int V_sprintf_s( char( &pDest )[nDestSize], _Printf_format_string_ const char *pFmt, ... )
{
	int result;

	va_list args;
	va_start( args, pFmt );
	result = V_vsprintf_s( pDest, nDestSize, pFmt, args );
	va_end( args );

	return result;
}

// Unsafe

int V_vsprintf( char *pDest, _Printf_format_string_ const char *pFmt, va_list args );

inline int V_sprintf( char *pDest, _Printf_format_string_ const char *pFmt, ... )
{
	int result;

	va_list args;
	va_start( args, pFmt );
	result = V_vsprintf( pDest, pFmt, args );
	va_end( args );

	return result;
}
