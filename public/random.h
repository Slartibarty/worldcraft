// Random number generator

#pragma once

void	RandomSeed( int iSeed );
float	RandomFloat( float flMinVal, float flMaxVal );
int		RandomInt( int iMinVal, int iMaxVal );
