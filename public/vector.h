/***
*
*	Copyright (c) 1999, Valve LLC. All rights reserved.
*
*	This product contains software technology licensed from Id
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc.
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/

#ifndef VECTOR_H
#define VECTOR_H

#pragma once

#include <cmath>
#include <cfloat>		// FLT_EPSILON
#include <cassert>

#include "random.h"

#include "vector2d.h"

// Uncomment this to add extra asserts to check for NANs, uninitialized vecs, etc.
//#define VECTOR_PARANOIA	1

// Uncomment this to make sure we don't do anything slow with our vectors
//#define VECTOR_NO_SLOW_OPERATIONS 1

#ifdef VECTOR_PARANOIA
#define CHECK_VALID(_v)		assert(_v.IsValid())
#else
#define CHECK_VALID(_v)
#endif

//-----------------------------------------------------------------------------
// 3D Vector
//-----------------------------------------------------------------------------

class Vector
{
public:
	union
	{
		struct
		{
			float x, y, z;
		};
		float v[3];
	};

	Vector() = default;

	Vector( const Vector & ) = default;
	Vector &operator=( const Vector & ) = default;

	Vector( Vector && ) = default;
	Vector &operator=( Vector && ) = default;

	constexpr Vector( float X, float Y, float Z ) noexcept : x( X ), y( Y ), z( Z ) {}

	void Init( float X = 0.0f, float Y = 0.0f, float Z = 0.0f )
	{
		x = X; y = Y; z = Z;
	}

	// Got any nasty NAN's?
	bool IsValid() const
	{
		return isfinite( x ) && isfinite( y ) && isfinite( z );
	}

	// avoid, use vec.v[i]
	float &operator[]( int i )
	{
		return v[i];
	}

	// avoid, use vec.v[i]
	float operator[]( int i ) const
	{
		return v[i];
	}

	float *Base()
	{
		return reinterpret_cast<float *>( this );
	}

	const float *Base() const
	{
		return reinterpret_cast<const float *>( this );
	}

	Vector2D &Vector::AsVector2D()
	{
		return *(Vector2D *)this;
	}

	const Vector2D &Vector::AsVector2D() const
	{
		return *(const Vector2D *)this;
	}

	void Random( float minVal, float maxVal )
	{
		x = RandomFloat( minVal, maxVal );
		y = RandomFloat( minVal, maxVal );
		z = RandomFloat( minVal, maxVal );
	}

	bool Vector::operator==( const Vector &src ) const
	{
		return ( src.x == x ) && ( src.y == y ) && ( src.z == z );
	}

	bool Vector::operator!=( const Vector &src ) const
	{
		return ( src.x != x ) || ( src.y != y ) || ( src.z != z );
	}

	// arithmetic operations
	inline Vector&	operator+=(const Vector &v);
	inline Vector&	operator-=(const Vector &v);
	inline Vector&	operator*=(const Vector &v);
	inline Vector&	operator*=(float s);
	inline Vector&	operator/=(const Vector &v);
	inline Vector&	operator/=(float s);

	// negate the vector components
	void Negate()
	{
		x = -x; y = -y; z = -z;
	}

	// get the vector's magnitude
	float Length() const
	{
		return sqrt( x*x + y*y + z*z );
	}

	// get the vector's magnitude squared
	float LengthSqr() const
	{
		return x*x + y*y + z*z;
	}

	float Length2D() const
	{
		return sqrt( x*x + y*y );
	}

	// get the distance from this vector to the other one
	float DistTo( const Vector &vOther ) const
	{
		Vector delta;

		delta.x = x - vOther.x;
		delta.y = y - vOther.y;
		delta.z = z - vOther.z;

		return delta.Length();
	}

	// Get the distance from this vector to the other one squared
	float DistToSqr( const Vector &vOther ) const
	{
		Vector delta;

		delta.x = x - vOther.x;
		delta.y = y - vOther.y;
		delta.z = z - vOther.z;

		return delta.LengthSqr();
	}

	// FIXME: Remove
	// For backwards compatibility
	// Multiply, add, and assign to this (ie: *this = a + b * scalar). This
	// is about 12% faster than the actual vector equation (because it's done per-component
	// rather than per-vector)
	void MulAdd( Vector const &a, Vector const &b, float scalar )
	{
		x = a.x + b.x * scalar;
		y = a.y + b.y * scalar;
		z = a.z + b.z * scalar;
	}

	// FIXME: remove
	// For backwards compatibility
	// dot product
	float Dot( Vector const &vOther ) const
	{
		return( x*vOther.x + y*vOther.y + z*vOther.z );
	}

#ifndef VECTOR_NO_SLOW_OPERATIONS

	// arithmetic operations
	Vector	operator-(void) const;

	Vector	operator+(const Vector& v) const;
	Vector	operator-(const Vector& v) const;
	Vector	operator*(const Vector& v) const;
	Vector	operator/(const Vector& v) const;
	Vector	operator*(float fl) const;
	Vector	operator/(float fl) const;

	// Cross product between two vectors.
	Vector	Cross(const Vector &vOther) const;

	// Returns a vector with the min or max in X, Y, and Z.
	Vector	Min(const Vector &vOther) const;
	Vector	Max(const Vector &vOther) const;

#else

private:
	// No copy constructors allowed if we're in optimal mode
	inline Vector(Vector const& vOther);

	// No assignment operators either...
	Vector& operator=( Vector const& src );

#endif
};

static_assert( std::is_trivial<Vector>::value );

//-----------------------------------------------------------------------------
// Vector related operations
//-----------------------------------------------------------------------------

// Vector clear
inline void VectorClear( Vector& a );

// Copy
inline void VectorCopy( Vector const& src, Vector& dst );

// Vector arithmetic
inline void VectorAdd( Vector const& a, Vector const& b, Vector& result );
inline void VectorSubtract( Vector const& a, Vector const& b, Vector& result );
inline void VectorMultiply( Vector const& a, float b, Vector& result );
inline void VectorMultiply( Vector const& a, Vector const& b, Vector& result );
inline void VectorDivide( Vector const& a, float b, Vector& result );
inline void VectorDivide( Vector const& a, Vector const& b, Vector& result );
inline void VectorScale ( Vector const& in, float scale, Vector& result );
// void VectorMA( Vector const& start, float s, Vector const& dir, Vector& result );

// Vector equality with tolerance
bool VectorsAreEqual( Vector const& src1, Vector const& src2, float tolerance = 0.0f );

#define VectorExpand(v) (v).x, (v).y, (v).z


// Normalization
// FIXME: Can't use quite yet
//float VectorNormalize( Vector& v );

// Length
inline float VectorLength( Vector const& v );

// Dot Product
inline float DotProduct(Vector const& a, Vector const& b);

// Cross product
void CrossProduct(Vector const& a, Vector const& b, Vector& result );

// Store the min or max of each of x, y, and z into the result.
void VectorMin( Vector const &a, Vector const &b, Vector &result );
void VectorMax( Vector const &a, Vector const &b, Vector &result );

// Linearly interpolate between two vectors
void VectorLerp(Vector const& src1, Vector const& src2, float t, Vector& dest );

#ifndef VECTOR_NO_SLOW_OPERATIONS

// Cross product
Vector CrossProduct( const Vector& a, const Vector& b );

// Random vector creation
Vector RandomVector( float minVal, float maxVal );

#endif

//-----------------------------------------------------------------------------
//
// Inlined Vector methods
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// initialization
//-----------------------------------------------------------------------------

inline void VectorClear( Vector& a )
{
	a.x = a.y = a.z = 0.0f;
}

//-----------------------------------------------------------------------------
// Copy
//-----------------------------------------------------------------------------

inline void VectorCopy( Vector const& src, Vector& dst )
{
	CHECK_VALID(src);
	dst.x = src.x;
	dst.y = src.y;
	dst.z = src.z;
}

//-----------------------------------------------------------------------------
// standard math operations
//-----------------------------------------------------------------------------

inline  Vector& Vector::operator+=(const Vector& v)
{
	CHECK_VALID(*this);
	CHECK_VALID(v);
	x+=v.x; y+=v.y; z += v.z;
	return *this;
}

inline  Vector& Vector::operator-=(const Vector& v)
{
	CHECK_VALID(*this);
	CHECK_VALID(v);
	x-=v.x; y-=v.y; z -= v.z;
	return *this;
}

inline  Vector& Vector::operator*=(float fl)
{
	x *= fl;
	y *= fl;
	z *= fl;
	CHECK_VALID(*this);
	return *this;
}

inline  Vector& Vector::operator*=(Vector const& v)
{
	CHECK_VALID(v);
	x *= v.x;
	y *= v.y;
	z *= v.z;
	CHECK_VALID(*this);
	return *this;
}

inline  Vector& Vector::operator/=(float fl)
{
	assert( fl != 0.0f );
	float oofl = 1.0f / fl;
	x *= oofl;
	y *= oofl;
	z *= oofl;
	CHECK_VALID(*this);
	return *this;
}

inline  Vector& Vector::operator/=(Vector const& v)
{
	CHECK_VALID(v);
	assert( v.x != 0.0f && v.y != 0.0f && v.z != 0.0f );
	x /= v.x;
	y /= v.y;
	z /= v.z;
	CHECK_VALID(*this);
	return *this;
}

inline void VectorAdd( Vector const& a, Vector const& b, Vector& c )
{
	CHECK_VALID(a);
	CHECK_VALID(b);
	c.x = a.x + b.x;
	c.y = a.y + b.y;
	c.z = a.z + b.z;
}

inline void VectorSubtract( Vector const& a, Vector const& b, Vector& c )
{
	CHECK_VALID(a);
	CHECK_VALID(b);
	c.x = a.x - b.x;
	c.y = a.y - b.y;
	c.z = a.z - b.z;
}

inline void VectorMultiply( Vector const& a, float b, Vector& c )
{
	CHECK_VALID(a);
	assert( isfinite(b) );
	c.x = a.x * b;
	c.y = a.y * b;
	c.z = a.z * b;
}

inline void VectorMultiply( Vector const& a, Vector const& b, Vector& c )
{
	CHECK_VALID(a);
	CHECK_VALID(b);
	c.x = a.x * b.x;
	c.y = a.y * b.y;
	c.z = a.z * b.z;
}

// for backwards compatability
inline void VectorScale ( Vector const& in, float scale, Vector& result )
{
	VectorMultiply( in, scale, result );
}


inline void VectorDivide( Vector const& a, float b, Vector& c )
{
	CHECK_VALID(a);
	assert( b != 0.0f );
	float oob = 1.0f / b;
	c.x = a.x * oob;
	c.y = a.y * oob;
	c.z = a.z * oob;
}

inline void VectorDivide( Vector const& a, Vector const& b, Vector& c )
{
	CHECK_VALID(a);
	CHECK_VALID(b);
	assert( (b.x != 0.0f) && (b.y != 0.0f) && (b.z != 0.0f) );
	c.x = a.x / b.x;
	c.y = a.y / b.y;
	c.z = a.z / b.z;
}

void VectorMA( Vector const& start, float s, Vector const& dir, Vector& result )
#if 0
{
	CHECK_VALID(start);
	CHECK_VALID(dir);
	assert( isfinite(s) );
	result.x = start.x + s*dir.x;
	result.y = start.y + s*dir.y;
	result.z = start.z + s*dir.z;
}
#else
;
#endif

inline void VectorLerp(const Vector& src1, const Vector& src2, float t, Vector& dest )
{
	CHECK_VALID(src1);
	CHECK_VALID(src2);
	dest[0] = src1[0] + (src2[0] - src1[0]) * t;
	dest[1] = src1[1] + (src2[1] - src1[1]) * t;
	dest[2] = src1[2] + (src2[2] - src1[2]) * t;
}


//-----------------------------------------------------------------------------
// Temporary storage for vector results so const Vector& results can be returned
//-----------------------------------------------------------------------------
inline Vector &AllocTempVector()
{
	static Vector s_vecTemp[32];
	static int s_nIndex = 0;

	s_nIndex = (s_nIndex + 1) & 0x1F;
	return s_vecTemp[s_nIndex];
}


//-----------------------------------------------------------------------------
// dot, cross
//-----------------------------------------------------------------------------
inline float DotProduct(const Vector& a, const Vector& b)
{
	CHECK_VALID(a);
	CHECK_VALID(b);
	return( a.x*b.x + a.y*b.y + a.z*b.z );
}

inline void CrossProduct(Vector const& a, Vector const& b, Vector& result )
{
	CHECK_VALID(a);
	CHECK_VALID(b);
	result.x = a.y*b.z - a.z*b.y;
	result.y = a.z*b.x - a.x*b.z;
	result.z = a.x*b.y - a.y*b.x;
}

inline float DotProductAbs( const Vector &v0, const Vector &v1 )
{
	CHECK_VALID(v0);
	CHECK_VALID(v1);
	return fabs(v0.x*v1.x) + fabs(v0.y*v1.y) + fabs(v0.z*v1.z);
}

inline float DotProductAbs( const Vector &v0, const float *v1 )
{
	return fabs(v0.x * v1[0]) + fabs(v0.y * v1[1]) + fabs(v0.z * v1[2]);
}

//-----------------------------------------------------------------------------
// length
//-----------------------------------------------------------------------------

inline float VectorLength( Vector const& v )
{
	CHECK_VALID(v);
	return (float)sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
}

//-----------------------------------------------------------------------------
// Normalization
//-----------------------------------------------------------------------------

/*
// FIXME: Can't use until we're un-macroed in mathlib.h
inline float VectorNormalize( Vector& v )
{
	assert( v.IsValid() );
	float l = v.Length();
	if (l != 0.0f)
	{
		v /= l;
	}
	else
	{
		// FIXME:
		// Just copying the existing implemenation; shouldn't res.z == 0?
		v.x = v.y = 0.0f; v.z = 1.0f;
	}
	return l;
}
*/

//-----------------------------------------------------------------------------
// Vector equality with tolerance
//-----------------------------------------------------------------------------
inline bool VectorsAreEqual( Vector const& src1, Vector const& src2, float tolerance )
{
	if (fabs(src1.x - src2.x) > tolerance)
		return false;
	if (fabs(src1.y - src2.y) > tolerance)
		return false;
	return (fabs(src1.z - src2.z) <= tolerance);
}


//-----------------------------------------------------------------------------
// Takes the absolute value of a vector
//-----------------------------------------------------------------------------
inline void VectorAbs( Vector const& src, Vector& dst )
{
	dst.x = fabs(src.x);
	dst.y = fabs(src.y);
	dst.z = fabs(src.z);
}


//-----------------------------------------------------------------------------
//
// Slow methods
//
//-----------------------------------------------------------------------------

#ifndef VECTOR_NO_SLOW_OPERATIONS

//-----------------------------------------------------------------------------
// Returns a vector with the min or max in X, Y, and Z.
//-----------------------------------------------------------------------------
inline Vector Vector::Min(const Vector &vOther) const
{
	return Vector(x < vOther.x ? x : vOther.x,
		y < vOther.y ? y : vOther.y,
		z < vOther.z ? z : vOther.z);
}

inline Vector Vector::Max(const Vector &vOther) const
{
	return Vector(x > vOther.x ? x : vOther.x,
		y > vOther.y ? y : vOther.y,
		z > vOther.z ? z : vOther.z);
}


//-----------------------------------------------------------------------------
// arithmetic operations
//-----------------------------------------------------------------------------

inline Vector Vector::operator-(void) const
{
	return Vector(-x,-y,-z);
}

inline Vector Vector::operator+(const Vector& v) const
{
	Vector res;
	VectorAdd( *this, v, res );
	return res;
}

inline Vector Vector::operator-(const Vector& v) const
{
	Vector res;
	VectorSubtract( *this, v, res );
	return res;
}

inline Vector Vector::operator*(float fl) const
{
	Vector res;
	VectorMultiply( *this, fl, res );
	return res;
}

inline Vector Vector::operator*(Vector const& v) const
{
	Vector res;
	VectorMultiply( *this, v, res );
	return res;
}

inline Vector Vector::operator/(float fl) const
{
	Vector res;
	VectorDivide( *this, fl, res );
	return res;
}

inline Vector Vector::operator/(Vector const& v) const
{
	Vector res;
	VectorDivide( *this, v, res );
	return res;
}

inline Vector operator*(float fl, const Vector& v)
{
	return v * fl;
}

//-----------------------------------------------------------------------------
// cross product
//-----------------------------------------------------------------------------

inline Vector Vector::Cross(const Vector& vOther) const
{
	Vector res;
	CrossProduct( *this, vOther, res );
	return res;
}

//-----------------------------------------------------------------------------
// 2D
//-----------------------------------------------------------------------------

inline Vector CrossProduct(const Vector& a, const Vector& b)
{
	return Vector( a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x );
}

inline void VectorMin( Vector const &a, Vector const &b, Vector &result )
{
	result.x = (a.x < b.x) ? a.x : b.x;
	result.y = (a.y < b.y) ? a.y : b.y;
	result.z = (a.z < b.z) ? a.z : b.z;
}

inline void VectorMax( Vector const &a, Vector const &b, Vector &result )
{
	result.x = (a.x > b.x) ? a.x : b.x;
	result.y = (a.y > b.y) ? a.y : b.y;
	result.z = (a.z > b.z) ? a.z : b.z;
}

// Get a random vector.
inline Vector RandomVector( float minVal, float maxVal )
{
	Vector random;
	random.Random( minVal, maxVal );
	return random;
}

#endif //slow

//-----------------------------------------------------------------------------
// Helper debugging stuff....
//-----------------------------------------------------------------------------

inline bool operator==( float const *f, Vector const &v ) = delete;
inline bool operator==( Vector const &v, float const *f ) = delete;
inline bool operator!=( float const *f, Vector const &v ) = delete;
inline bool operator!=( Vector const &v, float const *f ) = delete;

//-----------------------------------------------------------------------------
// Quaternion
//-----------------------------------------------------------------------------

class RadianEuler;

class Quaternion				// same data-layout as engine's vec4_t,
{								//		which is a float[4]
public:
	inline Quaternion(void)							{ }
	inline Quaternion(RadianEuler const &angle);	// evil auto type promotion!!!

	inline void Init(float ix=0.0f, float iy=0.0f, float iz=0.0f, float iw=0.0f)	{ x = ix; y = iy; z = iz; w = iw; }

	bool IsValid() const;

	// array access...
	float operator[](int i) const;
	float& operator[](int i);

	float x, y, z, w;
};


//-----------------------------------------------------------------------------
// Array access
//-----------------------------------------------------------------------------

inline float& Quaternion::operator[](int i)
{
	assert( (i >= 0) && (i < 4) );
	return ((float*)this)[i];
}

inline float Quaternion::operator[](int i) const
{
	assert( (i >= 0) && (i < 4) );
	return ((float*)this)[i];
}

//-----------------------------------------------------------------------------
// Radian Euler QAngle aligned to axis (NOT ROLL/PITCH/YAW)
//-----------------------------------------------------------------------------

class RadianEuler
{
public:
	inline RadianEuler(void)							{ }
	inline RadianEuler(float X, float Y, float Z)		{ x = X; y = Y; z = Z; }
	inline RadianEuler(Quaternion const &q);	// evil auto type promotion!!!

	// Initialization
	inline void Init(float ix=0.0f, float iy=0.0f, float iz=0.0f)	{ x = ix; y = iy; z = iz; }

	bool IsValid() const;

	// array access...
	float operator[](int i) const;
	float& operator[](int i);

	float x, y, z;
};


extern void AngleQuaternion( RadianEuler const &angles, Quaternion &qt );
extern void QuaternionAngles( Quaternion const &q, RadianEuler &angles );
inline Quaternion::Quaternion(RadianEuler const &angle)
{
	AngleQuaternion( angle, *this );
}

inline bool Quaternion::IsValid() const
{
	return isfinite(x) && isfinite(y) && isfinite(z) && isfinite(w);
}

inline RadianEuler::RadianEuler(Quaternion const &q)
{
	QuaternionAngles( q, *this );
}

inline void VectorCopy( RadianEuler const& src, RadianEuler &dst )
{
	CHECK_VALID(src);
	dst.x = src.x;
	dst.y = src.y;
	dst.z = src.z;
}

inline void VectorScale( RadianEuler const& src, float b, RadianEuler &dst )
{
	CHECK_VALID(src);
	assert( isfinite(b) );
	dst.x = src.x * b;
	dst.y = src.y * b;
	dst.z = src.z * b;
}

inline bool RadianEuler::IsValid() const
{
	return isfinite(x) && isfinite(y) && isfinite(z);
}

//-----------------------------------------------------------------------------
// Array access
//-----------------------------------------------------------------------------

inline float& RadianEuler::operator[](int i)
{
	assert( (i >= 0) && (i < 3) );
	return ((float*)this)[i];
}

inline float RadianEuler::operator[](int i) const
{
	assert( (i >= 0) && (i < 3) );
	return ((float*)this)[i];
}

//-----------------------------------------------------------------------------
// Degree Euler QAngle pitch, yaw, roll
//-----------------------------------------------------------------------------
class QAngle
{
public:
	// Members
	float x, y, z;

	// Construction/destruction
	QAngle(void);
	QAngle(float X, float Y, float Z);

	// Initialization
	void Init(float ix=0.0f, float iy=0.0f, float iz=0.0f);
	void Random( float minVal, float maxVal );

	// Got any nasty NAN's?
	bool IsValid() const;

	// array access...
	float operator[](int i) const;
	float& operator[](int i);

	// Base address...
	float* Base();
	float const* Base() const;

	// equality
	bool operator==(const QAngle& v) const;
	bool operator!=(const QAngle& v) const;

	// arithmetic operations
	QAngle&	operator+=(const QAngle &v);
	QAngle&	operator-=(const QAngle &v);
	QAngle&	operator*=(float s);
	QAngle&	operator/=(float s);

	// Get the vector's magnitude.
	float	Length() const;
	float	LengthSqr() const;

	// negate the QAngle components
	//void	Negate();

#ifndef VECTOR_NO_SLOW_OPERATIONS
	// copy constructors
	// QAngle(const QAngle &vOther);

	// assignment
	QAngle& operator=(const QAngle &vOther);

	// arithmetic operations
	QAngle	operator-(void) const;

	QAngle	operator+(const QAngle& v) const;
	QAngle	operator-(const QAngle& v) const;
	QAngle	operator*(float fl) const;
	QAngle	operator/(float fl) const;
#else

private:
	// No copy constructors allowed if we're in optimal mode
	QAngle(QAngle const& vOther);

	// No assignment operators either...
	QAngle& operator=( QAngle const& src );

#endif
};

inline void VectorAdd( QAngle const& a, QAngle const& b, QAngle& result )
{
	CHECK_VALID(a);
	CHECK_VALID(b);
	result.x = a.x + b.x;
	result.y = a.y + b.y;
	result.z = a.z + b.z;
}

inline void VectorMA( const QAngle &start, float scale, const QAngle &direction, QAngle &dest )
{
	CHECK_VALID(start);
	CHECK_VALID(direction);
	dest.x = start.x + scale * direction.x;
	dest.y = start.y + scale * direction.y;
	dest.z = start.z + scale * direction.z;
}

//-----------------------------------------------------------------------------
// constructors
//-----------------------------------------------------------------------------

inline QAngle::QAngle(void)
{
#ifdef VECTOR_PARANOIA
	// Initialize to NAN to catch errors
	x = y = z = VEC_T_NAN;
#endif
}

inline QAngle::QAngle(float X, float Y, float Z)
{
	x = X; y = Y; z = Z;
	CHECK_VALID(*this);
}

//-----------------------------------------------------------------------------
// initialization
//-----------------------------------------------------------------------------

inline void QAngle::Init( float ix, float iy, float iz )
{
	x = ix; y = iy; z = iz;
	CHECK_VALID(*this);
}


inline void QAngle::Random( float minVal, float maxVal )
{
	x = RandomFloat( minVal, maxVal );
	y = RandomFloat( minVal, maxVal );
	z = RandomFloat( minVal, maxVal );
	CHECK_VALID(*this);
}

inline QAngle RandomAngle( float minVal, float maxVal )
{
	Vector random;
	random.Random( minVal, maxVal );
	return QAngle( random.x, random.y, random.z );
}

//-----------------------------------------------------------------------------
// assignment
//-----------------------------------------------------------------------------

inline QAngle& QAngle::operator=(const QAngle &vOther)
{
	CHECK_VALID(vOther);
	x=vOther.x; y=vOther.y; z=vOther.z;
	return *this;
}


//-----------------------------------------------------------------------------
// Array access
//-----------------------------------------------------------------------------

inline float& QAngle::operator[](int i)
{
	assert( (i >= 0) && (i < 3) );
	return ((float*)this)[i];
}

inline float QAngle::operator[](int i) const
{
	assert( (i >= 0) && (i < 3) );
	return ((float*)this)[i];
}


//-----------------------------------------------------------------------------
// Base address...
//-----------------------------------------------------------------------------

inline float* QAngle::Base()
{
	return (float*)this;
}

inline float const* QAngle::Base() const
{
	return (float const*)this;
}

//-----------------------------------------------------------------------------
// IsValid?
//-----------------------------------------------------------------------------

inline bool QAngle::IsValid() const
{
	return isfinite(x) && isfinite(y) && isfinite(z);
}


//-----------------------------------------------------------------------------
// comparison
//-----------------------------------------------------------------------------

inline bool QAngle::operator==( QAngle const& src ) const
{
	CHECK_VALID(src);
	CHECK_VALID(*this);
	return (src.x == x) && (src.y == y) && (src.z == z);
}

inline bool QAngle::operator!=( QAngle const& src ) const
{
	CHECK_VALID(src);
	CHECK_VALID(*this);
	return (src.x != x) || (src.y != y) || (src.z != z);
}

//-----------------------------------------------------------------------------
// Copy
//-----------------------------------------------------------------------------

inline void VectorCopy( QAngle const& src, QAngle& dst )
{
	CHECK_VALID(src);
	dst.x = src.x;
	dst.y = src.y;
	dst.z = src.z;
}


//-----------------------------------------------------------------------------
// standard math operations
//-----------------------------------------------------------------------------


inline QAngle& QAngle::operator+=(const QAngle& v)
{
	CHECK_VALID(*this);
	CHECK_VALID(v);
	x+=v.x; y+=v.y; z += v.z;
	return *this;
}

inline QAngle& QAngle::operator-=(const QAngle& v)
{
	CHECK_VALID(*this);
	CHECK_VALID(v);
	x-=v.x; y-=v.y; z -= v.z;
	return *this;
}

inline QAngle& QAngle::operator*=(float fl)
{
	x *= fl;
	y *= fl;
	z *= fl;
	CHECK_VALID(*this);
	return *this;
}

inline QAngle& QAngle::operator/=(float fl)
{
	assert( fl != 0.0f );
	float oofl = 1.0f / fl;
	x *= oofl;
	y *= oofl;
	z *= oofl;
	CHECK_VALID(*this);
	return *this;
}


//-----------------------------------------------------------------------------
// length
//-----------------------------------------------------------------------------

inline float QAngle::Length( ) const
{
	CHECK_VALID(*this);
	return (float)sqrt( LengthSqr( ) );
}


inline float QAngle::LengthSqr( ) const
{
	CHECK_VALID(*this);
	return x * x + y * y + z * z;
}


//-----------------------------------------------------------------------------
// Vector equality with tolerance
//-----------------------------------------------------------------------------
inline bool QAnglesAreEqual( QAngle const& src1, QAngle const& src2, float tolerance = 0.0f )
{
	if (fabs(src1.x - src2.x) > tolerance)
		return false;
	if (fabs(src1.y - src2.y) > tolerance)
		return false;
	return (fabs(src1.z - src2.z) <= tolerance);
}



//-----------------------------------------------------------------------------
// arithmetic operations
//-----------------------------------------------------------------------------

inline QAngle QAngle::operator-(void) const
{
	return QAngle(-x,-y,-z);
}

inline QAngle QAngle::operator+(const QAngle& v) const
{
	QAngle res;
	res.x = x + v.x;
	res.y = y + v.y;
	res.z = z + v.z;
	return res;
}

inline QAngle QAngle::operator-(const QAngle& v) const
{
	QAngle res;
	res.x = x - v.x;
	res.y = y - v.y;
	res.z = z - v.z;
	return res;
}

inline QAngle QAngle::operator*(float fl) const
{
	QAngle res;
	res.x = x * fl;
	res.y = y * fl;
	res.z = z * fl;
	return res;
}

inline QAngle QAngle::operator/(float fl) const
{
	QAngle res;
	res.x = x / fl;
	res.y = y / fl;
	res.z = z / fl;
	return res;
}

inline QAngle operator*(float fl, const QAngle& v)
{
	return v * fl;
}


//-----------------------------------------------------------------------------

// FIXME: Change this back to a #define once we get rid of the float version
inline float VectorNormalize( Vector &vec )
{
	float radius = sqrt(vec.x*vec.x + vec.y*vec.y + vec.z*vec.z);

	// FLT_EPSILON is added to the radius to eliminate the possibility of divide by zero.
	float iradius = 1.f / ( radius + FLT_EPSILON );

	vec.x *= iradius;
	vec.y *= iradius;
	vec.z *= iradius;

	return radius;
}

inline float InvRSquared(const Vector &v)
{
	float r2 = DotProduct(v, v);
	return r2 < 1.f ? 1.f : 1/r2;
}

#endif
