// Random number generator

//-----------------------------------------------------------------------------

namespace randomprivate
{
	constexpr int NTAB = 32;

	constexpr int IA = 16807;
	constexpr int IM = 2147483647;
	constexpr int IQ = 127773;
	constexpr int IR = 2836;
	constexpr int NDIV = (1+(IM-1)/NTAB);
	constexpr unsigned int MAX_RANDOM_RANGE = 0x7FFFFFFF;

	// fran1 -- return a random floating-point number on the interval [0,1)
	//
	constexpr float AM = (1.0f/IM);
	constexpr float EPS = 1.2e-7f;
	constexpr float RNMX = (1.0f-EPS);

	// Variables

	int idum;
	int iy;
	int iv[NTAB];

	int GenerateRandomNumber()
	{
		int j;
		int k;
	
		if (idum <= 0 || !iy)
		{
			if (-(idum) < 1) 
				idum=1;
			else 
				idum = -(idum);

			for ( j=NTAB+7; j>=0; j--)
			{
				k = (idum)/IQ;
				idum = IA*(idum-k*IQ)-IR*k;
				if (idum < 0) 
					idum += IM;
				if (j < NTAB)
					iv[j] = idum;
			}
			iy=iv[0];
		}
		k=(idum)/IQ;
		idum=IA*(idum-k*IQ)-IR*k;
		if (idum < 0) 
			idum += IM;
		j=iy/NDIV;

	#if 0
		// We're seeing some strange memory corruption in the contents of s_pUniformStream. 
		// Perhaps it's being caused by something writing past the end of this array? 
		// Bounds-check in release to see if that's the case.
		if (j >= NTAB || j < 0)
		{
			DebuggerBreakIfDebugging();
			Warning("CUniformRandomStream had an array overrun: tried to write to element %d of 0..31. Contact Tom or Elan.\n", j);
			// Ensure that NTAB is a power of two.
			COMPILE_TIME_ASSERT( ( NTAB & ( NTAB - 1 ) ) == 0 );
			// Clamp j.
			j &= NTAB - 1;
		}
	#endif

		iy=iv[j];
		iv[j] = idum;

		return iy;
	}
}

//-----------------------------------------------------------------------------

void RandomSeed( int iSeed )
{
	using namespace randomprivate;

	idum = ( ( iSeed < 0 ) ? iSeed : -iSeed );
	iy = 0;
}

float RandomFloat( float flMinVal, float flMaxVal )
{
	using namespace randomprivate;

	// float in [0,1)
	float fl = AM * GenerateRandomNumber();
	if ( fl > RNMX )
	{
		fl = RNMX;
	}
	return ( fl * ( flMaxVal - flMinVal ) ) + flMinVal; // float in [low,high)
}

int RandomInt( int iMinVal, int iMaxVal )
{
	using namespace randomprivate;

	unsigned int maxAcceptable;
	unsigned int x = iMaxVal - iMinVal + 1;
	unsigned int n;
	if ( x <= 1 || MAX_RANDOM_RANGE < x - 1 )
	{
		return iMinVal;
	}

	// The following maps a uniform distribution on the interval [0,MAX_RANDOM_RANGE]
	// to a smaller, client-specified range of [0,x-1] in a way that doesn't bias
	// the uniform distribution unfavorably. Even for a worst case x, the loop is
	// guaranteed to be taken no more than half the time, so for that worst case x,
	// the average number of times through the loop is 2. For cases where x is
	// much smaller than MAX_RANDOM_RANGE, the average number of times through the
	// loop is very close to 1.
	//
	maxAcceptable = MAX_RANDOM_RANGE - ( ( MAX_RANDOM_RANGE + 1 ) % x );
	do
	{
		n = GenerateRandomNumber();
	} while ( n > maxAcceptable );

	return iMinVal + ( n % x );
}
